/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module CreditCard
define('CreditCard.SetSecurityCode.Length'
,	[
		'SC.Configuration'
	,	'CreditCard.Edit.View'
	,	'CreditCard.View'
	,	'CreditCard.Model'
	,	'CreditCard.Edit.Form.SecurityCode.View'
	,	'creditcard_edit.tpl'
	,	'creditcard_edit_form.tpl'
	,	'creditcard_edit_form_securitycode.tpl'
	,	'Backbone.CompositeView'
	,	'Backbone.FormView'
	,	'Backbone'
	,	'underscore'
	,	'LiveOrder.Model'
	,	'jQuery'
	,	'Utils'
	]
,	function (
		Configuration
	,	CreditCardEditiew
	,	CreditCardView
	,	CreditCardModel
	,	CreditCardEditFormSecurityCodeView
	,	creditcard_edit_tpl
	,	creditcard_edit_form_tpl
	,	creditcard_edit_form_securitycode
	,	BackboneCompositeView
	,	BackboneFormView
	,	Backbone
	,	_
	,	LiveOrderModel
	,	jQuery
	)
{
 'use strict';
	_.extend(CreditCardEditiew.prototype, {

		//@method setPaymethodKey
		setPaymethodKey: function (e)
		{
			var cc_number = jQuery(e.target).val().replace(/\s/g, '')
			,	form = jQuery(e.target).closest('form')
			,	paymenthod_key = this.paymenthodKeyCreditCart(cc_number);

			jQuery(e.target).val(cc_number);

			form.find('[name="paymentmethod"]').val(paymenthod_key || 0);
			
			if (paymenthod_key && this.showCardsImgs) 
			{
				
				form.find('[data-image="creditcard-icon"]').each(function (index, img)
				{
					var $img = jQuery(img);
					if ($img.data('value') === paymenthod_key)
					{
						$img.addClass('creditcard-edit-card-selected');
						$img.removeClass('creditcard-edit-card-no-selected');

						if (jQuery("#ccsecuritycode").val() === "")
						{
							if (jQuery($img).attr('alt') === 'American Express')
							{
								jQuery('#ccsecuritycode').attr('maxlength', 4);
                                jQuery('#ccsecuritycode').removeAttr("disabled");
							}
							else
							{
								jQuery('#ccsecuritycode').attr('maxlength', 3);
                                jQuery('#ccsecuritycode').removeAttr("disabled");
							}
						}

						if (jQuery("#ccsecuritycode").val() && jQuery("#ccsecuritycode").val() != "")
						{
							if ((jQuery($img).attr('alt') === 'American Express') && (jQuery('#ccsecuritycode').val().length != 4))
							{
								jQuery('#incorrect-sec-num').removeClass("hidden");
								jQuery('#ccsecuritycode').select();
								jQuery('#ccsecuritycode').attr('maxlength', 4);
                                jQuery('#ccsecuritycode').removeAttr("disabled");
							}

							if ((jQuery($img).attr('alt') != 'American Express') && (jQuery('#ccsecuritycode').val().length != 3))
							{
								jQuery('#incorrect-sec-num').removeClass("hidden");
								jQuery('#ccsecuritycode').select();
								jQuery('#ccsecuritycode').attr('maxlength', 3);
                                jQuery('#ccsecuritycode').removeAttr("disabled");
							}
						}
					}
					else
					{
						$img.addClass('creditcard-edit-card-no-selected');
						$img.removeClass('creditcard-edit-card-selected');
					}

				});
				form.find('[data-value="creditcard-select-container"]').css('display', 'none');
				form.find('.creditcard-img-container').css('display', 'block');
				
				jQuery("#not-supported").addClass("hidden");
				jQuery("#in-modal-not-supported").addClass("hidden");
			}
			else
			{
				if (_.isUndefined(this.model.validation.ccnumber.fn(cc_number, {}, form)))
				{
					//commented this section out to prevent the behavior of showing the selection for credit card
					//when the credit card number is invalid or none of the supported types
					//this.showCardsImgs = false;
					//form.find('[data-image="creditcard-icon"]').removeClass('creditcard-edit-form-card-no-selected');
					//form.find('[data-value="creditcard-select-container"]').css('display', 'block');
					//form.find('[data-value="creditcard-img-container"]').css('display', 'none');	

					jQuery('#not-supported').removeClass("hidden");
					jQuery('#in-modal-not-supported').removeClass("hidden");
					jQuery('#ccnumber').select();
					jQuery('#in-modal-ccnumber').select();		
				}	
				else
				{
					form.find('.creditcard-img-container').css('display', 'none');
				}	
			}
		}
	});

    _.extend(CreditCardView.prototype, {
		initialize: function (options)
		{
			BackboneCompositeView.add(this);
			this.options = options;

			if (this.options.showSecurityCodeForm)
			{
				if(this.options.model && this.options.model.get("paymentmethod")){
                    this.ccname = this.options.model.get("paymentmethod").name
				}
				this.model.set('hasSecurityCode', true);



				this.events = {
					'submit form': 'doNothing'
				};

				BackboneFormView.add(this);
			}
		}

		, 	childViews : {
            'CreditCard.Edit.Form.SecurityCode': function () {
            	var cccode = this.model.get('paymentmethod');
                return new CreditCardEditFormSecurityCodeView(
                    {
                        error: this.options.securityNumberError
                        , showCreditCardHelp: this.options.showCreditCardHelp
                        , creditCardHelpTitle: this.options.creditCardHelpTitle
						, ccname : this.ccname
						, value : this.options.ccsecuritycode
                    });
            }
		}
	});

    _.extend(CreditCardEditFormSecurityCodeView.prototype, {

        events: {
			'keyup [data-action="validate-number"]': 'validateNumber'
        },

        validateNumber : function(e){
            if (jQuery(e.target).val() != jQuery(e.target).val().replace(/[^0-9\.]/g, '')) {
                jQuery(e.target).val(jQuery(e.target).val().replace(/[^0-9\.]/g, ''));
            }
		},

		getContext : _.wrap(CreditCardEditFormSecurityCodeView.prototype.getContext, function(fn)
		{
			var currentContext = fn.apply(this, _.toArray(arguments).slice(1));

			var securitycodeLength =  this.options.ccname != "American Express" && this.options.ccname ? 3 : 4;
			var disable_cvv = this.options.ccname ? false : true;

			return _.extend(currentContext, {
                securitycodeLength : securitycodeLength,
                disable_cvv : disable_cvv
			});

		})
	});
 });