/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module LoginRegister
define('LoginRegister.Login.View.Extend'
,	[	
		'LoginRegister.Login.View'
	,	'Backbone'
	,	'underscore'
	,	'Utils'
	]
,	function (
		LoginRegisterView
	,	Backbone
	,	_
	)
{
	'use strict';

	return _.extend(LoginRegisterView.prototype, {

		events: _.extend(LoginRegisterView.prototype.events,{
			'click [data-action="register"]': 'registerNewCustomer'
		})

	,	registerNewCustomer: function()
		{
            var registerURL = window.location.origin+_.getAbsoluteUrl()+"checkout.ssp#register";

            if(jQuery("#register-show-view .login-register-checkout-as-guest-button-show").length > 0) {
				var divExpanded = jQuery("#register-show-view .login-register-checkout-as-guest-button-show").parent().attr("aria-expanded");
                if(!divExpanded )
                	jQuery("#register-show-view .login-register-checkout-as-guest-button-show").click();
            }

			jQuery('.login-register-register-form').has('#register-firstname').length ? jQuery('#register-firstname').focus() : window.location.href=registerURL;
		}
	});
});