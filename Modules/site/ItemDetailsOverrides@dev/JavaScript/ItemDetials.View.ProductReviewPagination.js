/*
 © 2016 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

//@module ItemDetails
define(
    'ItemDetials.View.ProductReviewPagination'
    ,	[
        'ItemDetails.View'

        ,	'Backbone'
        ,	'underscore'
        ,	'jQuery'
        ,   'Utils'
    ]
    ,	function (
        ItemDetailsView

        ,	Backbone
        ,	_
        ,	jQuery
        ,   Utils
    )
    {
        'use strict';

        return _.extend(ItemDetailsView.prototype, {

            initialize: _.wrap(ItemDetailsView.prototype.initialize, function wrapInitialize(fn) {
                fn.apply(this, _.toArray(arguments).slice(1));

                this.application.getLayout().on('afterAppendView', function (){
                    var queryOptions = Utils.parseUrlOptions(location.href);
                    if(queryOptions.first)
                        queryOptions.page = -1;
                    if((queryOptions.page && queryOptions.page != 1 && SC.ENVIRONMENT.scrolled != queryOptions.page) ){
                        SC.ENVIRONMENT.scrolled = queryOptions.page ;
                        $('html,body').animate({
                                scrollTop: $(".product-reviews-center-list").offset().top},
                            'slow');
                    }
                });
            })
        });
    });