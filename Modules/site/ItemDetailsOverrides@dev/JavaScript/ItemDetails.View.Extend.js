/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module ItemDetails
define(
	'ItemDetails.View.Extend'
,	[
		'ItemDetails.View'
	,	'Facets.Translator'
	,	'ItemDetails.Collection'
	,	'ItemDetails.Model'

	,	'item_details.tpl'

	,	'Backbone.CollectionView'
	,	'ItemDetails.ImageGallery.View'
	,	'ItemViews.Option.View'
	,	'ItemViews.Stock.View'

	,	'Backbone'
	,	'Backbone.CompositeView'
	,	'underscore'
	,	'jQuery'

	,	'jquery.zoom'
	,	'jQuery.bxSlider'
	,	'jQuery.scPush'
	,	'jQuery.scSeeMoreLess'
	,	'jQuery.overflowing'
	,	'lity'
	]
,	function (
		ItemDetailsView
	,	FacetsTranslator
	,	ItemDetailsCollection
	,	ItemDetailsModel

	,	item_details_tpl

	,	BackboneCollectionView
	,	ItemDetailsImageGalleryView
	,	ItemViewsOptionView
	,	ItemViewsStockView

	,	Backbone
	,	BackboneCompositeView
	,	_
	,	jQuery
	)
{
	'use strict';

	return _.extend(ItemDetailsView.prototype, {



		updateQuantity: function (e)
		{
			var new_quantity = parseInt(jQuery('[name="quantity"]').val(), 10)
			,	current_quantity = this.model.get('quantity')
			,	isOptimistic = this.application.getConfig('addToCartBehavior') === 'showCartConfirmationModal'
			,	min_quantity =  this.model.get('_minimumQuantity', true);

			if (new_quantity < this.model.get('_minimumQuantity', true))
			{
				if (require('LiveOrder.Model').loadCart().state() === 'resolved') // TODO: resolve error with dependency circular.
				{
					var itemCart = SC.Utils.findItemInCart(this.model, require('LiveOrder.Model').getInstance()) // TODO: resolve error with dependency circular.
					,	total = itemCart && itemCart.get('quantity') || 0;

					if ((total + new_quantity) < this.model.get('_minimumQuantity', true))
					{
						new_quantity = min_quantity;
					}
				}
			}

			jQuery('[name="quantity"]').val(new_quantity);

			if (new_quantity !== current_quantity)
			{
				this.model.setOption('quantity', new_quantity);
				var disabled = new_quantity == 1;
                $(".item-details-quantity-remove").prop( "disabled", disabled );
			}

			if (this.$containerModal)
			{
				//this.refreshInterface(e);

				// need to trigger an afterAppendView event here because, like in the PDP, we are really appending a new view for the new selected matrix child
				this.application.getLayout().trigger('afterAppendView', this);
			}
		}
	});
});	