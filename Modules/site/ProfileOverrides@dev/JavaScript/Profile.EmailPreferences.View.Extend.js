/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

define('Profile.EmailPreferences.View.Extend'
,	[	'Backbone'
	,	'Profile.EmailPreferences.View'
	,	'underscore'
	,	'jQuery'
	]
,	function (
		Backbone
	,	ProfileEmailPreferences
	,	_
	,	jQuery
	)
{
	'use strict';

	return _.extend(ProfileEmailPreferences.prototype, {
		saveForm: function (e, model, props)
		{
			e.preventDefault();

			model = model || this.model;

			this.$savingForm = jQuery(e.target).closest('form');

			if (this.$savingForm.length)
			{
				// Disables all for submit buttons, to prevent double submitions
				this.$savingForm.find('input[type="submit"], button[type="submit"]').attr('disabled', true);
				// and hides reset buttons
				//this.$savingForm.find('input[type="reset"], button[type="reset"]').hide();
			}

			this.hideError();

			var self = this;

			// Returns the promise of the save acction of the model
			return model.save(props || this.$savingForm.serializeObject(), {

					wait: true

					// Hides error messages, re enables buttons and triggers the save event
					// if we are in a modal this also closes it
				,	success: function (model, response)
					{
						if (self.inModal && self.$containerModal)
						{
							self.$containerModal.modal('hide');
						}

						if (self.$savingForm.length)
						{
							self.hideError(self.$savingForm);
							self.$savingForm.find('[type="submit"], [type="reset"]').attr('disabled', false);
							model.trigger('save', model, response);
						}
					}

					// Re enables all button and shows an error message
				,	error: function (model, response)
					{
						self.$savingForm.find('*[type=submit], *[type=reset]').attr('disabled', false);

						if (response.responseText)
						{
							model.trigger('error', jQuery.parseJSON(response.responseText));
						}
					}
				}
			);
		},

        toggleReset: _.debounce(function (e)
        {
            var $form = jQuery(e.target).closest('form')
                ,	model = this.model
                ,	attribute, value

                // look for the changed fields
                ,	fields_changed = _.filter($form.serializeObject(), function (item, key)
                {
                    attribute = model.get(key);
                    value = jQuery.trim(item);

                    return attribute ? attribute !== value : !!value;
                });

            return this;
        },300)

	});
});