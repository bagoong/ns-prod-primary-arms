define('ProductReviews.FormPreview.View.Extend'
,	[
		'ProductReviews.FormPreview.View'
	,	'Profile.Model'
	,	'ProductReviews.Collection'
	,	'ProductReviews.FormConfirmation.View'
	,	'ProductReviews.Preview.View'

	,	'product_reviews_form_preview.tpl'
	]
, function(
		ProductReviewsFormPreviewView
	,	ProfileModel
	,	ProductReviewsCollection
	,	ProductReviewsFormConfirmationView
	,	ProductReviewsPreview

	,	product_reviews_form_preview
	)
{
	'use strict';

	return _.extend(ProductReviewsFormPreviewView.prototype, {

        edit : _.wrap(ProductReviewsFormPreviewView.prototype.edit, function(fn){
            fn.apply(this, _.toArray(arguments).slice(1));

            jQuery('#text').select().focus();
        })

	});
});
