/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module ProductReviews
define('ProductReviews.Center.View.Extend'
,	[
		'ProductReviews.Center.View'
	,	'SC.Configuration'
	,	'ListHeader.View'
	,	'GlobalViews.Pagination.View'

	,	'underscore'
	,	'jQuery'
	,	'Backbone'
	,	'Utils'
	]
, function (
        ProductReviewsCenterView
	,	Configuration
	,	ListHeaderView
	,	GlobalViewsPaginationView
	,	_
	,	jQuery
	,	Backbone
	)
{
	'use strict';

	// @class ProductReviews.Center.View
	// This view is shown when listing the reviews of an item contains event handlers for voting helpfulness and flaging a review
	// @extends Backbone.View
	return _.extend(ProductReviewsCenterView.prototype ,  {


        childViews: _.extend(ProductReviewsCenterView.prototype.childViews, {

			'GlobalViews.Pagination': function()
			{
				return new GlobalViewsPaginationView(_.extend({
					currentPage: this.collection.page || 0
				,	totalPages: this.collection.totalPages || 0
				,	pager: function (page)
					{
						var fragment = Backbone.history.fragment;
						if(page > 1){
                            fragment = _.removeUrlParameter(fragment, 'first');
							fragment = _.setUrlParameter(fragment, 'page', page);
						}else{
                            fragment = _.removeUrlParameter(fragment, 'page');
                            fragment = _.setUrlParameter(fragment, 'first', true);
						}
						return '/'+ fragment;
					}
				,	extraClass: 'pull-right no-margin-top no-margin-bottom'
				},	Configuration.get('defaultPaginationSettings')));
			}

		,	'ListHeader.View': function()
			{
				var ListHeaderViewIntance = new ListHeaderView(this.setupListHeader(this.collection));
				ListHeaderViewIntance.pager = function (page)
                {
                    var fragment = Backbone.history.fragment;
                    if(page > 1){
                        fragment = _.removeUrlParameter(fragment, 'first');
                        fragment = _.setUrlParameter(fragment, 'page', page);
                    }else{
                        fragment = _.removeUrlParameter(fragment, 'page');
                        fragment = _.setUrlParameter(fragment, 'first', true);
                    }
                    return '/'+ fragment;
                };
				return ListHeaderViewIntance;
			}
		})
    })
});
