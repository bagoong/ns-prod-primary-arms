define('ProductReviews.Form.View.Extend'
    ,	[
        'ProductReviews.Form.View'
        ,	'SC.Configuration'
        ,	'Profile.Model'
        ,	'ProductReviews.Collection'
        ,	'ProductReviews.FormPreview.View'
        ,	'ProductReviews.FormConfirmation.View'

        ,	'product_reviews_form.tpl'
    ]
    , function(
        ProductReviewsFormView
        ,	Configuration
        ,	ProfileModel
        ,	ProductReviewsCollection
        ,	ProductReviewsFormPreviewView
        ,	ProductReviewsFormConfirmationView

        ,	product_reviews_form
    )
    {
        'use strict';

        // @class ProductReviews.Form.View
        // This view is used to render the Product Review form. It handles the rating and submission of the review
        // @extends Backbone.View
        return _.extend(ProductReviewsFormView.prototype, {

            showContent : _.wrap(ProductReviewsFormView.prototype.showContent, function(fn){
                fn.apply(this, _.toArray(arguments).slice(1));

                if (this.isLoggedIn) {
                    jQuery('#text').focus();
                } else {
                    jQuery('#writerName').focus();
                }
            })

        });
    });
