//@module OrderWizard.Module.PaymentMethod
define(
    'OrderWizard.Module.PaymentMethod.Creditcard.Extend'
    ,	[
        'OrderWizard.Module.PaymentMethod.Creditcard'
        ,   "SC.Configuration"
        ,	'CreditCard.View'
        ,	'Backbone'
        ,	'underscore'
    ]
    ,	function (
        OrderWizardModulePaymentMethodCreditCard
        ,   Configuration
        ,	CreditCardView
        ,	Backbone
        ,	_
    ) {
        return _.extend(OrderWizardModulePaymentMethodCreditCard.prototype, {
            childViews: _.extend(OrderWizardModulePaymentMethodCreditCard.prototype.childViews, {
                'SelectedCreditCard': function ()
                {
                    return new CreditCardView({
                        model: this.creditcard
                        ,	showSecurityCodeForm: this.requireccsecuritycode
                        ,	securityNumberError: this.isSecurityNumberInvalid && this.securityNumberErrorMessage
                        ,	ccsecuritycode: !this.ccsecuritycode && this.paymentMethod.get('creditcard') ? this.paymentMethod.get('creditcard').ccsecuritycode : this.ccsecuritycode
                        ,	showCreditCardHelp: Configuration.get('showCreditCardHelp', true)
                        ,	creditCardHelpTitle: Configuration.get('creditCardHelpTitle', true)
                        ,	collapseElements: Configuration.get('collapseElements', true)
                    });
                }
            })
        })
    });