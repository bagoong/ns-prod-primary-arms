/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module CreditCard
// Model for handling credit cards (CRUD)
define('CreditCard.Model.Extend'
,	[	'CreditCard.Model'
	,	'SC.Configuration'
	,	'Backbone'
	,	'underscore'
	,	'jQuery'
	,	'Utils'
	]
,	function(
        CreditCardModel
	,	Configuration
	,	Backbone
	,	_
	,	jQuery
	)
{
	'use strict';


	// @class CreditCard.Model responsible of implement the REST model of a credit card and of validating 
	// the credit card properties @extends Backbone.Model
	return _.extend(CreditCardModel.prototype, {

		//@property validation. Backbone.Validation attribute used for validating the form before submit.
		validation: _.extend(CreditCardModel.prototype.validation, {
				ccnumber: {	fn: function (cc_number, attr, form)
				{
					
					// credit card number validation
					// It validates that the number pass the Luhn test and also that it has the right starting digits that identify the card issuer
					if (!cc_number)
					{
                        jQuery('#ccsecuritycode').prop("disabled", true)
                        jQuery('#ccsecuritycode').val("");
						return _('Card Number is required').translate();
					}

					if (_.isUndefined(form.internalid))
					{
						cc_number = cc_number.replace(/\s/g, '');

						//check Luhn Algorithm
						var card_number_is_valid = _(cc_number.split('').reverse()).reduce(function (a, n, index)
							{
								return a + _((+n * [1, 2][index % 2]).toString().split('')).reduce(function (b, o)
									{ return b + (+o); }, 0);
							}, 0) % 10 === 0;

						if (!card_number_is_valid)
						{
                            jQuery('#ccsecuritycode').val("");
                            jQuery('#ccsecuritycode').prop("disabled", true)
							// we throw an error if the number fails the regex or the Luhn algorithm
							return _('Credit Card Number is invalid').translate();
						}
					}
				}
			},

            ccsecuritycode: { fn: function (cc_security_code)
            {
                if  (SC.ENVIRONMENT.siteSettings.checkout.requireccsecuritycode === 'T' && (this.hasSecurityCode || this.get('hasSecurityCode')))
                {
                    var errorMessage = _.validateSecurityCode(cc_security_code);
                    if (errorMessage)
                    {
                        return errorMessage;
                    } else {

                        var isAmerican = false;

                        if(this.get("paymentmethod")){
                            isAmerican = this.get("paymentmethod").name == "American Express";
                        }else if(cc_security_code){

                            if($(".creditcard-header-icon").attr("alt")){
                                isAmerican = $(".creditcard-header-icon").attr("alt") == "American Express";
                            } else {
                                var ccnumber;

                                if (this.get("ccnumber"))
                                    ccnumber = this.get("ccnumber")
                                else
                                    ccnumber = $(".creditcard-edit-form-input").val();

                                if(ccnumber) {
                                    var payment_methods_configuration = Configuration.get('paymentmethods');
                                    // get the credit card key
                                    var paymenthod_key;
                                    var self = this;

                                    // validate that the number and issuer
                                    _.each(payment_methods_configuration, function (payment_method_configuration) {
                                        if (payment_method_configuration.regex && RegExp(payment_method_configuration.regex).test(ccnumber)) {
                                            paymenthod_key = payment_method_configuration.key;
                                        }
                                    });

                                    var paymentmethod = paymenthod_key && _.findWhere(Configuration.get('siteSettings.paymentmethods'), {key: paymenthod_key});
                                    if (paymentmethod && paymentmethod.name == "American Express") {
                                        isAmerican = true;
                                    }
                                }
                            }
                        }

                        if ( isAmerican && jQuery.trim(cc_security_code).length != 4) {
                            return _('Security Number is invalid').translate();
                        }

                    }
                }
            }
            }
		})
	});
});
