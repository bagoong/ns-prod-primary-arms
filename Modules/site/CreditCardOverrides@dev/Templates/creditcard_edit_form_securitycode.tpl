{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="creditcard-edit-form-securitycode">
	<div class="creditcard-edit-form-securitycode-group" data-input="ccsecuritycode" data-validation="control-group">
		<label class="creditcard-edit-form-securitycode-group-label" for="ccsecuritycode">
			{{translate 'Security Number'}} <span class="creditcard-edit-form-securitycode-group-label-required">*</span>
		</label>

		<div class="creditcard-edit-form-securitycode-controls" data-validation="control">
			<!-- this is for CVV field accepting alpahbet characters
			<input type="text" class="creditcard-edit-form-securitycode-group-input" id="ccsecuritycode" name="ccsecuritycode" value="{{value}}" maxlength="{{securitycodeLength}}">
			-->

			<input type="text" data-action="validate-number" class="creditcard-edit-form-securitycode-group-input" id="ccsecuritycode"  name="ccsecuritycode" value="{{value}}" maxlength="{{securitycodeLength}}" {{#if disable_cvv}} disabled {{/if}} >

			<a href="#" class="creditcard-edit-form-securitycode-link">
				<span class="creditcard-edit-form-securitycode-icon-container">
					<i class="creditcard-edit-form-securitycode-icon" data-toggle="popover" data-placement="bottom" data-title="{{creditCardHelpTitle}}"/>
				</span>
			</a>
			<!-- this is for displaying a message that the security number is invalid in case it first inputs the
			  -- security code before the credit card number and it does not match the length of the corresponding
			  -- card to it
			<p id="incorrect-sec-num" data-validation-error="block">Security number is invalid.</p>-->
			<!--onkeyup="this.value=this.value.replace(/[^\d]/,'')"-->
		</div>
	</div>
</div>