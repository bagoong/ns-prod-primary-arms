/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Wizard
define('Wizard.Step.Extend'
,	[	'Wizard.Step'
	,	'Backbone'
	,	'underscore'
	,	'jQuery'

	,	'order_wizard_step.tpl'
	,	'wizard_step.tpl'
	,	'GlobalViews.Message.View'

	,	'Backbone.CompositeView'
	,	'Utils'
	]
,	function (
		WizardStep
	,	Backbone
	,	_
	,	jQuery

	,	order_wizard_step_tpl
	,	wizard_step_tpl
	,	GlobalViewsMessageView

	,	BackboneCompositeView
	)
{
	'use strict';

	return _.extend(WizardStep.prototype, {

		submitErrorHandler: function (error)
		{
			this.enableNavButtons();
			this.wizard.manageError(error, this);

			_.each(this.moduleInstances, function (module_instance)
			{
				module_instance.enableInterface();
			});

			if($(".global-views-message-error").length > 0 && $(".global-views-message-error").length != $(".global-views-message-error [hidden]").length ) {
                $('html, body').animate({
                    scrollTop: $(".global-views-message-error").offset().top
                }, 500);
            }else if($("p[data-validation-error]").length > 1 ){
                $('html, body').animate({
                    scrollTop: $("p[data-validation-error]").closest("div[data-validation*='control-group']").offset().top
                }, 500);
			} else {
                this.error = error;
                this.showError();
			}
		},

		submit : _.wrap(WizardStep.prototype.submit, function(fn){
            if($("p[data-validation-error]").length > 1){ //grated than 1 as there is an error message that is always there
                $('html, body').animate({
                    scrollTop: $("p[data-validation-error]").closest("div[data-validation*='control-group']").offset().top
                }, 500);
			}
            else
            	fn.apply(this, _.toArray(arguments).slice(1));
		})


	});
});