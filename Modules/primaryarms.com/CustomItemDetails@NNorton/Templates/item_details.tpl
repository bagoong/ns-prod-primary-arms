{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}
<div class="item-details">
  <div data-cms-area="item_details_banner" data-cms-area-filters="page_type"></div>
  <header class="item-details-header">
    <div id="banner-content-top" class="item-details-banner-top"></div>
  </header>
  <div class="item-details-divider-desktop"></div>
  <article class="item-details-content" itemscope itemtype="http://schema.org/Product">
    <meta itemprop="url" content="{{model._url}}">
    <div id="banner-details-top" class="item-details-banner-top-details"></div>
    <section class="item-details-main-content">
      <div class="item-details-content-header">
        <h1 class="item-details-content-header-title" itemprop="name">{{model._pageTitle}}</h1>
        <div class="item-details-sku-container"> <span class="item-details-sku"> {{translate 'SKU:'}} </span> <span class="item-details-sku-value" itemprop="sku"> {{sku}} </span> </div>
        <div class="item-details-manufacturer-container">
          <div class="item-details-manufacturer">{{translate 'Manufactured by '}}{{manufacturer}}</div>
        </div>
        {{#if showReviews}}
        <div class="item-details-rating-header" {{#if hasRating}}itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating"{{/if}}>
          <div class="item-details-rating-header-rating" data-view="Global.StarRating"></div>
        </div>
        {{/if}}
        <div data-cms-area="item_info" data-cms-area-filters="path"></div>
      </div>
      <div class="item-details-divider"></div>
      <div class="item-details-image-gallery-container"> {{#if ffl}}
        <div id="restricted-indicator" class="item-restricted-indicator"> FFL </div>
        {{/if}}
        {{#if nfa}}
        <div id="restricted-indicator" class="item-restricted-indicator"> NFA </div>
        {{/if}}
        <div id="banner-image-top" class="content-banner banner-image-top"></div>
        <div data-view="ItemDetails.ImageGallery"></div>
        
        <!--Video location for desktop--> 
        {{#if youTube}}
        <div class="item-details-video-container-top">
          <ul class="bxslider" data-slider>
            {{{youTube}}} 
            
            <!--This is the Layout for the videos--> 
            <!--<li data-lity> <div class="item-details-video-col">
            		<div class="item-details-video" href="https://www.youtube-nocookie.com/embed/UdPemw8CVSU?rel=0&amp;showinfo=1" data-lity>
                    	<iframe class="item-details-video-no-play" width="120" height="65" src="https://www.youtube-nocookie.com/embed/UdPemw8CVSU?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                    </div>Test Video</div></li>-->
          </ul>
        </div>
        {{/if}}
        <div id="inline" style="background:#fff" class="lity-hide">Inline content</div>
        <div id="banner-image-bottom" class="content-banner banner-image-bottom"></div>
      </div>
      <div class="item-details-divider"></div>
      <div class="item-details-main">
        <section class="item-details-info">
          <div id="banner-summary-bottom" class="item-details-banner-summary-bottom"></div>
          <div data-view="Item.Price"></div>
          <div data-view="Item.Stock"></div>
          {{#if limits}}
          	<div class="item-details-limit-message"> <strong>NOTE: Limit {{limits}} per household. Quantities exceeding {{limits}} will not allow checkout.</strong> </div>
          {{/if}}
        </section>
        {{#if isPurchasable}}
            <section class="item-details-ship-fast-message">
                "We ship fast!" Order by 5PM EST M-F or 12PM EST Saturday and your order ships same day. FFL items may take longer to process.
            </section>
        {{/if}}
        {{#if showRequiredReference}}
        	<div class="item-details-text-required-reference-container"> <small>Required <span class="item-details-text-required-reference">*</span></small> </div>
        {{/if}}
        <section class="item-details-options">
            {{#if isItemProperlyConfigured}}
              {{#if hasAvailableOptions}}
                <button class="item-details-options-pusher" data-type="sc-pusher" data-target="item-details-options">
                    {{#if isReadyForCart}}{{ translate 'Options' }}{{else}}{{ translate 'Select options' }}{{/if}}
                    {{#if hasSelectedOptions}}:{{/if}} <i></i>
                    {{#each selectedOptions}}
                        {{#if @first}} <span> {{ label }} </span> {{else}} <span> , {{ label }} </span> {{/if}}
                    {{/each}} </button>
              {{/if}}
              <div class="item-details-options-content" data-action="pushable" data-id="item-details-options">
                <div class="item-details-options-content-price" data-view="Item.Price"></div>
                <div class="item-details-options-content-stock"  data-view="Item.Stock"></div>
                <div data-view="ItemDetails.Options"></div>
              </div>
              {{else}}
              <div class="alert alert-error"> {{translate '<b>Warning</b>: This item is not properly configured, please contact your administrator.'}} </div>
            {{/if}}
        </section>
        {{#if isPriceEnabled}}
        {{#if isItemProperlyConfigured}}
        <section class="item-details-actions row">
          <form action="#" class="item-details-add-to-cart-form" data-validation="control-group">
            {{#if hasAddonOptions}}
            <div class="item-details-content-related-items">
              <div data-view="AddonOptions.Items"></div>
            </div>
            {{/if}} 
            {{#unless isReadyForCart}}
            {{#if showSelectOptionMessage}}
            <p class="item-details-add-to-cart-help"> <i class="item-details-add-to-cart-help-icon"></i> <span class="item-details-add-to-cart-help-text">{{translate 'Please select options before adding to cart'}}</span> </p>
            {{/if}}
            {{/unless}}
            
            {{#if showQuantity}}
            	<input type="hidden" name="quantity" id="quantity" value="1">
            {{else}}
                {{#if isPurchasable}}
                    <div class="item-details-options-quantity col-xs-6 col-xs-offset-3 col-sm-3 col-sm-offset-0" data-validation="control">
                      <label for="quantity" class="item-details-options-quantity-title"> {{translate 'Quantity'}} </label>
                      <button class="item-details-quantity-remove" data-action="minus" {{#if isMinusButtonDisabled}}disabled{{/if}}>-</button>
                      <input class="item-details-quantity-value" type="number" name="quantity" id="quantity"  value="{{quantity}}" min="1">
                      <button class="item-details-quantity-add" data-action="plus">+</button>
                      
                      {{#if showMinimumQuantity}}
                        <small class="item-details-options-quantity-title-help"> {{translate 'Minimum of $(0) required' minQuantity}} </small>
                      {{/if}}
                    </div>
                {{/if}}
            {{/if}}
            <div class="item-details-actions-container {{#if isPurchasable}}col-sm-9{{else}}col-sm-12{{/if}}">
                {{#if isPurchasable}} 
                    <!--Add to Cart button-->
                    <div class="item-details-add-to-cart">
                    	<button data-type="add-to-cart" data-action="sticky" class="item-details-add-to-cart-button" {{#unless isReadyForCart}}disabled{{/unless}}><i class="fa fa-shopping-cart" aria-hidden="true"></i> {{translate 'Add to Cart'}} </button>
                    </div>
                {{else}} 
                    <!--Email me when back in stock button-->
                    <div class="item-details-back-in-stock">
                    	<button data-type="" data-action="trigger-email" class="item-details-back-in-stock-button"> {{translate 'Email Me When In Stock'}} </button>
                    </div>
                {{/if}}
                  
                {{#if isReadyForWishList}}
                    <div class="item-details-add-to-wishlist" data-type="product-lists-control"></div>
                {{else}}
                    <div class="item-details-add-to-wishlist" data-type="product-lists-control" data-disabledbutton="true"></div>
                {{/if}}
            </div>
          </form>
          <div data-type="alert-placeholder"></div>
        </section>
        {{/if}}
        {{/if}}
        {{#if restrictionMessage}}
        <div class="item-details-message"> <a class="item-details-message-head collapsed" data-toggle="collapse" data-target="#accordion-id" aria-expanded="false" aria-controls="accordion-id"> <i class="fa fa-exclamation-triangle"></i> This item is regulated. <i class="acordion-head-toggle-icon"></i> </a> </div>
        <div class="item-details-message-content collapse" id="accordion-id">
          <p>{{{restrictionMessage}}}</p>
        </div>
        {{/if}}
        <div class="item-details-main-bottom-banner">
          <div data-view="SocialSharing.Flyout"></div>
          <div id="banner-summary-bottom" class="item-details-banner-summary-bottom"></div>
        </div>
        <div id="banner-details-bottom" class="item-details-banner-details-bottom" data-cms-area="item_info_bottom" data-cms-area-filters="page_type"></div>
      </div>
    </section>
    <div class="row">
      <div class="item-details-more-info-content col-sm-12 col-md-7"> {{#if showDetails}}
        {{#each details}}
        {{!-- Mobile buttons --}}
        <button class="item-details-info-pusher" data-target="item-details-info-{{ @index }}" data-type="sc-pusher">
        <h2 class="item-details-description-header">{{ name }}</h2>
        <i></i>
        <p class="item-details-info-hint"> {{{trimHtml content 150}}} </p>
        </button>
        {{/each}}
        <div class="item-details-more-info-content-container">
          <div id="banner-content-top" class="content-banner banner-content-top"></div>
          <div role="tabpanel"> {{!-- When more than one detail is shown, these are the tab headers  --}}
            <ul class="item-details-more-info-content-tabs" role="tablist">
              {{#each details}}
              <li class="item-details-tab-title {{#if @first}} active {{/if}}" role="presentation"> <a href="#" data-target="#item-details-info-tab-{{@index}}" data-toggle="tab">{{name}}</a> </li>
              {{/each}}
            </ul>
            {{!-- Tab Contents --}}
            <div class="item-details-tab-content" > {{#each details}}
              <div role="tabpanel" class="item-details-tab-content-panel {{#if @first}}active{{/if}}" id="item-details-info-tab-{{@index}}" itemprop="{{itemprop}}" data-action="pushable" data-id="item-details-info-{{ @index }}">
                <h2 class="item-details-description-header">{{name}}</h2>
                <div id="item-details-content-container-{{@index}}">{{{content}}}</div>
              </div>
              {{/each}}
              <div class="item-details-action"> <a href="#" class="item-details-more" data-action="show-more">{{translate 'See More'}}</a> <a href="#" class="item-details-less" data-action="show-more">{{translate 'See Less'}}</a> </div>
            </div>
          </div>
          <div id="banner-content-bottom" class="content-banner banner-content-bottom"></div>
        </div>
        {{/if}} 
        <!--Manual location for desktop--> 
        {{#if manualUrl1}}
        <div class="item-details-manual-desktop">
          <div href="{{manualUrl1}}" data-lity><i id="manualicon" class="fa fa-file-pdf-o"></i> {{sku}} <span id="manual">{{translate 'MANUAL'}}{{#if manualUrl2}} 1{{/if}}</span></div>
        </div>
        {{/if}}
        {{#if manualUrl2}}
        <div class="item-details-manual-desktop">
          <div href="{{manualUrl2}}" data-lity><i id="manualicon" class="fa fa-file-pdf-o"></i> {{sku}} <span id="manual">{{translate 'MANUAL'}} 2</span></div>
        </div>
        {{/if}}
        {{#if manualUrl3}}
        <div class="item-details-manual-desktop">
          <div href="{{manualUrl3}}" data-lity><i id="manualicon" class="fa fa-file-pdf-o"></i> {{sku}} <span id="manual">{{translate 'MANUAL'}} 3</span></div>
        </div>
        {{/if}} </div>
      <div class="item-details-attributes-content col-sm-12 col-md-5">
        <div class="item-details-attributes-container">
          <button class="item-details-attributes-pusher" data-target="item-details-attributes" data-type="sc-pusher">
          <h2 class="item-details-attributes-header">{{translate 'Specifications'}}</h2>
          <i></i> </button>
          <div class="item-details-attributes" data-action="pushable" data-id="item-details-attributes">
            <h2 class="item-details-attributes-header">{{translate 'Specifications'}}</h2>
            {{#if wsatt}}
            {{#each wsatt.attributes}}
            <div id="attribute-row"> <span id="attribute-name">{{attribute}}</span> <span id="attribute-value">{{value}}</span> </div>
            {{/each}}
            {{/if}} </div>
        </div>
        
        <!--Manual location for mobile--> 
        {{#if manualUrl1}}
        <div class="item-details-manual-mobile">
          <div href="{{manualUrl1}}" data-lity><i id="manualicon" class="fa fa-file-pdf-o"></i> {{sku}} <span id="manual">{{translate 'MANUAL'}}{{#if manualUrl2}} 1{{/if}}</span></div>
        </div>
        {{/if}}
        {{#if manualUrl2}}
        <div class="item-details-manual-mobile">
          <div href="{{manualUrl2}}" data-lity><i id="manualicon" class="fa fa-file-pdf-o"></i> {{sku}} <span id="manual">{{translate 'MANUAL'}} 2</span></div>
        </div>
        {{/if}}
        {{#if manualUrl3}}
        <div class="item-details-manual-mobile">
          <div href="{{manualUrl3}}" data-lity><i id="manualicon" class="fa fa-file-pdf-o"></i> {{sku}} <span id="manual">{{translate 'MANUAL'}} 3</span></div>
        </div>
        {{/if}}
      </div>
      <!--Video location for mobile--> 
      {{#if youTube}}
      <div class="item-details-video-container-bottom">
        <ul class="bxslider" data-slider>
          {{{youTube}}}
        </ul>
      </div>
      {{/if}} </div>
    <div class="item-details-divider-desktop"></div>
    <section class="item-details-product-review-content" > {{#if showReviews}}
      <button class="item-details-product-review-pusher" data-target="item-details-review" data-type="sc-pusher">{{ translate 'Reviews' }}
      <div class="item-details-product-review-pusher-rating" data-view="Global.StarRating"></div>
      <i></i> </button>
      <div class="item-details-more-info-content-container" data-action="pushable" data-id="item-details-review">
        <div data-view="ProductReviews.Center"></div>
      </div>
      {{/if}} </section>
    <div class="item-details-content-related-items">
      <div data-view="Related.Items"></div>
    </div>
    <div class="item-details-content-correlated-items">
      <div data-view="Correlated.Items"></div>
    </div>
    <div id="banner-details-bottom" class="content-banner banner-details-bottom" data-cms-area="item_details_banner_bottom" data-cms-area-filters="page_type"></div>
  </article>
</div>

<!-- Modal -->
<div class="modal fade" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="emailModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="row">
          <div class="col-sm-6">
            <h4 class="modal-title" id="emailModalLabel">Email Me When In Stock</h4>
          </div>
          <div class="col-sm-6" style="text-align: right;">
            <button type="button" class="global-views-modal-content-header-close" data-dismiss="modal" aria-hidden="true" aria-label="Close"> × </button>
          </div>
        </div>
      </div>
      <div class="modal-body">
        <div class="email-message" id="email-message"></div>
        <div class="email-form form-group" id="email-form">
          <label for="email-input">Enter your email address:</label>
          <input type="email" class="email-input" id="email-input" name="email">
          <p>
            <input class="bis-mailing-list-check" id="bis-mailing-list-check" type="checkbox" checked>
            <span>Yes, I would like to receive special offers, and to hear about new products and brands.</span>
          <p>
            <button data-type="" data-action="send-email" class="item-details-send-button email-send" id="email-send">Send</button>
            <img class="email-sign-up-direct-add" src="" width="0" height="0" border="0" alt="" style="display: none"/> 
        </div>
      </div>
      <div class="modal-footer"> </div>
    </div>
  </div>
</div>
