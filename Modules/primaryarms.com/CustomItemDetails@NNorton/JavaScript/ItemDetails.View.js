/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module ItemDetails
define(
	'ItemDetails.View'
,	[
		'Facets.Translator'
	,	'ItemDetails.Collection'
	,	'ItemDetails.Model'

	,	'item_details.tpl'
	,	'item_details_gift_certificate.tpl'
	,	'quick_view.tpl'

	,	'Backbone.CollectionView'
	,	'ItemDetails.ImageGallery.View'
	,	'ItemViews.Price.View'
	,	'GlobalViews.StarRating.View'
	,	'ProductReviews.Center.View'
	,	'ItemViews.Option.View'
	,	'ItemViews.Stock.View'
	,	'ItemViews.RelatedItem.View'
	,	'ItemRelations.Correlated.View'
	,	'ItemRelations.Related.View'
	,	'SocialSharing.Flyout.View'
	,	'Profile.Model'
	,	'Tracker'
	,	'RecentlyViewedItems.View'
	,	'AddonOptionsItems.View'

	,	'SC.Configuration'
	,	'Backbone'
	,	'Backbone.CompositeView'
	,	'underscore'
	,	'jQuery'
	,	'RecentlyViewedItems.Collection'
	,	'LiveOrder.Model'
	,	'Utils'
	,	'EmailBackInStock.Model'

	,	'jquery.zoom'
	,	'jQuery.bxSlider'
	,	'jQuery.scPush'
	,	'jQuery.scSeeMoreLess'
	,	'jQuery.overflowing'
	,	'lity'
	]
,	function (
		FacetsTranslator
	,	ItemDetailsCollection
	,	ItemDetailsModel

	,	item_details_tpl
	,	item_details_gift_certificate
	,	quick_view_tpl

	,	BackboneCollectionView
	,	ItemDetailsImageGalleryView
	,	ItemViewsPriceView
	,	GlobalViewsStarRatingView
	,	ProductReviewsCenterView
	,	ItemViewsOptionView
	,	ItemViewsStockView
	,	ItemViewsRelatedItemView
	,	ItemRelationsCorrelatedView
	,	ItemRelationsRelatedView
	,	SocialSharingFlyoutView
	,	ProfileModel
	,	Tracker
	,	RecentlyViewedItemsView
	,	AddonOptionsItemsView

	,	Configuration
	,	Backbone
	,	BackboneCompositeView
	,	_
	,	jQuery
	,	RecentlyViewedItemsCollection
	,	LiveOrderModel
	,	Utils
	,	EmailBackInStock
	)
{
	'use strict';

	var colapsibles_states = {}

	//@class ItemDetails.View Handles the PDP and quick view @extend Backbone.View
	return Backbone.View.extend({

		//@property {String} title
		title: ''

		//@property {String} page_header
	,	page_header: ''

		//@property {Function} template
	,	template: item_details_tpl

		//@property {String} modalClass
	,	modalClass: 'global-views-modal-large'

		//@property {Boolean} showModalPageHeader
	,	showModalPageHeader: false

		//@property {Object} attributes List of HTML attributes applied by Backbone into the $el
	,	attributes: {
			'id': 'product-detail'
		,	'class': 'view product-detail'
		}

		//TODO ALWAYS USE [data-action=""] WHEN ATTACHING EVENTS INTO THE DOM!
		//@property {Object} events
	,	events: {
			'blur [data-toggle="text-option"]': 'setOption'
		,	'click [data-toggle="set-option"]': 'setOption'
		,	'change [data-toggle="select-option"]': 'setOption'

		,	'keydown [data-toggle="text-option"]': 'tabNavigationFix'
		,	'focus [data-toggle="text-option"]': 'tabNavigationFix'

		,	'click [data-action="plus"]': 'addQuantity'
		,	'click [data-action="minus"]': 'subQuantity'

		,	'change [name="quantity"]': 'updateQuantity'
		,	'keypress [name="quantity"]': 'submitOnEnter'

		,	'click [data-type="add-to-cart"]': 'addToCart'

		,	'shown .collapse': 'storeColapsiblesState'
		,	'hidden .collapse': 'storeColapsiblesState'

		,	'click [data-action="show-more"]': 'showMore'
		
		,	'click [data-type="add-option-to-cart"]': 'addOptionToCart'
		,	'click [data-action="trigger-email"]': 'captureEmail'
		,	'click [data-action="trigger-email-quick-view"]': 'captureEmailQuickView'


		,	'click [data-action="send-email"]': 'sendEmail'
		,	'focus #email-input': 'clearMessage'

		}

		//@method initialize
		//@param {ItemDetails.View.Initialize.Parameters}
		//@return {Void}
	,	initialize: function (options)
		{
			var self = this; 
			var bxOptions =	{
					slideWidth: 120
				,	minSlides: 2
				,	maxSlides: 10
				,	moveSlides: 1
				,	slideMargin: 10
				,	forceStart: false
				,	pager: false
				,	touchEnabled:true
				,	nextText: '<a class="item-details-carousel-next"><span class="control-text">' + _('next').translate() + '</span> <i class="carousel-next-arrow"></i></a>'
				,	prevText: '<a class="item-details-carousel-prev"><i class="carousel-prev-arrow"></i> <span class="control-text">' + _('prev').translate() + '</span></a>'
				,	controls: true
			};
			
			this.on('afterViewRender', function()
			{
				_.initBxSlider(self.$('[data-slider]'), bxOptions);
			});
			
			this.application = options.application;
			this.reviews_enabled = SC.ENVIRONMENT.REVIEWS_CONFIG && SC.ENVIRONMENT.REVIEWS_CONFIG.enabled;

			BackboneCompositeView.add(this);
			Backbone.Validation.bind(this);

			if (!this.model)
			{
				throw new Error('A model is needed');
			}
			
			this.discountOptionsGroups = [1, 2, 3, 4];
			
			//Change the template if this is a gift certificate
			this.once('afterViewRender', function(){
				if (self.model.get('itemtype') === 'GiftCert'){
					self.template = item_details_gift_certificate;
					this.render();
				}
			});
		}

		// @method getBreadcrumbPages Returns the breadcrumb for the current page based on the current item
		// @return {Array<BreadcrumbPage>} breadcrumb pages
	,	getBreadcrumbPages: function ()
		{
			return this.model.get('_breadcrumb');
		}

		//@property {Object} childViews
	,	childViews: {
			'ItemDetails.ImageGallery': function ()
			{
				return new ItemDetailsImageGalleryView({images: this.model.get('_images', true)});
			}
		,	'Item.Price': function ()
			{
				return new ItemViewsPriceView({
					model: this.model
				,	origin: this.inModal ? 'PDPQUICK' : 'PDPFULL'
				});
			}
		,	'Global.StarRating': function ()
			{
				return new GlobalViewsStarRatingView({
					model: this.model
				,	showRatingCount: true
				});
			}
		,	'ProductReviews.Center': function ()
			{
				return new ProductReviewsCenterView({ item: this.model, application: this.application });
			}
		,	'ItemDetails.Options': function ()
			{
				var options_to_render = this.model.getPosibleOptions();

				_.each(options_to_render, function (option)
				{
					// If it's a matrix it checks for valid combinations
					if (option.isMatrixDimension)
					{
						var available = this.model.getValidOptionsFor(option.itemOptionId);
						_.each(option.values, function (value)
						{
							value.isAvailable = _.contains(available, value.label);
						});
					}
				}, this);

				return new BackboneCollectionView({
					collection: new Backbone.Collection(options_to_render)
				,	childView: ItemViewsOptionView
				,	viewsPerRow: 1
				,	childViewOptions: {
						item: this.model
					}
				});
			}
		,	'Item.Stock': function ()
			{
				return new ItemViewsStockView({model: this.model});
			}
		,	'Correlated.Items': function ()
			{
				return new ItemRelationsCorrelatedView({ itemsIds: this.model.get('internalid'), application: this.application });
			}
		,	'Related.Items': function ()
			{
				return new ItemRelationsRelatedView({ itemsIds: this.model.get('internalid'), application: this.application });
			}
		,	'SocialSharing.Flyout': function ()
			{
				return new SocialSharingFlyoutView();
			}
		,	'RecentlyViewed.Items' : function ()
			{
				return new RecentlyViewedItemsView({ application: this.application });
			}
		,	'AddonOptions.Items': function ()
			{
				var custitem_itemoptions = this.model.get('custitem_itemoptions')
				,	addon_options_array = []
				,	addon_options_int_array = [];
				
				if (custitem_itemoptions)
				{
					custitem_itemoptions = JSON.parse(custitem_itemoptions);
					custitem_itemoptions = custitem_itemoptions.options;
					addon_options_int_array = _.map(custitem_itemoptions, function (option) {
						option.item = parseInt(option.item);
						return option;
					});
					addon_options_array = _.pluck(addon_options_int_array, 'item');
				}
				
				/*if (custitem_itemoptions)
				{
					custitem_itemoptions = JSON.parse(custitem_itemoptions);
					custitem_itemoptions = custitem_itemoptions.options;
					_.each(custitem_itemoptions, function (group) {
					addon_options_int_array = _.map(group.items, function (option) {
						option.item = parseInt(option.item);
						return option;
					});
					});
					addon_options_array = _.pluck(addon_options_int_array, 'item');
				}*/
				
				return new AddonOptionsItemsView({
						AddonOptionsItems: addon_options_array
					,	AddonOptionsPrices: addon_options_int_array
					,	itemsIds: this.model.get('internalid')
					,	productType: 'Test 1234'
					,	application: this.application
				});
			}
		}

		// @method storeColapsiblesState
		// Since this view is re-rendered every time you make a selection we need to keep the state of the collapsed parts for the next render
		// This method save the status (collapsed/expanded) of all elements with the class .collapse
	,	storeColapsiblesState: function ()
		{
			//TODO This should be a plugin or be located in some external part!
			this.storeColapsiblesStateCalled = true;

			this.$('.collapse').each(function (index, element)
			{
				colapsibles_states[SC.Utils.getFullPathForElement(element)] = jQuery(element).hasClass('in');
			});
		}

		// @method resetColapsiblesState
		// As we keep track of the state (See storeColapsiblesState method), we need to reset the value of the collapsable items onces we render the first time
	,	resetColapsiblesState: function ()
		{
			//TODO This should be a plugin or be located in some external part!
			var self = this;
			_.each(colapsibles_states, function (is_in, element_selector)
			{
				self.$(element_selector)[is_in ? 'addClass' : 'removeClass']('in').css('height', is_in ? 'auto' : '0');
			});
		}

		//@method updateQuantity Updates the quantity of the model
		//@param {jQuery.Event} e
		//@return {Void}
	,	updateQuantity: function (e)
		{
			var new_quantity = parseInt(jQuery('[name="quantity"]').val(), 10)
			,	current_quantity = this.model.get('quantity')
			,	isOptimistic = this.application.getConfig('addToCartBehavior') === 'showCartConfirmationModal'
			,	min_quantity =  this.model.get('_minimumQuantity', true);

			if (new_quantity < this.model.get('_minimumQuantity', true))
			{
				if (require('LiveOrder.Model').loadCart().state() === 'resolved') // TODO: resolve error with dependency circular.
				{
					var itemCart = SC.Utils.findItemInCart(this.model, require('LiveOrder.Model').getInstance()) // TODO: resolve error with dependency circular.
					,	total = itemCart && itemCart.get('quantity') || 0;

					if ((total + new_quantity) < this.model.get('_minimumQuantity', true))
					{
						new_quantity = min_quantity;
					}
				}
			}

			jQuery('[name="quantity"]').val(new_quantity);

			if (new_quantity !== current_quantity)
			{
				this.model.setOption('quantity', new_quantity);

				if (!this.$containerModal || !isOptimistic)
				{
					this.refreshInterface(e);
				}
			}

			if (this.$containerModal)
			{
				this.refreshInterface(e);

				// need to trigger an afterAppendView event here because, like in the PDP, we are really appending a new view for the new selected matrix child
				this.application.getLayout().trigger('afterAppendView', this);
			}
		}

		// @method addQuantity Increase the product's Quantity by 1
		// @param {jQuery.Event} e
		//@return {Void}
	,	addQuantity: function (e)
		{
			e.preventDefault();
			var input_quantity = this.$('[name="quantity"]')
			,	old_value = parseInt(input_quantity.val(), 10);

			input_quantity.val(old_value + 1);
			//input_quantity.trigger('change'); This was default behavior which caused lag
			this.updateQuantity();
		}

		// @method subQuantity Decreases the product's Quantity by 1
		// @param {jQuery.Event} e
	,	subQuantity: function (e)
		{
			e.preventDefault();

			var input_quantity = this.$('[name="quantity"]')
			,	old_value = parseInt(input_quantity.val(), 10);

			input_quantity.val(old_value-1);
			//input_quantity.trigger('change'); This was default behavior which caused lag
			this.updateQuantity();
		}

		// @method submitOnEnter Submit the form when user presses enter in the quantity input text
		// @param {jQuery.Event} e
		//@return {Void}
	,	submitOnEnter: function (e)
		{
			if (e.keyCode === 13)
			{
				e.preventDefault();
				e.stopPropagation();
				this.addToCart(e);
			}
		}
		
	,	addOptionToCart: function (e)
		{
			e.preventDefault();
		}
		
	,	getSelectedOptionItems: function()
		{
			var self = this
			,	items = []
			,	items_for_cart = []
			,	models = this.childViewInstances['AddonOptions.Items'] ? this.childViewInstances['AddonOptions.Items'].collection.models : null;
			
			//Only proceed if this item contains addon items
			if (models ? models.length > 0 : false)
			{
				//Filter items for bulk operation
				_.each(models, function(pli)
				{
					var quantity = self.$('[name="quantity"]').val();
					//irrelevant items: no-op
					if (pli.get('checked') !== true)
					{
						return;
					}
	
					/*items.push(pli);*/
					pli.setOption('quantity', quantity);
					//Does not need to be used. Passing whole model to the cart
					//var item = pli
					//,	item_internal_id = item.get('_id')
					//,	item_for_cart = self.getItemForCart(item_internal_id, pli.get('quantity'), item.itemoptions_detail);
	
					items.push(pli);
				});
	
				//items: backbone models representing selected items
				//button_selector: all the buttons that should be disabled when performing a batch operation
				return {
					items: items
				//,	items_for_cart: items_for_cart
				};
			} else {
				return {
					items: 0
				}
			}
		}

		// @method addToCart Updates the Cart to include the current model
		// also takes care of updating the cart if the current model is already in the cart
		// @param {jQuery.Event} e
		//@return {Void}
	,	addToCart: function (e)
		{
			e.preventDefault();

			// Updates the quantity of the model
			var quantity = this.$('[name="quantity"]').val();
			this.model.setOption('quantity', quantity);

			if (this.model.isValid(true) && this.model.isReadyForCart())
			{
				var self = this
				,	cart = LiveOrderModel.getInstance()
				,	layout = this.application.getLayout()
				,	add_to_cart_promise
				,	error_message = _('Sorry, there is a problem with this Item and can not be purchased at this time. Please check back later.').translate()
				,	isOptimistic = this.application.getConfig('addToCartBehavior') === 'showCartConfirmationModal'
				,	selected_models = this.getSelectedOptionItems()
				,	sku = this.model.get('_sku')
				,	order_limit = this.model.get('limits')
				,	is_in_cart_model = Utils.findItemInCart(this.model, cart)
				,	qty_in_cart = is_in_cart_model ? is_in_cart_model.get('quantity') : null
				,	qty_available = this.model.get('quantityavailable')
				,	is_backorderable = (this.model.get('outofstockbehavior') === 'Allow back orders but display out-of-stock message' || this.model.get('outofstockbehavior') === 'Allow back orders with no out-of-stock message') ? true : false;

				/*if (this.model.itemOptions && this.model.itemOptions.GIFTCERTRECIPIENTEMAIL)
				{
					if (!Backbone.Validation.patterns.email.test(this.model.itemOptions.GIFTCERTRECIPIENTEMAIL.label))
					{
						self.showError(_('Recipient email is invalid').translate(),null,true);
						return;
					}
				}*/
				
				//If this is a gift certificate validate the form fields
				if(this.model.get('itemtype') === 'GiftCert')
				{
					var certificate_amount = $('#option-custcol_gift_certificate_amount_2').val()
					,	email = $('#option-GIFTCERTRECIPIENTEMAIL').val()
					,	error_message = ''
					,	error_title_1 = ''
					,	error_title_2 = '';

					if (!Backbone.Validation.patterns.email.test(email))
					{
						if (isOptimistic)
						{
							error_message += "The email you entered cannot be processed.<br>Please enter a valid email address.<br>";
							error_title_1 = "Invalid Email Address";
						}
					}
					
					if(!(!isNaN(parseFloat(certificate_amount)) && isFinite(certificate_amount)))
					{
						if (isOptimistic)
						{
							error_message += certificate_amount + " is not a valid a number.<br>Please enter a number for your Gift Certificate amount.";
							error_title_2 = "Invalid Gift Certificate Amount";
						}
					} else if (parseFloat(certificate_amount) > 1500)
					{
						if (isOptimistic)
						{
							error_message += "Gift certificates may only be purchased up to a value of $1,500. Please reduce the gift certificate amount to $1,500.";
							error_title_2 = "Maximum Gift Certificate Amount Exceeded";
						}
					} else if (parseFloat(certificate_amount) < 10)
					{
						if (isOptimistic)
						{
							error_message += "The minimum value for a gift certificate is $10. Please change the value of your gift certificate to at least $10.";
							error_title_2 = "Under Minimum Gift Certificate Amount";
						}
					}
					
					if(error_message)
					{
						var error_title = (error_title_1 && error_title_2) ? "Invalid Input" : (error_title_1 || error_title_2)
						self.showErrorInModal(_(error_message).translate(),_(error_title).translate());
						return;
					}
				}

				if (isOptimistic)
				{
					//Set the cart to use optimistic when using add to cart
					// TODO pass a param to the add to cart instead of using this hack!
					cart.optimistic = {
						item: this.model
					,	quantity: quantity
					};
				}
				
				//Check the quantity available
				if((qty_in_cart + Number(quantity)) > qty_available && !is_backorderable)
				{
					if (isOptimistic)
					{
						self.showErrorInModal(_("We're sorry, " + sku + " does not have enough available inventory for your purchase.<br>Please reduce your order Qty.").translate(),_("Available Quantity Exceeded").translate());
					}
				}
				//Check the order limit
				else if (order_limit || order_limit === 0 ? (qty_in_cart + Number(quantity) > order_limit) : false)
				{
					if (isOptimistic)
					{
						self.showErrorInModal(_("We're sorry, " + sku + " has a maximum order limit of " + order_limit + (qty_in_cart ? ". <br>You currently have " + qty_in_cart + " of these items in your cart." : "") + "<br>Please reduce the Qty within this limit.").translate(),_("Order Limit Exceeded").translate());
					}
				}
				else {
					
					//Add multiple items to the cart if this is an option item
					if (selected_models.items.length >= 1)
					{
						//no items selected: no opt
						if (selected_models.items.length < 1)
						{
							return;
						}
						//Set parent model as Parent
						self.model.itemOptions.custcol_pco = {};
						self.model.itemOptions.custcol_pco.internalid = 'Parent';
						self.model.itemOptions.custcol_pco.label = 'Parent';
						
						selected_models.items.push(self.model);
			
						//add items to cart
						var cart_promise = cart.addItems(selected_models.items).done(function ()
						{
							//If there is an item added into the cart
							if (cart.getLatestAddition())
							{
								layout.showCartConfirmation();
							}
							else
							{
								self.showError(error_message);
							}
						});
	
						/*if (isOptimistic)
						{
							cart.optimistic.promise = cart_promise;
							layout.showCartConfirmation();
						}*/
					}
					// If the item is already in the cart
					else if (this.model.cartItemId)
					{
						//Set as orphan because it is not a child or parent
						self.model.itemOptions.custcol_pco = {};
						self.model.itemOptions.custcol_pco.internalid = 'Orphan';
						self.model.itemOptions.custcol_pco.label = 'Orphan';
						//Set gift cert amount for NS script to read value
						if(self.model.get('itemtype') === 'GiftCert'){
							var value = $('#option-custcol_gift_certificate_amount_2').val();
							self.model.itemOptions.custcol_gc_amount = {};
							self.model.itemOptions.custcol_gc_amount.internalid = value.toString();
							self.model.itemOptions.custcol_gc_amount.label = value.toString();
						}
						cart_promise = cart.updateItem(this.model.cartItemId, this.model).done(function ()
						{
							//If there is an item added into the cart
							if (cart.getLatestAddition())
							{
								if (self.$containerModal)
								{
									self.$containerModal.modal('hide');
								}
	
								if (layout.currentView instanceof require('Cart.Detailed.View'))
								{
									layout.currentView.showContent();
								}
							}
							else
							{
								self.showError(error_message);
							}
						});
					}
					else
					{
						//Set as orphan because it is not a child or parent
						self.model.itemOptions.custcol_pco = {};
						self.model.itemOptions.custcol_pco.internalid = 'Orphan';
						self.model.itemOptions.custcol_pco.label = 'Orphan';
						//Set gift cert amount for NS script to read value
						if(self.model.get('itemtype') === 'GiftCert'){
							var value = $('#option-custcol_gift_certificate_amount_2').val();
							self.model.itemOptions.custcol_gc_amount = {};
							self.model.itemOptions.custcol_gc_amount.internalid = value.toString();
							self.model.itemOptions.custcol_gc_amount.label = value.toString();
						}
						cart_promise = cart.addItem(self.model).done(function ()
						{
							//If there is an item added into the cart
							if (cart.getLatestAddition())
							{
								if (isOptimistic)
								{
									layout.showCartConfirmation();
								}
							}
							else
							{
								self.showError(error_message);
							}
						});
	
						/*if (isOptimistic)
						{
							cart.optimistic.promise = cart_promise;
							layout.showCartConfirmation();
						}*/
					}
				
					// Handle errors. Checks for rollback items.
					
						cart_promise.fail(function (jqXhr)
						{
							var error_details = null;
							try
							{
								var response = JSON.parse(jqXhr.responseText);
								error_details = response.errorDetails;
							}
							finally
							{
								if (error_details && error_details.status === 'LINE_ROLLBACK')
								{
									var new_line_id = error_details.newLineId;
									self.model.cartItemId = new_line_id;
								}
		
								self.showError(_('We couldn\'t process your item').translate());
		
								if (isOptimistic)
								{
									self.showErrorInModal(_('We couldn\'t process your item').translate());
								}
							}
						});
	
					// Disables the button while it's being saved then enables it back again
					if (e && e.target)
					{
						var $target = jQuery(e.target).attr('disabled', true);
	
						cart_promise.always(function ()
						{
							$target.attr('disabled', false);
						});
					}
				}
			}
		}

		// @method refreshInterface
		// Computes and store the current state of the item and refresh the whole view, re-rendering the view at the end
		// This also updates the URL, but it does not generates a history point
		//@return {Void}
	,	refreshInterface: function ()
		{
			var self = this;

			var focused_element = this.$(':focus').get(0);

			this.focusedElement = focused_element ? SC.Utils.getFullPathForElement(focused_element) : null;

			
				Backbone.history.navigate(this.options.baseUrl + this.model.getQueryString(), {replace: true});
			

			//TODO This should be a render, as the render aim to re-paint a view and the showContent aims to navigations
			self.showContent({dontScroll: true});
		}

		// @method showInModal overrides the default implementation to take care of showing the PDP in a modal by changing the template
		// This doesn't trigger the after events because those are triggered by showContent
		// @param {Object} options Any options valid to show content
		// @return {jQuery.Deferred}
	,	showInModal: function (options)
		{
			this.template = quick_view_tpl;

			return this.application.getLayout().showInModal(this, options);
		}

		// @method prepareViewToBeShown
		// Prepares the model and other internal properties before view.showContent
		// Prepares the view to be shown. Set its title, header and the items options are calculated
		//@return {Void}
	,	prepareViewToBeShown: function ()
		{
			// TODO Change this. This method is called by the itemDetails.router before call showContent of this view.
			// As each time an option change the view is re-painted by using showContent, we cannot add this code in the showContent code
			// the correct way to do this is call render for each re-paint.
			this.title = this.model.get('_pageTitle');
			this.page_header = this.model.get('_pageHeader');

			this.computeDetailsArea();
		}

		// @method computeDetailsArea
		// Process what you have configured in itemDetails as item details.
		// In the PDP extra information can be shown based on the itemDetails property in the Shopping.Configuration.
		// These are extra field extracted from the item model
		//@return {Void}
	,	computeDetailsArea: function ()
		{
			var self = this
			,	details = [];

			_.each(this.application.getConfig('itemDetails', []), function (item_details)
			{
				var content = '';

				if (item_details.contentFromKey)
				{
					content = self.model.get(item_details.contentFromKey);
				}

				if (jQuery.trim(content))
				{
					details.push({
						name: item_details.name
					,	isOpened: item_details.opened
					,	content: content
					,	itemprop: item_details.itemprop
					});
				}
			});

			this.details = details;
		}

		// @method getMetaKeywords
		// @return {String}
	,	getMetaKeywords: function ()
		{
			// searchkeywords is for alternative search keywords that customers might use to find this item using your Web store's internal search
			// they are not for the meta keywords
			// return this.model.get('_keywords');
			return this.getMetaTags().filter('[name="keywords"]').attr('content') || '';
		}

		// @method getMetaTags
		// @return {Array<HTMLElement>}
	,	getMetaTags: function ()
		{
			return jQuery('<head/>').html(
				jQuery.trim(
					this.model.get('_metaTags')
				)
			).children('meta');
		}

		// @method getMetaDescription
		// @return {String}
	,	getMetaDescription: function ()
		{
			return this.getMetaTags().filter('[name="description"]').attr('content') || '';
		}

		// @method tabNavigationFix
		// When you blur on an input field the whole page gets rendered,
		// so the function of hitting tab to navigate to the next input stops working
		// This solves that problem by storing a a ref to the current input
		// @param {jQuery.Event} e
		//@return {Void}
	,	tabNavigationFix: function (e)
		{
			var self = this;
			this.hideError();

			// We need this timeout because sometimes a view is rendered several times and we don't want to loose the tabNavigation
			if (!this.tabNavigationTimeout)
			{
				// If the user is hitting tab we set tabNavigation to true, for any other event we turn it off
				this.tabNavigation = e.type === 'keydown' && e.which === 9;
				this.tabNavigationUpsidedown = e.shiftKey;
				this.tabNavigationCurrent = SC.Utils.getFullPathForElement(e.target);
				if (this.tabNavigation)
				{
					this.tabNavigationTimeout = setTimeout(function ()
					{
						self.tabNavigationTimeout = null;
						this.tabNavigation = false;
					}, 5);
				}
			}
		}

		//@method showContent
		//@param {Object<dontScroll:Boolean>} options
		//@return {Void}
	,	showContent: function (options)
		{
			var self = this;

			// Once the showContent has been called, this make sure that the state is preserved
			// REVIEW: the following code might change, showContent should receive an options parameter
			this.application.getLayout().showContent(this, options && options.dontScroll).done(function ()
			{
				Tracker.getInstance().trackProductView(self.model);

				self.afterAppend();
				self.initPlugins();

				self.$('[data-type="add-to-cart"]').attr('disabled', true);
				LiveOrderModel.loadCart().done(function()
				{
					if (self.model.isReadyForCart())
					{
						self.$('[data-type="add-to-cart"]').attr('disabled', false);
					}
				});
			});
		}

		//@method afterAppend
		//@return {Void}
	,	afterAppend: function ()
		{
			var overflow = false;
			this.focusedElement && this.$(this.focusedElement).focus();

			if (this.tabNavigation)
			{
				var current_index = this.$(':input').index(this.$(this.tabNavigationCurrent).get(0))
				,	next_index = this.tabNavigationUpsidedown ? current_index - 1 : current_index + 1;

				this.$(':input:eq('+ next_index +')').focus();
			}

			this.storeColapsiblesStateCalled ? this.resetColapsiblesState() : this.storeColapsiblesState();

			RecentlyViewedItemsCollection.getInstance().addHistoryItem(this.model);

			if (this.inModal)
			{
				var $link_to_fix = this.$el.find('[data-name="view-full-details"]');
				$link_to_fix.mousedown();
				$link_to_fix.attr('href', $link_to_fix.attr('href') + this.model.getQueryString());
			}

			this.$('#item-details-content-container-0').overflowing('#item-details-info-tab-0', function ()
			{
				overflow = true;
			});

			if(!overflow)
			{
				this.$('.item-details-more').hide();
			}
		}

		// @method initPlugins
		//@return {Void}
	,	initPlugins: function ()
		{
			this.$el.find('[data-action="pushable"]').scPush();
			this.$el.find('[data-action="tab-content"]').scSeeMoreLess();
		}

		// @method setOption When a selection is change, this computes the state of the item to then refresh the interface.
		// @param {jQuery.Event} e
		// @return {Void}
	,	setOption: function (e)
		{
			var self = this
			,	$target = jQuery(e.currentTarget)
			,	value = $target.val() || $target.data('value') || null
			,	cart_option_id = $target.closest('[data-type="option"]').data('cart-option-id');

			// prevent from going away
			e.preventDefault();

			// if option is selected, remove the value
			if ($target.data('active'))
			{
				value = null;
			}

			// it will fail if the option is invalid
			try
			{
				this.model.setOption(cart_option_id, value);
			}
			catch (error)
			{
				// Clears all matrix options
				_.each(this.model.getPosibleOptions(), function (option)
				{
					option.isMatrixDimension && self.model.setOption(option.cartOptionId, null);
				});

				// Sets the value once again
				this.model.setOption(cart_option_id, value);
			}

 			this.refreshInterface(e);

			// Need to trigger an afterAppendView event here because, like in the PDP, we are really appending a new view for the new selected matrix child
			if (this.$containerModal)
			{
				this.application.getLayout().trigger('afterAppendView', this);
			}
		}

		//@method showMore Toggle the content of an options, and change the label Show Less and Show More by adding a class
		//@return {Void}
	,	showMore: function ()
		{
			this.$el.find('.item-details-tab-content').toggleClass('show');
		}

	,	captureEmailQuickView: function(e)
		{
   			e.preventDefault();
			$(".email-form-quick-view").show();
    		var itemid = this.model.get('internalid');
		}

    ,	captureEmail: function(e)
    	{
    		e.preventDefault();
    		var itemid = this.model.get('internalid');
    		$('#emailModal').modal();
    	}

    ,	sendEmail: function(e)
    	{
    		var itemid = this.model.get('internalid');
    		var email = $('.email-input').val();
			var self = this;
    		var emailBackInStock = new EmailBackInStock();
			emailBackInStock.save({email:email, itemid:itemid}, 
				{
    			error: function(resp) {
    				$('.email-message').css("display","block");
    				$('.email-message').css("color","red");
    				$('.email-message').text("Please enter a valid email");
   				}
			}).done(function(resp){
				//Callback function to display thank you message
				self.sendEmailComplete(resp);

				//Callback function to submit the Bronto form only if checkbox is checked
				if ($('.bis-mailing-list-check:checked').val() == 'on')
				{
					self.sendBrontoComplete(email);
				}
			});
    	}
		
		//Display the thank you message
	,	sendEmailComplete: function (resp){
			$('.email-form').css("display","none");
			$('.email-message').css("display","block");
			$('.email-message').css("color","black");
			$('.email-message').text("Thank you!");
        }

		//Submit the Bronto form
	,	sendBrontoComplete: function(email){
			//Generates the direct add url
			var url = 'http://info.primaryarms.com/public/?q=direct_add&fn=Public_DirectAddForm&id=bqsvzgbdbvzyrsnzxousbwwqanlbbfd&email='+email+'&list1=0bc603ec0000000000000000000000052b6b&list2=0bd203ec00000000000000000000000c25f1';
			
			//Modify the src url and trigger click event on the direct add image
			$('.email-sign-up-direct-add').attr('src', url);
			$('.email-sign-up-direct-add').trigger('click');
				
			//This technique below was producing cross domain errors
			
			//Set the form input value to the email address provided in the back in stock input field
			/*$('.fb-email').val(email);
			//Defines the ajax submission 
			$('#bis-mailing-list-form').submit(function(e){
				$.ajax({
					type: "POST",
					url: 'http://info.primaryarms.com/public/webform/process/',
					data: $("#bis-mailing-list-form").serialize(),
					success: function(e)
					{
						console.log('success');
					},
					error:function(e) {
                		console.log("Error");
						console.log(e);
            		}
				 });
				//Prevent the page reload or redirect
				e.preventDefault();
			});
			//Required to initialize the ajax submission
			$('#bis-mailing-list-form').submit();*/
		}
		
    ,	clearMessage: function()
    	{
    		$('#email-message').css("display","none");
    	}

		//@method getContext
		//@return {ItemDetails.View.Context}
	,	getContext: function ()
		{
			var model = this.model
			,	thumbnail = model.get('_images', true)[0] || model.get('_thumbnail')
			,	selected_options = model.getSelectedOptions()
			,	quantity = model.get('quantity')
			,	min_quantity = model.get('_minimumQuantity', true)
			,	min_disabled = false
			,	restriction_type = model.get('custitemship_restrictions')
			,	is_ffl = false
			,	is_nfa = false
			,	cim = model.get('custitem_itemoptions')
			,	att = model.get('custitem_test_for_website')
			,	price_level = SC.SESSION && SC.SESSION.priceLevel ? SC.SESSION.priceLevel : SC.DEFAULT_SESSION.priceLevel
			,	limits = null;
			
			if (typeof cim !== 'undefined' && cim !== '' && cim !== 'undefined') {
				var customitemoptions = JSON.parse(cim);
			}
			if (typeof att !== 'undefined' && att !== '' && att !== 'undefined') {
				var customattributes = JSON.parse(att);
			}
			
			if (model.get('quantity') <= model.get('_minimumQuantity', true))
			{
				// TODO: resolve error with dependency circular.
				if (require('LiveOrder.Model').loadCart().state() === 'resolved')
				{
					// TODO: resolve error with dependency circular.
					var itemCart = SC.Utils.findItemInCart(model, require('LiveOrder.Model').getInstance())
					,	total = itemCart && itemCart.get('quantity') || 0;

					if ((total + model.get('quantity')) <= model.get('_minimumQuantity', true))
					{
						min_disabled = true;
					}
				}
				else
				{
					min_disabled = false;
				}
			}
			
			//Set's is_ffl or is_nfa based on the restriction type
			switch(restriction_type) {
				default:
					break;
				case 'Unrestricted':
					break;
				case 'FFL':
					is_ffl = true;
					break;
				case 'FFL-Handgun':
					is_ffl = true;
					break;
				case 'FFL - CA':
					is_ffl = true;
					break;
				case 'NFA - Firearm':
					is_nfa = true;
					break;
				case 'NFA - Suppressor':
					is_nfa = true;
					break;
			}
			
			//Set the dealer limits based on price level. This include military and law enforcement
			switch(price_level) {
				case '5':
					limits = model.get('custitem_active_customer_limit');
					break;
				case '2':
					limits = model.get('custitem_active_dealer_1_limit');
					break;
				case '6':
					limits = model.get('custitem_active_dealer_2_limit');
					break;
				case '7':
					limits = model.get('custitem_active_dealer_3_limit');
					break;
				case '10':
					limits = model.get('custitem_active_mil_limit');
					break;
				default:
					limits = model.get('custitem_active_customer_limit');
			}
			
			//Set limits to null incase they are set to 99999 through Rodney's Ad Manager
			//Limits are set to 99999 in the Ad Manager when they should not have a limit
			if(limits === 99999){
				limits = null;
			}
			
			//Set the current limit on the model for the add to cart function so we don't have to switch through them again
			model.set('limits', limits);
			
			//@class ItemDetails.View.Context
			return {
				//@property {ItemDetails.Model} model
				model: model
				//@property {Boolean} isPriceEnabled
			,	isPriceEnabled: !ProfileModel.getInstance().hidePrices()
				//@property {Array<ItemDetailsField>} details
			,	details: this.details
				//@property {Boolean} showDetails
			,	showDetails: this.details.length > 0
				//@property {Boolean} isItemProperlyConfigured
			,	isItemProperlyConfigured: model.isProperlyConfigured()
				//@property {Boolean} showQuantity
			,	showQuantity: model.get('_itemType') === 'GiftCert'
				//@property {Boolean} showRequiredReference
			,	showRequiredReference: model.get('_itemType') === 'GiftCert'
				//@property {Boolean} showSelectOptionifOutOfStock
			,	showSelectOptionMessage : !model.isSelectionComplete() && model.get('_itemType') !== 'GiftCert'
				//@property {Boolean} showMinimumQuantity
			,	showMinimumQuantity: !! min_quantity && min_quantity > 1
				//@property {Boolean} isReadyForCart
			,	isReadyForCart: model.isReadyForCart()
				//@property {Boolean} showReviews
			,	showReviews: this.reviews_enabled
				//@property {String} itemPropSku
			,	itemPropSku: '<span itemprop="sku">' + model.get('_sku', true) + '</span>'
				//@property {String} item_url
			,	item_url : model.get('_url') + model.getQueryString()
				//@property {String} thumbnailUrl
			,	thumbnailUrl : this.options.application.resizeImage(thumbnail.url, 'main')
				//@property {String} thumbnailAlt
			,	thumbnailAlt : thumbnail.altimagetext
				//@property {String} sku
			,	sku : model.get('_sku', true)
				//@property {Boolean} isMinQuantityOne
			,	isMinQuantityOne : model.get('_minimumQuantity', true) === 1
				//@property {Number} minQuantity
			,	minQuantity : min_quantity
				//@property {Number} quantity
			,	quantity : quantity
				//@property {Boolean} isMinusButtonDisabled
			,	isMinusButtonDisabled: min_disabled || model.get('quantity') === 1
				//@property {Boolean} hasCartItem
			,	hasCartItem : !!model.cartItemId
				//@property {Array} selectedOptions
			,	selectedOptions: selected_options
				//@property {Boolean} hasSelectedOptions
			,	hasSelectedOptions: !!selected_options.length
				//@property {Boolean} hasAvailableOptions
			,	hasAvailableOptions: !!model.getPosibleOptions().length
				//@property {Boolean} isReadyForWishList
			,	isReadyForWishList: model.isReadyForWishList()
			
			,	isPurchasable: model.get('isinstock') || model.get('isbackorderable')
			
			,	manufacturer: model.get('manufacturer')
			
			,	wsatt: customattributes
			
			,	restrictionMessage: model.get('custitem_warning_message')
			
			,	ffl: is_ffl
			
			,	nfa: is_nfa
			
			,	youTube: model.get('custitem_youtube')
			
			,	manualUrl1: model.get('custitem_user_manual_1')
			
			,	manualUrl2: model.get('custitem_user_manual_2')
			
			,	manualUrl3: model.get('custitem_user_manual_3')
			
			,	customOptions: customitemoptions
			
			,	url: SC.ENVIRONMENT.currentHostString
			
			//,	isDealer: !!(SC.SESSION ? (SC.SESSION.priceLevel !== '5') : false)
			
			//,	dealerLimits: dealer_limit
			
			,	limits: limits
			
			,	hasAddonOptions: !!(model.get('custitem_itemoptions'))
			
			,	discountOptionsGroups: this.discountOptionsGroups
			}
		}
	});
});

//@class ItemDetails.View.Initialize.Parameters
//@property {ItemDetails.Model} model
//@property {String} baseUrl
//@property {ApplicationSkeleton} application
