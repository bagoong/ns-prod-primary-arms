{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="header-sidebar-wrapper">
	<div data-view="Header.Profile"></div>
	
	<div class="header-sidebar-menu-wrapper" data-type="header-sidebar-menu">

		<ul class="header-sidebar-menu">
            <li>
                <a class="header-menu-home-anchor" id="home-icon" href="/" data-touchpoint="home" data-hashtag="#/"><i class="header-menu-home-icon" id="header-menu-home-icon-mobile"></i></a>
            </li>
			{{#each categories}}
            {{#unless this.hideMobile}}
			<li class="{{#if @last}}header-sidebar-menu-lastoption{{/if}}">
				<a {{objectToAtrributes this}} {{#if categories}}data-action="push-menu"{{/if}} name="{{text}}">
				{{text}}
				{{#if categories}}<i class="header-sidebar-menu-push-icon"></i>{{/if}}
				</a>
				{{#if categories}}
				<ul>

					<li>
						<a href="#" class="header-sidebar-menu-back" data-action="pop-menu" name="back-sidebar">
							<i class="header-sidebar-menu-pop-icon"></i>
							{{translate 'Back'}}
						</a>
					</li>
					{{#each categories}}
                    	{{#if class}}
                            <li>
                                <a class="{{class}}" {{objectToAtrributes this}} {{#if categories}}data-action="push-menu"{{/if}}>
                                {{text}}
                                </a>
                            </li>
                        {{/if}}
                        {{#if categories}}
                        	{{#each categories}}
                                {{#if class}}
                                <li>
                                    <a {{objectToAtrributes this}} name="{{text}}" {{#if categories}}data-action="push-menu"{{/if}}>{{text}}{{#if categories}}<i class="header-sidebar-menu-push-icon"></i>{{/if}}</a>
                                    {{#if categories}}
                                    <ul>
                                        <li>
                                            <a href="#" class="header-sidebar-menu-back" data-action="pop-menu">
                                                <i class="header-sidebar-menu-pop-icon"></i>
                                                {{translate 'Back'}}
                                            </a>
                                        </li>
                                        <li>
                                            <a {{objectToAtrributes this}} name="{{text}}">
                                                {{translate 'All '}}{{text}}{{translate ' products'}}
                                            </a>
                                        </li>
                                        {{#each categories}}
                                        <li>
                                            <a {{objectToAtrributes this}} name="{{text}}">{{text}}</a>
                                        </li>
                                        {{/each}}
                                    </ul>
                                    {{/if}}
                                </li>
                                {{/if}}
							{{/each}}
                        {{/if}}
					{{/each}}
				</ul>
				{{/if}}
			</li>
            {{/unless}}
			{{/each}}
            {{#if saleData}}
                <li>
                    <a class="header-menu-home-anchor-{{id}}" id="sale-menu-item" {{#unless saleCategories}}href="/{{saleData.url}}+{{saleData.values.0.url}}" data-touchpoint="home" data-hashtag="#/{{saleData.url}}+{{saleData.values.0.url}}"{{/unless}}{{#if saleCategories}}data-action="push-menu"{{/if}}>Sale
                    {{#if saleCategories}}<i class="header-sidebar-menu-push-icon"></i>{{/if}}
                    </a>
                    {{#if saleCategories}}
                        <ul class="header-sidebar-menu">
                        	<li>
                                <a href="#" class="header-sidebar-menu-back" data-action="pop-menu" name="back-sidebar">
                                    <i class="header-sidebar-menu-pop-icon"></i>
                                    {{translate 'Back'}}
                                </a>
                            </li>
                            {{#each saleData.values}}
                            <li>
                                <a href="/{{../saleData.url}}+{{url}}" data-touchpoint="home" data-hashtag="#/{{../saleData.url}}+{{url}}" style="display: block">{{label}}</a>
                            </li>
                            {{/each}}
                            {{#each saleCategories.values}}
                                <li>
                                    <a href="/{{../saleCategories.url}}+{{url}}" data-touchpoint="home" data-hashtag="#/{{../saleCategories.url}}+{{url}}" style="display: block">{{label}}</a>
                                </li>
                            {{/each}}
                        </ul>
                    {{/if}}
                </li>
            {{/if}}
            <!--<li>
                <div class="header-menu-home-anchor" data-cms-area="header_menu_item" data-cms-area-filters="global">
                </div>
            </li>-->
			{{#if showExtendedMenu}}
			<li class="header-sidebar-menu-myaccount" data-view="Header.Menu.MyAccount"></li>
			{{/if}}
			<li data-view="RequestQuoteWizardHeaderLink">
			</li>
		</ul>

	</div>

	{{#if showExtendedMenu}}
	<a class="header-sidebar-user-logout" href="#" data-touchpoint="logout" name="logout">
		<i class="header-sidebar-user-logout-icon"></i>
		{{translate 'Sign Out'}}
	</a>
	{{/if}}

	{{#if showLanguages}}
	<div data-view="Global.HostSelector"></div>
	{{/if}}
	{{#if showCurrencies}}
	<div data-view="Global.CurrencySelector"></div>
	{{/if}}

</div>