/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Header
define(
	'Header.Menu.View'
, [
		'Profile.Model'
	,	'SC.Configuration'
	,	'Header.Profile.View'
	,	'Header.Menu.MyAccount.View'
	,	'GlobalViews.HostSelector.View'
	,	'GlobalViews.CurrencySelector.View'
	,	'SiteSearch.View'
	
	,	'header_menu.tpl'

	,	'Backbone'
	,	'Backbone.CompositeView'
	,	'underscore'
	,	'jQuery.sidebarMenu'
	]
,	function(
		ProfileModel
	,	Configuration
	,	HeaderProfileView
	,	HeaderMenuMyAccountView
	,	GlobalViewsHostSelectorView
	,	GlobalViewsCurrencySelectorView
	,	SiteSearchView

	,	header_menu

	,	Backbone
	,	BackboneCompositeView
	,	_
		
	)
{
	'use strict';

	//@class Header.Menu.View @extends Backbone.View
	return Backbone.View.extend({

		template: header_menu

	,	initialize: function ()
		{
			var self = this;
			BackboneCompositeView.add(this);

			ProfileModel.getPromise().done(function ()
			{
				self.render();
			});
			
			this.once('afterViewRender', function() {
				/*$(document).ready(function() {
					self.$el.find('[data-type="SiteSearch"]').show();
				});*/
				
				//Check if there are any sales
				if(!SC.isPageGenerator() && window.location.pathname !== '/sca-dev-montblanc/checkout.ssp' && window.location.pathname !== '/sca-dev-montblanc/checkout-local.ssp'){
					jQuery.ajax({
						url: _.getAbsoluteUrl( '../../api/items?c=3901023&country=US&currency=USD&include=facets&language=en&limit=0&n=2&offset=0&pricelevel=5&facet.exclude=custitem_marketing_categories,custitem_1,custitem_2,custitem_accessory_attachment,custitem_action,custitem_adjustable,custitem_ambidextrous,custitem_armor_type,custitem_assembly,custitem_barrel_length,custitem_brand,custitem_bullet_weight,custitem_caliber_gauge,custitem_caliber_marking,custitem_carrier_style,custitem_color,custitem_decibel_reduction,pricelevel2,pricelevel6,pricelevel7,custitem_fitment,custitem_fixed_folding,custitem_focal_plane,custitem_forward_cant,custitem_frame_size,custitem_gas_system_length,custitem_illuminated,custitem_in_stock,custitem_latch_size,custitem_leg_style,pricelevel10,custitem_magnification,custitem_material,custitem_night_vision_compatible,pricelevel5,custitem_optic_type,custitem_pa_featured_products,custitem_pa_staff_picks,custitem_pa_top_selling,custitem_padded,custitem_platform,custitem_position,pricelevel,custitem_quick_release,custitem_ring_height,custitem_size,custitem_storage,custitem_style,custitem_thread_pattern,custitem_throw,custitem_tube_diameter,custitem_type,itemtype,custitem_user_serviceable,onlinecustomerprice')
					}).done(function(data) {
						var saleData = _.find(data.facets, function(facet){return facet.id === 'custitem_ad_name'});
						var saleCategories = _.find(data.facets, function(facet){return facet.id === 'custitem_sale_name'});
						if(saleData && (saleData.values.length > 0)){
							self.sale_data = saleData;
							self.sale_categories = saleCategories;
							SC.saleData = saleData;
							Backbone.trigger('saleDataAvailable', saleData);
						}
						
						self.render();
					});
				}
			});
			
			
		}

	,	childViews: {
			'Header.Profile': function ()
			{
				return new HeaderProfileView({
					showMyAccountMenu: false
				,	application: this.options.application
				});
			}
		,	'Header.Menu.MyAccount': function () 
			{
				return new HeaderMenuMyAccountView(this.options);
			}
		,	'Global.HostSelector': function ()
			{
				return new GlobalViewsHostSelectorView();
			}
		,	'Global.CurrencySelector': function ()
			{
				return new GlobalViewsCurrencySelectorView();
			}
		/*,	'SiteSearch': function()
			{
				return new SiteSearchView();
			}*/
		}

	,	render: function()
		{
			Backbone.View.prototype.render.apply(this, arguments);
			this.$('[data-type="header-sidebar-menu"]').sidebarMenu();
		}

		// @method getContext @return {Header.Sidebar.View.Context}
	,	getContext: function()
		{
			var profile = ProfileModel.getInstance()
			,	is_loading = !_.getPathFromObject(Configuration, 'performance.waitForUserProfile', true) && ProfileModel.getPromise().state() !== 'resolved'
			,	is_loged_in = profile.get('isLoggedIn') === 'T' && profile.get('isGuest') === 'F'
			,	environment = SC.ENVIRONMENT
			,	show_languages = environment.availableHosts && environment.availableHosts.length > 1
			,	show_currencies = environment.availableCurrencies && environment.availableCurrencies.length > 1 && !Configuration.notShowCurrencySelector
			,	urlArray = '';
			
			if(SC.saleData){
				_.each(SC.saleData.values, function(value, index){
					urlArray += (value.url + (index !== SC.saleData.values.length - 1 ? ',' : ''));
				});
			}

			// @class Header.Sidebar.View.Context
			return {
				// @property {Array<NavigationData>} navigationItems
				categories: Configuration.navigationData || []
				// @property {Boolean} showExtendedMenu
			,	showExtendedMenu: !is_loading && is_loged_in
				// @property {Boolean} showLanguages
			,	showLanguages: show_languages
				// @property {Boolean} showCurrencies
			,	showCurrencies: show_currencies
			
			,	saleData: this.sale_data ? this.sale_data : null
			
			,	saleURLArray: urlArray
			
			,	saleCategories: this.sale_categories ? this.sale_categories : null
				
			};
		}
	});

});