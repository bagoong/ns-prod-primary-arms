/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Header
define(
	'Header.View'
,	[
		'SC.Configuration'
	,	'Profile.Model'
	,	'Header.Logo.View'
	,	'Header.MiniCart.View'
	,	'Header.MiniCartSummary.View'
	,	'Header.Profile.View'
	,	'Header.Menu.View'
	,	'GlobalViews.HostSelector.View'
	,	'GlobalViews.CurrencySelector.View'
	,	'SiteSearch.View'
	,	'CustomTracking.Model'
	,	'header.tpl'

	,	'jQuery'
	,	'Backbone'
	,	'Backbone.CompositeView'
	,	'underscore'
	,	'Utils'
	]
,	function(
		Configuration
	,	ProfileModel
	,	HeaderLogoView
	,	HeaderMiniCartView
	,	HeaderMiniCartSummaryView
	,	HeaderProfileView
	,	HeaderMenuView
	,	GlobalViewsHostSelectorView
	,	GlobalViewsCurrencySelectorView
	,	SiteSearchView
	,	CustomTrackingModel
	,	header_tpl

	,	jQuery
	,	Backbone
	,	BackboneCompositeView
	,	_
	)
{
	'use strict';

	// @class Header.View @extends Backbone.View
	return Backbone.View.extend({

		template: header_tpl

	,	events: {
			'click [data-action="show-sitesearch"]': 'showSiteSearch'
		,	'click [data-action="header-sidebar-show"]' : 'showSidebar'
		,	'click [data-action="hide-sitesearch"]': 'hideSiteSearch'
		,	'click [data-action="header-sidebar-hide"]': 'hideSidebar'
		,	'click [data-type="header-sidebar-menu"]': 'hideSidebar'
		,	'click [data-action="trigger-tracking"]': 'trackingModal'
		,	'click [data-action="get-tracking"]': 'getTracking'
		,	'click #tracking-close': 'resetTrackingForm'

		}

	,	initialize: function ()
		{
			var self = this;
			
			BackboneCompositeView.add(this);

			//Comment this out to always show site search
			/*if (_.getDeviceType() !== 'desktop') {
				Backbone.history.on('all', this.verifyShowSiteSearch, this);
			}*/
			
			//This shows the site search after render
			/*this.on('afterViewRender', function() {
				$(document).ready(function() {
					self.$el.find('.header-site-search').show();
				});
			});*/
		}

		// @method verifyShowSiteSearch expand the site search only if hash===home and (phone or tablet)
	,	verifyShowSiteSearch: function ()
		{
			var hash = Backbone.history.getFragment() || '';
			hash = hash.indexOf('?') === -1 ? hash : hash.substring(0, hash.indexOf('?'));
			var is_home = hash === '' || hash === '/';

			if (_.getDeviceType() !== 'desktop' && is_home)
			{
				this.showSiteSearch(null, true);
			}
			else
			{
				// This hide sitesearch when navigate
				this.hideSiteSearch();
			}
		}

		// @method showSiteSearch
	,	showSiteSearch: function (ev)
		{
			ev && ev.preventDefault();

			// This add a class 'active' to change button color
			this.$('[data-action="show-sitesearch"]').toggleClass('active');
			var self = this;

			this.$('[data-type="SiteSearch"]').stop(true, false).slideToggle(function ()
			{
				// Set focus and cleans previous search
				self.childViewInstances.SiteSearch.showSiteSearch();
			});
		}

		//@method hideSiteSearch Auto-hide the site search form
		//@return {Void}
	,	hideSiteSearch: function ()
				{
			var self = this;

			// This hide Sitesearch div
			self.$('[data-type="SiteSearch"]').slideUp();
				}

		// @method showMiniCart
		//@return {Void}
	,	showMiniCart: function ()
		{
			this.$('[data-type="mini-cart"]').parent().addClass('open');
		}

		// @method showSidebar
		//@return {Void}
	,	showSidebar: function ()
		{
			jQuery('#main').addClass('header-sidebar-opened');
		}

		// @method hideSidebar
		//@return {Void}
	,	hideSidebar: function(e)
		{
			if(e.target.tagName === 'A')
			{
				if(this.activeLink)
				{
					this.activeLink.removeClass('active');
				}
				this.activeLink = jQuery(e.target);
				this.activeLink.addClass('active');
			}
			jQuery('#main').removeClass('header-sidebar-opened');
		}

	,	childViews: {
			'Header.MiniCart': function()
			{
				return new HeaderMiniCartView();
			}
		,	'Header.MiniCartSummary': function()
			{
				return new HeaderMiniCartSummaryView();
			}
		,	'Header.Profile': function()
			{
				var header_profile_view_options = _.extend({
					showMyAccountMenu: true
				,	application: this.options.application
				}, this.options.headerProfileViewOptions || {});

				return new HeaderProfileView(header_profile_view_options);
			}
		,	'Header.Logo': function()
			{
				return new HeaderLogoView(this.options);
			}
		,	'Header.Menu': function()
			{
				var header_view_options = _.extend(
					{
					application: this.options.application
					}
					, this.options.headerProfileViewOptions || {});

				return new HeaderMenuView(header_view_options);
			}
		,	'Global.HostSelector': function()
			{
				return new GlobalViewsHostSelectorView();
			}
		,	'Global.CurrencySelector': function()
			{
				return new GlobalViewsCurrencySelectorView();
			}
		,	'SiteSearch': function()
			{
				return new SiteSearchView();
			}
		}

	,	trackingModal: function()
		{
			//Hide the sidebar
			$('#main').removeClass('header-sidebar-opened')
			//Show the tracking Modal
        	$('#trackingModal').modal();
		}

	,	getTracking: function()
		{
    		var salesOrder = $('#sales-order').val();
    		var zipCode = $('#zip-code').val();
    		var trackingModel = new CustomTrackingModel();
			trackingModel.save({salesOrder:salesOrder, zipCode:zipCode}, {
    			error: function(resp) {
    			$('#tracking-message').css("display","block");
    			$('#tracking-message').css("color","red");
    			$('#tracking-message').text("No Tracking Available");
   				}
			}).then(function (resp){
    			$('#tracking-form').css("display","none");
    			$('#tracking-message').css("display","block");
    			$('#tracking-message').css("color","black");
    			$('#tracking-message').text(resp.tracking);
        	});
		}

	,	resetTrackingForm: function()
		{
			$('#tracking-form').css("display","block");
			$('#tracking-message').css("display","none");
			$('#tracking-message').css("color","black");
			$('#tracking-message').text('');
			$('#sales-order').val('');
    		$('#zip-code').val('');
		}

		// @method getContext
		// @return {Header.View.Context}
	,	getContext: function()
		{
			var environment = SC.ENVIRONMENT
			,	show_languages = environment.availableHosts && environment.availableHosts.length > 1
			,	show_currencies = environment.availableCurrencies && environment.availableCurrencies.length > 1 && !Configuration.notShowCurrencySelector;

			// @class Header.View.Context
			return {
				// @property {Profile.Model} profileModel
				profileModel: ProfileModel.getInstance()
				// @property {Boolean} showLanguages
			,	showLanguages: show_languages
				// @property {Boolean} showCurrencies
			,	showCurrencies: show_currencies
				// @property {Boolean} showLanguagesOrCurrencies
			,	showLanguagesOrCurrencies: show_languages || show_currencies
				// @property {Boolean} showLanguagesAndCurrencies
			,	showLanguagesAndCurrencies: show_languages && show_currencies
				// @property {Boolean} isHomeTouchpoint
			,	isHomeTouchpoint: Configuration.currentTouchpoint === 'home'
				// @property {String} cartTouchPoint
			,	cartTouchPoint: Configuration.modulesConfig.Cart && Configuration.modulesConfig.Cart.startRouter ? Configuration.currentTouchpoint : 'viewcart'
			
			,	isMobile: _.isPhoneDevice() || _.isTabletDevice()
			};
			// @class Header.View
		}

		//@method destroy Override default destroy method to clean navigation event handlers
		//@return {Void}
	,	destroy: function ()
		{
			Backbone.View.prototype.destroy.apply(this, arguments);

			Backbone.history.off('all', this.verifyShowSiteSearch);
		}
	});

});
