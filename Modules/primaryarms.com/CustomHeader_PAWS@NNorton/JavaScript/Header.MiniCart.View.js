/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Header
define(
	'Header.MiniCart.View'
,	[
		'LiveOrder.Model'
	,	'Header.MiniCartSummary.View'
	,	'Header.MiniCartItemCell.View'
	,	'Profile.Model'

	,	'SC.Configuration'

	,	'header_mini_cart.tpl'

	,	'underscore'
	,	'Backbone'
	,	'Backbone.CompositeView'
	,	'Backbone.CollectionView'
	,	'underscore'
	,	'Utils'
	]
,	function(
		LiveOrderModel
	,	HeaderMiniCartSummaryView
	,	HeaderMiniCartItemCellView
	,	ProfileModel
	,	Configuration

	,	header_mini_cart_tpl

	,	_
	,	Backbone
	,	BackboneCompositeView
	,	BackboneCollectionView
	)
{
	'use strict';

	// @class Header.MiniCart.View @extends Backbone.View
	return Backbone.View.extend({

		template: header_mini_cart_tpl

	,	initialize: function()
		{
			BackboneCompositeView.add(this);

			var self = this;

			this.isLoading = true;
			this.itemsInCart = 0;

			LiveOrderModel.loadCart().done(function ()
			{
				var cart = LiveOrderModel.getInstance();
			
				self.itemsInCart = cart.getTotalItemCount();
			
				self.isLoading = false;
				self.render();

				cart.on('change', function ()
				{
					self.itemsInCart = cart.getTotalItemCount();
					self.render();
				});
			});
		}

	,	render: function()
		{
			Backbone.View.prototype.render.apply(this, arguments);

			//on tablet or desktop make the minicart dropdown
			if ( _.isTabletDevice() || _.isDesktopDevice())
			{
				this.$('[data-type="mini-cart"]').attr('data-toggle', 'dropdown');
			}

		}

	,	childViews: {
			'Header.MiniCartSummary': function()
			{
				return new HeaderMiniCartSummaryView();
			}
		,	'Header.MiniCartItemCell': function()
			{
				return new BackboneCollectionView({
					collection: !this.isLoading ? LiveOrderModel.getInstance().get('lines') : new Backbone.Collection()
				,	childView: HeaderMiniCartItemCellView
				,	viewsPerRow: 1
				});
			}
		}

		// @method getContext @return {Header.MiniCart.View.Context}
	,	getContext: function()
		{
			var summary = LiveOrderModel.getInstance().get('summary')
			,	lines = !this.isLoading ? LiveOrderModel.getInstance().get('lines') : ''
			,	items = lines !== '' ? _.map(lines.models, function(line){return line.get('item');}) : []
			,	prevent_checkout = false
			,	prevent_checkout_message = ''
			,	price_level = SC.SESSION ? SC.SESSION.priceLevel : SC.DEFAULT_SESSION.priceLevel;
			
			//Prevent checkout if some items are out of stock
			if (_.some(items, function(item){return item.get('_isInStock') === false && !(item.get('isbackorderable'));}))
			{
				prevent_checkout = true;
				prevent_checkout_message += "<p><i class='fa fa-exclamation-triangle'></i> Unfortunately some of the items in your cart are no longer in stock. Please remove the items in order to checkout.</p>";
			}
			
			//Function called to check limits and quantity available preventing checkout if limit exceeded or quantity available is too low
			function checkLimits(price_level_limit){
				_.each(items, function(item){
						if (item.get(price_level_limit) || item.get(price_level_limit) === 0 ? (item.get('quantity') > item.get(price_level_limit)) : false)
						{
							prevent_checkout = true;
							prevent_checkout_message += "<p><i class='fa fa-exclamation-triangle'></i> " + item.get('_sku') + " has an order limit of " + item.get(price_level_limit) + ". Please reduce the Qty within this limit.</p>";
						}

						//If item is not a gift certificate and is not backordable then check quantity
						if (item.get('itemtype') !== 'GiftCert' && !(item.get('isbackorderable'))){
							if (item.get('quantity') > item.get('quantityavailable'))
							{
								prevent_checkout = true;
								prevent_checkout_message += "<p><i class='fa fa-exclamation-triangle'></i> " + item.get('_sku') + " does not have enough available inventory for your purchase.<br>Please reduce your order Qty.</p>";
							}
						}
					});
			}
			
			//Check the price level and pass in the current limit to use
			switch(price_level) {
				case '5':
					checkLimits('custitem_active_customer_limit');
					break;
				case '2':
					checkLimits('custitem_active_dealer_1_limit');
					break;
				case '6':
					checkLimits('custitem_active_dealer_2_limit');
					break;
				case '7':
					checkLimits('custitem_active_dealer_3_limit');
					break;
				case '10':
					checkLimits('custitem_active_mil_limit');
					break;
				default:
					checkLimits('custitem_active_customer_limit');
			}

			// @class Header.MiniCart.View.Context
			return {
				// @property {Number} itemsInCart
				itemsInCart: this.itemsInCart
				// @property {Boolean} showPluraLabel
			,	showPluraLabel: this.itemsInCart !== 1

				// @property {Boolean} showLines
			,	showLines: this.itemsInCart > 0
				// @property {Boolean} isLoading
			,	isLoading: this.isLoading

				// @property {Boolean} subTotal
			,	subTotal: false

				// @property {String} subtotalFormatted
			,	subtotalFormatted: !this.isLoading ? (summary && summary.subtotal_formatted) : ''
				// @property {OrderLine.Collection} lines
			,	lines: !this.isLoading ? LiveOrderModel.getInstance().get('lines') : new Backbone.Collection()

				// @property {String} cartTouchPoint
			,	cartTouchPoint: _.getPathFromObject(Configuration, 'modulesConfig.Cart.startRouter', false) ? Configuration.currentTouchpoint : 'viewcart'
				// @property {Boolean} isPriceEnabled
			,	isPriceEnabled: !ProfileModel.getInstance().hidePrices()
			
			,	preventCheckout: prevent_checkout
			
			,	preventCheckoutMessage: prevent_checkout_message
			};
		}
	});

});