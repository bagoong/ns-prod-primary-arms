define('Data.Model',
  [
  'Backbone',
  'underscore'
  ],
  function (Backbone, _) {
    return Backbone.Model.extend({
      urlRoot: _.getAbsoluteUrl('services/Data.Service.ss')
    });
  }
)