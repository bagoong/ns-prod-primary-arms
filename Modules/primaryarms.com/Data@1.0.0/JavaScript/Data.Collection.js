define('Data.Collection',
  [
  'Backbone',
  'Data.Model'
  ],
  function (Backbone, Model) {
    return Backbone.Collection.extend({
      model: Model,
      url: _.getAbsoluteUrl('services/Data.Service.ss')
    });
  }
);