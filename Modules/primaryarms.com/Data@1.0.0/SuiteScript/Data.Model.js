define('Data.Model',
  [
  'SC.Model',
  'Utils.Fedex'
  ],
  function (SCModel,fedex) {
    return SCModel.extend({
      name: 'Data',
      get: function(action,zip,length,width,height,country,weight){


              var myFedex = new fedex.getRates()
              myFedex.initialize({
                weight:weight,
                zip:zip,
                length:length,
                width:width,
                height:height,
                country:country
              })
              myFedex.RateRequest()
              return myFedex.Rates()
      } 
    });
  }
)