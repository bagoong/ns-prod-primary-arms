{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}
<div class="header-message" data-type="message-placeholder"></div>
<div class="home-cms-page-banner-top home-banner-top-message" data-cms-area="home_message_top" data-cms-area-filters="global"></div>
<div class="header-main-wrapper">
  <nav class="header-main-nav">
    <div id="banner-header-top" class="content-banner banner-header-top" data-cms-area="header_banner_top" data-cms-area-filters="global"></div>
    <!--<div class="header-sidebar-toggle-wrapper">
      <button class="header-sidebar-toggle" data-action="header-sidebar-show"> <i class="header-sidebar-toggle-icon"></i> </button>
    </div>--> 
    {{#if isMobile}}<!--Mobile Version of the Header area-->
    <div class="header-content">
      <div class="row">
        <div class="header-sidebar-toggle-wrapper">
          <button class="header-sidebar-toggle" data-action="header-sidebar-show"> <i class="header-sidebar-toggle-icon"></i></button><a data-action="header-sidebar-show"> MENU</a>
        </div>
        <div class="header-right-menu">
          <div class="header-menu-profile" data-view="Header.Profile"> </div>
          {{#if showLanguagesOrCurrencies}}
          <div class="header-menu-settings"> <a href="#" class="header-menu-settings-link" data-toggle="dropdown" title="{{translate 'Settings'}}"> <i class="header-menu-settings-icon"></i> <i class="header-menu-settings-carret"></i> </a>
            <div class="header-menu-settings-dropdown">
              <h5 class="header-menu-settings-dropdown-title">{{translate 'Site Settings'}}</h5>
              {{#if showLanguages}}
              <div data-view="Global.HostSelector"></div>
              {{/if}}
              {{#if showCurrencies}}
              <div data-view="Global.CurrencySelector"></div>
              {{/if}} </div>
          </div>
          {{/if}}
          <div class="header-menu-searchmobile">
            <button class="header-menu-searchmobile-link" data-action="show-sitesearch" title="{{translate 'Search'}}"> <i class="header-menu-searchmobile-icon"></i> </button>
          </div>
          <div class="header-menu-cart">
            <div class="header-menu-cart-dropdown" >
              <div data-view="Header.MiniCart"></div>
            </div>
          </div>
        </div>
        <div class="header-logo-wrapper">
          <div data-view="Header.Logo"></div>
        </div>
      </div>
      <div class="row">
      	<div class="header-contact-info">
          <div class="header-contact-info-list">
            <ul class="header-contact-info-ul">
              <li><i class="fa fa-envelope"></i> info@primaryarms.com</li>
              <li> | </li>
              <li><i class="fa fa-phone"></i><a href="tel: 17133449600">713-344-9600</a></li>
            </ul>
          </div>
        </div>
        <div class="header-site-search" data-view="SiteSearch" data-type="SiteSearch"></div>
      </div>
    </div>
    {{else}}<!--Desktop Version of the Header area-->
    <div class="header-content">
      <div class="row">
        <div class="header-sidebar-toggle-wrapper">
          <button class="header-sidebar-toggle" data-action="header-sidebar-show"> <i class="header-sidebar-toggle-icon"></i> </button>
        </div>
        <div class="header-right-menu">
          <div class="header-menu-profile" data-view="Header.Profile"> </div>
          {{#if showLanguagesOrCurrencies}}
          <div class="header-menu-settings"> <a href="#" class="header-menu-settings-link" data-toggle="dropdown" title="{{translate 'Settings'}}"> <i class="header-menu-settings-icon"></i> <i class="header-menu-settings-carret"></i> </a>
            <div class="header-menu-settings-dropdown">
              <h5 class="header-menu-settings-dropdown-title">{{translate 'Site Settings'}}</h5>
              {{#if showLanguages}}
              <div data-view="Global.HostSelector"></div>
              {{/if}}
              {{#if showCurrencies}}
              <div data-view="Global.CurrencySelector"></div>
              {{/if}} </div>
          </div>
          {{/if}}
          <div class="header-menu-searchmobile">
            <button class="header-menu-searchmobile-link" data-action="show-sitesearch" title="{{translate 'Search'}}"> <i class="header-menu-searchmobile-icon"></i> </button>
          </div>
          <!--<div class="header-menu-quote" data-view="RequestQuoteWizardHeaderLink"> </div>-->
          <div class="header-menu-cart">
            <div class="header-menu-cart-dropdown" >
              <div data-view="Header.MiniCart"></div>
            </div>
          </div>
        </div>
        <div class="header-contact-info"> 
          <!--<div class="header-contact-info-email-button">
                  <a href="" class="header-contact-info-email-button-link button-primary">Email Sign Up</a>
                </div>-->
          <div class="header-contact-info-list">
            <ul class="header-contact-info-ul">
              <li><i class="fa fa-envelope"></i> info@primaryarms.com</li>
              <li> | </li>
              <li><i class="fa fa-phone"></i><a href="tel: 17133449600">713-344-9600</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="header-logo-wrapper">
          <div data-view="Header.Logo"></div>
        </div>
        <a href="/email-sign-up" data-touchpoint="home" data-hashtag="#/email-sign-up"><div id="header-email-sign-up-button"><i class="fa fa-envelope"></i> Email Sign Up</div></a>
        <div class="header-site-search" data-view="SiteSearch" data-type="SiteSearch"></div>
      </div>
      <!-- <div class="header-menu-search">
          <div class="header-site-search" data-view="SiteSearch" data-type="SiteSearch"></div>
      </div>--> 
    </div>
    {{/if}}<!--END Header content-->
    <div id="banner-header-bottom" class="content-banner banner-header-bottom" data-cms-area="header_banner_bottom" data-cms-area-filters="global"></div>
  </nav>
</div>
<div class="header-sidebar-overlay" data-action="header-sidebar-hide"></div>
<div class="header-secondary-wrapper" data-view="Header.Menu" data-phone-template="header_sidebar" data-tablet-template="header_sidebar"> </div>
<!-- Top header message -->
<section class="home-cms-page-banner-top home-banner-top-message" data-cms-area="home_message" data-cms-area-filters="global"></section>

<!-- Modal -->
<div class="modal fade" id="trackingModal" tabindex="-1" role="dialog" aria-labelledby="trackingModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="row">
          <div class="col-sm-6">
            <h4 class="modal-title" id="trackingModalLabel">Find My Order</h4>
          </div>
          <div class="col-sm-6" style="text-align: right;">
            <button type="button" class="global-views-modal-content-header-close" id="tracking-close" data-dismiss="modal" aria-hidden="true" aria-label="Close"> × </button>
          </div>
        </div>
      </div>
      <div class="modal-body">
      	<div class="row">
            <div id="tracking-message" class="col-xs-12"></div>
            <div id="tracking-form" class="form-group">
            	<div class="col-xs-12 col-sm-6">
              		<label class="header-tracking-label" for="sales-order">Sales Order:</label>
              		<input type="text" class="header-tracking-input" id="sales-order" name="sales-order">
              	</div>
                <div class="col-xs-12 col-sm-6">
                  <label class="header-tracking-label" for="zip-code">Billing Zip Code:</label>
                  <input type="text" class="header-tracking-input" id="zip-code" name="zip-code">
                </div>
                <div class="col-xs-12">
              		<button data-action="get-tracking" class="get-tracking-send-button" id="get-tracking-send-button">Send</button>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer"> </div>
    </div>
  </div>
</div>
