{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<nav class="header-menu-secondary-nav">
	<!--<div class="header-menu-search">
    	<div class="header-site-search" data-view="SiteSearch" data-type="SiteSearch"></div>
	</div>-->
	<ul class="header-menu-level1">
        <li class="header-menu-item">
        	<a class="header-menu-home-anchor" id="home-icon" href="/" data-touchpoint="home" data-hashtag="#/"><i class="fa fa-home header-menu-home-icon"></i></a>
        </li>
		{{#each categories}}
		<li class="header-menu-item" id="{{id}}">
			<a class="{{class}}" {{objectToAtrributes this}}>
			{{text}}
			</a>
			{{#if categories}}
                <div class="header-menu-level-container-dropdown" id="{{text}}">
                	{{#if group}}
                        {{#if ../../../saleData}}
                        	<div class="col-xs-1"></div>
                            {{#each ../../../../saleData.values}}
                                <div class="col-xs-2" style="padding: 0px;">
                                    <ul>
                                        <li class="header-menu-level3-menu">
                                            <a href="/{{../../../../../saleData.url}}+{{this.url}}" data-touchpoint="home" data-hashtag="#/{{../../../../../saleData.url}}+{{this.url}}">
                                            {{this.label}}
                                            <!--{{#if ../../../../../saleCategories.values}}<i class="fa fa-angle-down" style="margin-right: 10%;"></i>{{/if}}-->
                                            </a>
                                            <!--{{#if ../../../../../saleCategories.values}}
                                            	<ul class="header-menu-level3">
                                            	{{#each ../../../../../../saleCategories.values}}
                                            		<li class="header-menu-level3-menu">
                                                        <a href="/{{../../../../../../../saleCategories.url}}+{{url}}" data-touchpoint="home" data-hashtag="#/{{../../../../../../../saleCategories.url}}+{{url}}">{{label}}</a>
                                                    </li>
                                            	{{/each}}
                                                </ul>
                                            {{/if}}-->
                                        </li>
                                    </ul>
                                </div>
                            {{/each}}
                        {{/if}}
                        <div class="col-xs-1" style="clear: left;"></div>
                        {{#each categories}}
                            {{#if class}}
                                <a class="{{class}} {{widthClass}}">{{{text}}}</a>
                            {{/if}}
                        {{/each}}
                        <div class="col-xs-1"></div>
                        <div class="col-xs-1" style="clear: left;"></div>
                        {{#each categories}}
                            <div class="col-xs-2" style="padding: 0px;">
                                <ul>
                                    {{#if categories}}
                                        {{#each categories}}
                                            <li class="{{class}}">
                                            <a {{objectToAtrributes this}}  style="display: block">{{text}}{{#if categories}}<i class="fa fa-angle-down" style="margin-right: 10%;"></i>{{/if}}</a>
                                            {{#if categories}}
                                                <ul class="header-menu-level3">
                                                {{#each categories}}
                                                    <li class="{{class}}">
                                                    <a {{objectToAtrributes this}}>{{text}}</a>
                                                    </li>
                                                {{/each}}
                                                </ul>
                                            {{/if}}
                                            </li>
                                        {{/each}}
                                    {{/if}}
                                </ul>
                            </div>
                        {{/each}}
                    {{else}}
                    	{{#each categories}}
                            <div class="header-menu-item-level2">
                                <a class="{{class}}" {{objectToAtrributes this}} style="display: block">{{text}}{{#if categories}}<i class="fa fa-angle-right"></i>{{/if}}</a>
                                {{#if categories}}
                                    <div class="header-menu-level3" id="{{text}}">
                                        {{#each categories}}
                                            <a class="{{class}}" {{objectToAtrributes this}} style="display: block">{{text}}</a>
                                        {{/each}}
                                    </div>
                                {{/if}}
                            </div>
                        {{/each}}
                    {{/if}}
                </div>
			{{/if}}
		</li>
		{{/each}}
        {{#if saleData}}
        <li class="header-menu-item" id="sale-menu-item-container">
        	<a class="header-menu-home-anchor" id="sale-menu-item" href="/{{saleData.url}}+{{saleURLArray}}" data-touchpoint="home" data-hashtag="#/{{saleData.url}}+{{saleURLArray}}">Sale</a>
            <!--{{#if saleCategories}}
                <div class="header-menu-level-container-dropdown" id="sale-dropdown">
                	{{#each ../saleData.values}}
                        <div class="header-menu-item-level2">
                            <a class="header-menu-level3-menu"  href="/{{../../saleData.url}}+{{url}}" data-touchpoint="home" data-hashtag="#/{{../../saleData.url}}+{{url}}" style="display: block">{{label}}</a>
                        </div>
                    {{/each}}
                	{{#each saleCategories.values}}
                        <div class="header-menu-item-level2">
                            <a class="header-menu-level3-menu"  href="/{{../saleCategories.url}}+{{url}}" data-touchpoint="home" data-hashtag="#/{{../saleCategories.url}}+{{url}}" style="display: block">{{label}}</a>
                        </div>
                    {{/each}}
                </div>
            {{/if}}-->
        </li>
        {{/if}}
        <!--<li>
        	<div class="header-menu-home-anchor" data-cms-area="header_menu_item" data-cms-area-filters="global">
            </div>
        </li>-->

	</ul>

</nav>
