{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<!--
	Additional logic was added to check if the current touchpoint is the customercenter.
	If it is, we do not need data-touchpoint and data-hashtag on the links.
	Using #unless isCustomerCenter
-->

<a class="header-menu-myaccount-anchor" href="#" data-action="push-menu" name="myaccount">
	{{translate 'My Account'}}
	<i class="header-menu-myaccount-menu-push-icon"></i>
</a>

<ul class="header-menu-myaccount">
	<li>
		<a href="#" class="header-menu-myaccount-back" data-action="pop-menu" name="back">
			<i class="header-menu-myaccount-pop-icon "></i>
			{{translate 'Back'}}
		</a>
	</li>
	<li class="header-menu-myaccount-overview">
		<a class="header-menu-myaccount-overview-anchor" href="/overview" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#overview"{{/unless}} name="accountoverview">
			{{translate 'Account Overview'}}
		</a>

		<a class="header-menu-myaccount-signout-link" href="#" data-touchpoint="logout" name="signout">
			<i class="header-menu-myaccount-signout-icon"></i>
			{{translate 'Sign Out'}}
		</a>
	</li>

	<li class="header-menu-myaccount-item-level2 header-menu-myaccount-level2-orders" data-permissions="{{purchasesPermissions}}" data-permissions-operator="OR">
		<a class="header-menu-myaccount-anchor-level2" href="#" data-action="push-menu" name="orders">
			{{translate 'Purchases'}}
			<i class="header-menu-myaccount-menu-push-icon"></i>
		</a>
		<ul class="header-menu-myaccount-level3 header-menu-myaccount-level3-orders">
			<li>
				<a href="#" class="header-menu-myaccount-back" data-action="pop-menu" name="back-level3">
					<i class="header-menu-myaccount-pop-icon "></i>
					{{translate 'Back'}}
				</a>
			</li>
			<li data-permissions="{{purchasesPermissions}}">
				<a class="header-menu-myaccount-anchor-level3" href="/purchases" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#purchases"{{/unless}} name="orderhistory">
					{{translate 'Purchases History'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" href="/returns" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#returns"{{/unless}} data-permissions="{{returnsPermissions}}" name="returns">
					{{translate 'Returns'}}
				</a>
			</li>
			<li data-permissions="{{purchasesPermissions}}">
				<a class="header-menu-myaccount-anchor-level3" href="/reorderItems" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#reorderItems"{{/unless}} name="reorderitems">
					{{translate 'Reorder Items'}}
				</a>
			</li>
			<li class="header-menu-myaccount-item-level3" data-permissions="transactions.tranFind.1,transactions.tranEstimate.1">
				<a class="header-menu-myaccount-anchor-level3" data-action="push-menu" href="/quotes" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#quotes"{{/unless}} name="quotes">
					{{translate 'Quotes'}}
					<i class="header-menu-myaccount-menu-push-icon"></i>
				</a>
				<ul class="header-menu-myaccount-level4">
					<li>
						<a href="#" class="header-menu-myaccount-back" data-action="pop-menu" name="back-level4">
							<i class="header-menu-myaccount-pop-icon "></i>
							{{translate 'Back'}}
						</a>
					</li>
					<li>
						<a class="header-menu-myaccount-anchor-level4" href="/quotes" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#quotes"{{/unless}} name="allmyquotes">
							{{translate 'All my Quotes'}}
						</a>
					</li>
					{{#if hasProductList}}
						<li>
							{{#if hasNoItem}}
								<a class="header-menu-myaccount-anchor-level4" href="/quotes/new" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#quotes/new"{{/unless}} name="requestaquotes">
									{{translate 'Request a Quote'}}
								</a>
							{{else}}

								{{#if hasItemInBasket}}
									<a class="header-menu-myaccount-anchor-level4" href="/quotebasket" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#quotebasket"{{/unless}} name="quotebasket">
										{{translate 'Quote basket <span>(300)</span>'}}
									</a>
								{{/if}}
							{{/if}}
						</li>
					{{/if}}
				</ul>
			</li>
		</ul>
	</li>

	<!-- Product Lists - For single list mode data-hashtag will be added dynamically -->
	{{#if isProductListsEnabled}}
		<li class="header-menu-myaccount-item-level2">
			<a class="header-menu-myaccount-anchor-level2" href="/wishlist" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#wishlist"{{/unless}} name="wishlist">
				{{translate 'Wishlist'}}
			</a>

			<ul class="header-menu-myaccount-level3">
				{{#if productListsReady}}
					{{#unless isSingleList}}
						<li>
							<a href="/wishlist" class="header-menu-myaccount-anchor-level3" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#wishlist"{{/unless}} name="allmylists">
								{{translate 'All my lists'}}
							</a>
						</li>
					{{/unless}}
					{{#each productLists}}
					<li>
						<a href="/{{url}}" class="header-menu-myaccount-anchor-level3" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="{{url}}"{{/unless}} name="{{name}}">
							{{name}} ({{ items.length }})
						</a>
					</li>
					{{/each}}
				{{else}}
					<li>
						<a href="#" class="header-menu-myaccount-anchor-level3">
							{{translate 'Loading...'}}
						</a>
					</li>
				{{/if}}
			</ul>
		</li>
	{{/if}}


	<!-- Billing -->
	<li class="header-menu-myaccount-item-level2">
		<a class="header-menu-myaccount-anchor-level2" href="#" data-action="push-menu" name="billing">
			{{translate 'Billing'}}
			<i class="header-menu-myaccount-menu-push-icon"></i>
		</a>
		<ul class="header-menu-myaccount-level3">
			<li>
				<a href="#" class="header-menu-myaccount-back" data-action="pop-menu" name="back-level3">
					<i class="header-menu-myaccount-pop-icon "></i>
					{{translate 'Back'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" tabindex="-1" href="/balance" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#balance"{{/unless}} name="accountbalance">{{translate 'Account Balance'}}</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" tabindex="-1" href="/invoices" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#invoices"{{/unless}} data-permissions="transactions.tranCustInvc.1" name="invoices">{{translate 'Invoices'}}</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" tabindex="-1" href="/transactionhistory" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#transactionhistory"{{/unless}} data-permissions="transactions.tranCustInvc.1, transactions.tranCustCred.1, transactions.tranCustPymt.1, transactions.tranCustDep.1, transactions.tranDepAppl.1" data-permissions-operator="OR" name="transactionhistory">{{translate 'Transaction History'}}</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" tabindex="-1" href="/printstatement" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#printstatement"{{/unless}} data-permissions="transactions.tranStatement.2" name="printastatement">{{translate 'Print a Statement'}}</a>
			</li>
		</ul>
	</li>

	<!-- Settings -->
	<li class="header-menu-myaccount-item-level2">
		<a class="header-menu-myaccount-anchor-level2" tabindex="-1" href="#" data-action="push-menu" name="settings">
			{{translate 'Settings'}}
			<i class="header-menu-myaccount-menu-push-icon"></i>
		</a>
		<ul class="header-menu-myaccount-level3">
			<li>
				<a href="#" class="header-menu-myaccount-back" data-action="pop-menu" name="back-level3">
					<i class="header-menu-myaccount-pop-icon "></i>
					{{translate 'Back'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" href="/profileinformation" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#profileinformation"{{/unless}} name="profileinformation">
					{{translate 'Profile Information'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" href="/emailpreferences" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#emailpreferences"{{/unless}} name="emailpreferences">
					{{translate 'Email Preferences'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" href="/addressbook" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#addressbook"{{/unless}} name="addressbook">
					{{translate 'Address Book'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" href="/creditcards" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#creditcards"{{/unless}} name="creditcards">
					{{translate 'Credit Cards'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" href="/updateyourpassword" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#updateyourpassword"{{/unless}} name="updateyourpassword">
					{{translate 'Update Your Password'}}
				</a>
			</li>
		</ul>
	</li>

	{{#if isCaseModuleEnabled}}
	<li class="header-menu-myaccount-item-level2" data-permissions="lists.listCase.2">
		<a  class="header-menu-myaccount-anchor-level2" tabindex="-1" href="#" data-action="push-menu" name="cases">
			{{translate 'Cases'}}
			<i class="header-menu-myaccount-menu-push-icon"></i>
		</a>
		<ul class="header-menu-myaccount-level3">
			<li>
				<a href="#" class="header-menu-myaccount-back" data-action="pop-menu" name="back-level3">
					<i class="header-menu-myaccount-pop-icon "></i>
					{{translate 'Back'}}
				</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" tabindex="-1" href="/cases" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#cases"{{/unless}} name="allmycases">{{translate 'Support Cases'}}</a>
			</li>
			<li>
				<a class="header-menu-myaccount-anchor-level3" tabindex="-1" href="/newcase" {{#unless isCustomerCenter}}data-touchpoint="customercenter" data-hashtag="#newcase"{{/unless}} name="submitnewcase">{{translate 'Submit New Case'}}</a>
			</li>
		</ul>
	</li>
	{{/if}}

</ul>