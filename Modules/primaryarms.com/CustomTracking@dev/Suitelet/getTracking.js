function getTracking(){
    var obj = [];
    var salesOrder = request.getParameter('salesOrder');
    var zipCode = request.getParameter('zipCode');
    var filters = [
        new nlobjSearchFilter('tranid', null, 'is', salesOrder),
        new nlobjSearchFilter('shipzip', null, 'is', zipCode),
        new nlobjSearchFilter('mainline', null, 'is', true)
    ];
    var columns = [
        new nlobjSearchColumn('trackingnumbers',null,'max')
    ];
    var tracking = '';
    var searchResults = nlapiSearchRecord('transaction', null, filters,columns);
    nlapiLogExecution('DEBUG', searchResults.length, salesOrder + " || " + zipCode);
    if(searchResults && salesOrder && zipCode){
        tracking = searchResults[0].getValue('trackingnumbers',null,'max');
        if(tracking){
            obj = {
                tracking : "Your tracking number is " + tracking
            };
        }else{
            obj = {
                tracking : "No tracking available"
            }
        }
    }else{
        obj = {
            tracking: "Something appears to be wrong, please check your SO Number and your Shipping Zip Code. If you continue having a problems with this form please contact Customer Service at 713-344-9600, visit Contact Us, or email info@primaryarms.com"
        }
    }

    response.setContentType('json');
    response.write(JSON.stringify(obj));

}
