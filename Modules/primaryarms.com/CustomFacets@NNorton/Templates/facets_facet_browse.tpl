{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<section class="facets-facet-browse">
	<div data-cms-area="item_list_banner" data-cms-area-filters="page_type"></div>

	{{#if showResults}}
		<header class="facets-facet-browse-header">
			<div>
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<h1 class="facets-facet-browse-title" data-quantity="{{total}}" style="font-size: 19px;">
							{{#if keywords}}
								{{#if isTotalProductsOne}}
									{{translate '1 Result for <span class="facets-facet-browse-title-alt">$(0)</span>' keywords}}
								{{else}}
									{{translate '$(0) Results for <span class="facets-facet-browse-title-alt">$(1)</span>' total keywords}}
								{{/if}}
							{{else}}
								{{#if isTotalProductsOne}}
									{{translate '1 '}}<span class="pa-cta">{{category}}</span>{{translate ' Product'}}
								{{else}}
									{{translate '$(0) ' total}}<span class="pa-cta">{{category}}</span>{{translate ' Products'}}
								{{/if}}
							{{/if}}
						</h1>

						<nav class="facets-facet-browse-list-header">

							
							<div class="facets-facet-browse-list-header-actions" data-view="Facets.ItemListDisplaySelector"></div>

							<div class="facets-facet-browse-list-header-expander">
								<button class="facets-facet-browse-list-header-expander-button collapsed" data-toggle="collapse" data-target="#list-header-filters" aria-expanded="true" aria-controls="list-header-filters">
									{{translate 'Sort & Filter'}}
									<i class="facets-facet-browse-list-header-expander-icon"></i>
								</button>
							</div>

							<div class="facets-facet-browse-list-header-filters collapse" id="list-header-filters">
								<div class="facets-facet-browse-list-header-filters-wrapper">

									<div class="facets-facet-browse-list-header-filters-row">

										<div class="facets-facet-browse-list-header-filter-column" data-view="Facets.ItemListShowSelector">
										</div>

										<div class="facets-facet-browse-list-header-filter-column" data-view="Facets.ItemListSortSelector">
										</div>

										
									</div>

								</div>
							</div>
                            
                            {{#if hasItemsAndFacets}}
                            <br>
                                <button class="facets-facet-browse-list-header-filter-facets" data-type="sc-pusher" data-target="product-search-facets">
                                    {{translate 'Narrow By'}}
                                    <i class="facets-facet-browse-list-header-filter-facets-icon"></i>
                                </button>
                            {{/if}}
                            <div class="show-stock">
                                <a id="stock-toggle" href="{{currentFacets}}{{inStockFacet}}{{currentParameters}}">
                                    <label for="stock-toggle" id="stock-toggle-label">
                                        <input type="checkbox" {{checked}} name="stock-toggle" />
                                        Hide Out of Stock
                                    </label>
                                </a>
                            </div>
						</nav>
					</div>
				</div>
			</div>	
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="item-compare itemCompareCont" style="display:none;">
							<input type="button" name="itemCompare" id="itemCompare" class="facets-item-cell-grid-add-to-cart-button" value="Compare Items"/>
                            <a style="color: white; margin-left: 10px;" data-action="item-compare-clear">CLEAR <i class="fa fa-times-circle" aria-hidden="true"></i></a>
						</div>
						<!--End Item Compare-->
					</div>
				</div>
			</div>
		</header>

		<div class="facets-facet-browse-content">

				<div data-cms-area="facet_navigation_top" data-cms-area-filters="page_type"></div>
				<div class="facets-facet-browse-facets" data-action="pushable" data-id="product-search-facets" data-view="Facets.FacetedNavigation" data-exclude-facets="category">
				</div>

				<div data-cms-area="facet_navigation_bottom" data-cms-area-filters="page_type"></div>
				<!--
				Sample of how to add a particular facet into the HTML. It is important to specify the data-facet-id="<facet id>"
				properly <div data-view="Facets.FacetedNavigation.Item" data-facet-id="custitem1"></div>
				 -->

				<div class="facets-facet-browse-results">
					<meta itemprop="name" content="{{title}}"/>
					<div id="banner-section-top" class="content-banner banner-section-top" data-cms-area="item_list_banner_top" data-cms-area-filters="path"></div>
					
					


					<div class="facets-facet-browse-narrowedby" data-view="Facets.FacetsDisplay">
					</div>

					{{#if isEmptyList}}
						<div data-view="Facets.Items.Empty">
						</div>
					{{else}}
						<div class="facets-facet-browse-items" data-view="Facets.Items">
						</div>
					{{/if}}


		</div>

		<div class="facets-facet-browse-pagination" data-view="GlobalViews.Pagination">
		</div>
	{{else}}
		<div class="facets-facet-browse-empty-items" data-view="Facets.Items.Empty">
		</div>
	{{/if}}
	
	<div id="banner-section-bottom" class="content-banner banner-section-bottom" data-cms-area="item_list_banner_bottom" data-cms-area-filters="page_type"></div>
    <div data-view="RecentlyViewed.Items" class="facet-recently-viewed"></div>
</section>

<!-- Modal -->
<div class="modal fade" id="compareModal" tabindex="-1" role="dialog" aria-labelledby="CompareModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      	<div class="row">
		  <div class="col-sm-6">
		  	<h4 class="modal-title" id="CompareModalLabel">Item Compare</h4>
		  </div>
		  <div class="col-sm-6" style="text-align: right;">
		  	<!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times; Close</span></button> -->
		  	<button type="button" class="global-views-modal-content-header-close" data-dismiss="modal" aria-hidden="true" aria-label="Close"> × </button>
		  </div>
		</div>
        
        
      </div>
      <div class="modal-body">
        {{{itemCompare}}}
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>
  </div>
</div>