/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Facets
define('Facets.Browse.View'
,	[	'SC.Configuration'
	,	'LiveOrder.Model'
	,	'Facets.Helper'
	,	'Categories'
	,	'Facets.FacetedNavigation.View'
	,	'Facets.FacetedNavigationItem.View'
	,	'Facets.FacetsDisplay.View'
	,	'Facets.ItemListDisplaySelector.View'
	,	'Facets.ItemListSortSelector.View'
	,	'Facets.ItemListShowSelector.View'
	,	'Facets.ItemCell.View'
	,	'Facets.Empty.View'
	,	'GlobalViews.Pagination.View'
	,	'Tracker'
	,	'RecentlyViewedItems.View'

	,	'ItemDetails.ImageGallery.View'
	,	'ItemViews.Price.View'
	,	'ItemDetails.Model'

	,	'facets_facet_browse.tpl'
	,	'facets_items_collection.tpl'
	,	'facets_items_collection_view_cell.tpl'
	,	'facets_items_collection_view_row.tpl'

	,	'Backbone'
	,	'Backbone.CollectionView'
	,	'Backbone.CompositeView'
	,	'EmailBackInStock.Model'
	,	'underscore'
	,	'jQuery'
	,	'Bootstrap.Slider'
	,	'Utils'
	]
,	function (
		Configuration
	,	LiveOrderModel
	,	Helper
	,	Categories
	,	FacetsFacetedNavigationView
	,	FacetsFacetedNavigationItemView
	,	FacetsFacetsDisplayView
	,	FacetsItemListDisplaySelectorView
	,	FacetsItemListSortSelectorView
	,	FacetsItemListShowSelectorView
	,	FacetsItemCellView
	,	FacetsEmptyView
	,	GlobalViewsPaginationView
	,	Tracker
	,	RecentlyViewedItemsView

	,	ItemDetailsImageGalleryView
	,	ItemViewsPriceView
	,	ItemDetailsModel

	,	facet_browse_tpl
	,	facets_items_collection_tpl
	,	facets_items_collection_view_cell_tpl
	,	facets_items_collection_view_row_tpl

	,	Backbone
	,	BackboneCollectionView
	,	BackboneCompositeView
	,	EmailBackInStock
	,	_
	,	jQuery
	)
{
	'use strict';


	// statuses stores the statuses of the collapsible facets
	var statuses = window.statuses = {}
		// collapsable_elements stores the statuses of the collapsible elements. This store elements collapsable that are not facets
		/*
		each object should be of the form
		{
			selector: '' //id of the element that will collapsed/expanded
		,	collapsed: true/false
		}
		*/
	,	collapsable_elements = window.collapsable_elements = {};

	//@class Facets.Browse.View View that handles the item list @extends Backbone.View
	return Backbone.View.extend({

		template: facet_browse_tpl
	,	description: _('This is a description').translate()

	,	attributes: {
			'id': 'facet-browse'
		,	'class': 'view facet-browse'
		}

	,	events: {
			'click [data-toggle="facet-navigation"]': 'toggleFacetNavigation'
		,	'change [data-toggle="add-to-cart"] input[name="quantity"]': 'changeQ'
		,	'submit [data-toggle="add-to-cart"]': 'addToCart'
		,	'click [data-action="toggle-filters"]': 'toggleFilters'
		//custom events
		,	'click #itemCompare': 'showItemCompare'
		,	'click .remove-item': 'removeItem'
		,	'click .itemCompareChecked' : 'countChecked'
		,	'click [data-action="trigger-email"]': 'captureEmail'
		,	'click [data-action="send-email"]': 'sendEmail'
		,	'focus .email-input': 'clearMessage'
		,	'click #stock-toggle' : 'addStockFacet'//'setStock'
		,	'click [data-action="item-compare-clear"]': 'clearItemCompare'

		}
		
	,	addStockFacet: function(e){
			if (SC.ENVIRONMENT.stockChecked !== 'checked'){
				SC.ENVIRONMENT.stockChecked = 'checked';
			} else {
				SC.ENVIRONMENT.stockChecked = false;
			}
		}

	,	initialize: function (options)
		{
			BackboneCompositeView.add(this);

			this.statuses = statuses;
			this.collapsable_elements = collapsable_elements;
			this.translator = options.translator;
			this.application = options.application;
			this.category = Categories.getBranchLineFromPath(this.translator.getFacetValue('category'))[0];

			this.cart = LiveOrderModel.getInstance();

			this.collapsable_elements['facet-header'] = this.collapsable_elements['facet-header'] || {
				selector: 'this.collapsable_elements["facet-header"]'
			,	collapsed: false
			};

			var self = this;
			/*if (SC.ENVIRONMENT.stockChecked === 'checked') {
				this.once('beforeViewRender', function(){
					self.toggleStock();
				});
			}*/
			
			var regexInStock = /IsInStock/ig;
			
			if(!(regexInStock.test(window.location.hash) || regexInStock.test(window.location.href))){
				SC.ENVIRONMENT.stockChecked = false;
			} else {
				SC.ENVIRONMENT.stockChecked = 'checked';
			}
		}

		//@method toggleFilters
	,	toggleFilters: function (e)
		{
			e.preventDefault();

			var current_target = jQuery(e.currentTarget);

			this.collapsable_elements['facet-header'].collapsed = !this.collapsable_elements['facet-header'].collapsed;

			current_target.find('.filter-icon').toggleClass('icon-chevron-up');

			current_target.parents('[data-type="accordion"]')
				.toggleClass('well')
				.toggleClass('facet-header-white-well')
				.find('[data-type="accordion-body"]').stop().slideToggle();
		}

		//@method getPath
	,	getPath: function ()
		{
			var canonical = window.location.protocol + '//' + window.location.hostname + '/' + Backbone.history.fragment
			,	index_of_query = canonical.indexOf('?');

			// !~ means: indexOf == -1
			return !~index_of_query ? canonical : canonical.substring(0, index_of_query);
		}

		//@method getCanonical
	,	getCanonical: function ()
		{
			var canonical_url = this.getPath()
			,	current_page = this.translator.getOptionValue('page');

			if (current_page > 1)
			{
				canonical_url += '?page=' + current_page;
			}

			return canonical_url;
		}

		//@method getRelPrev
	,	getRelPrev: function ()
		{
			var previous_page_url = this.getPath()
			,	current_page = this.translator.getOptionValue('page');

			if (current_page > 1)
			{
				if (current_page === 2)
				{
					return previous_page_url;
				}

				if (current_page > 2)
				{
					return previous_page_url += '?page=' + (current_page - 1);
				}
			}

			return null;
		}

		//@method getRelNext
	,	getRelNext: function ()
		{
			var next_page_url = this.getPath()
			,	current_page = this.translator.getOptionValue('page');

			if (current_page < this.totalPages)
			{
				return next_page_url += '?page='+ (current_page + 1);
			}

			return null;
		}

		//@method formatFacetTitle: accepts a facet object and returns a string formatted to be displayed on the document's title according with user facet configuration property titleToken
		//@param {Object} facet @returns {String}
	,	formatFacetTitle: function (facet)
		{
			var defaults = {
				range: '$(2): $(0) to $(1)'
			,	multi: '$(1): $(0)'
			,	single: '$(1): $(0)'
			};

			if (facet.id === 'category')
			{
				//we search for a category title starting from the last category of the branch
				var categories = Categories.getBranchLineFromPath(this.options.translator.getFacetValue('category'));
				if(categories && categories.length > 0)
				{
					for(var i = categories.length - 1; i >= 0; i--)
					{
						var category = categories[i];
						var category_title = category.pagetitle || category.itemid;
						if(category_title)
						{
							return category_title;
						}
					}
				}
				return null;
			}

			if (!facet.config.titleToken)
			{
				facet.config.titleToken = defaults[facet.config.behavior] || defaults.single;
			}
			if (_.isFunction(facet.config.titleToken))
			{
				return facet.config.titleToken(facet);
			}
			else if (facet.config.behavior === 'range')
			{
				return _(facet.config.titleToken).translate(facet.value.to, facet.value.from, facet.config.name);
			}
			else if (facet.config.behavior === 'multi')
			{
				var buffer = [];
				_.each(facet.value, function (val)
				{
					buffer.push(val);
				});
				return _(facet.config.titleToken).translate(buffer.join(', '), facet.config.name);
			}
			else
			{
				var value = this.translator.getLabelForValue(facet.config.id, facet.value);

				return _(facet.config.titleToken).translate(value, facet.config.name);
			}
		}

		// @method getTitle overrides Backbone.Views.getTitle
	,	getTitle: function ()
		{
			if (this.title)
			{
				return this.title;
			}

			var facets = this.options.translator.facets
			,	title = '';

			if (facets && facets.length)
			{
				var buffer = []
				,	facet = null;

				for (var i = 0; i < facets.length; i++)
				{
					facet = facets[i];
					buffer.push(this.formatFacetTitle(facet));

					if (i < facets.length - 1)
					{
						buffer.push(facet.config.titleSeparator || ', ');
					}
				}

				title = this.application.getConfig('searchTitlePrefix', '') +
						buffer.join('') +
						this.application.getConfig('searchTitleSuffix', '');
			}
			else if (this.translator.getOptionValue('keywords'))
			{
				title = _('Search results for "$(0)"').translate(
					this.translator.getOptionValue('keywords')
				);
			}
			else
			{
				title = this.application.getConfig('defaultSearchTitle', '');
			}

			// Update the meta tag 'twitter:title'
			this.setMetaTwitterTitle(title);

			return title;
		}

		// @method setMetaTwitterTitle @param {Strnig}title
	,	setMetaTwitterTitle: function (title)
		{
			var seo_twitter_title = jQuery('meta[name="twitter:title"]');
			seo_twitter_title && seo_twitter_title.attr('content', title);
		}

		// @method showContent overrides Backbone.View.showContent to works with the title to find the proper wording and calls the layout.showContent
	,	showContent: function ()
		{
			// If its a free text search it will work with the title
			var self = this
			,	keywords = this.translator.getOptionValue('keywords')
			,	resultCount = this.model.get('total');

			if (keywords)
			{
				keywords = decodeURIComponent(keywords);

				if (resultCount > 0)
				{
					this.subtitle =  resultCount > 1 ? _('Results for "$(0)"').translate(keywords) : _('Result for "$(0)"').translate(keywords);
				}
				else
				{
					this.subtitle = _('We couldn\'t find any items that match "$(0)"').translate(keywords);
				}
			}

			this.totalPages = Math.ceil(resultCount / this.translator.getOptionValue('show'));

			this.application.getLayout().showContent(this).done(function ()
			{
				Tracker.getInstance().trackProductList(self.model.get('items'));
				self.render();

				if(jQuery.fn.scPush)
				{
					self.$el.find('[data-action="pushable"]').scPush({target: 'tablet'});
					self.$el.find('.sc-pusher-header-back').text('Apply Filters');
				}
			});
			
			var element = this.$el.find('[data-type="carousel-items"]');
			_.initBxSlider(element, Configuration.bxSliderDefaults);
		}

		//@method changeQ
	,	changeQ: function(e)
		{
			e.preventDefault();
			var options = jQuery(e.target).closest('form').serializeObject()
			,	model = this.model.get('items').get(options.item_id);
			// Updates the quantity of the model
			model.setOption('quantity', options.quantity);
			jQuery(e.target).closest('.item-cell').find('[itemprop="price"]').html(model.getPrice().price_formatted);
		}

		// @method addToCart Adds the item to the cart
	,	addToCart: function (e)
		{
			e.preventDefault();
			
			//Hide the compare modal and compare button
			$('#compareModal').modal('hide');
			$('.item-compare').hide();

			var options = jQuery(e.target).serializeObject()
			,	model = this.model.get('items').get(options.item_id);

			// Updates the quantity of the model
			model.setOption('quantity', options.quantity);

			if (model.isReadyForCart())
			{
				var self = this
				,	cart = this.cart
				,	layout = this.application.getLayout()
				,	cart_promise = jQuery.Deferred()
				,	error_message = _('Sorry, there is a problem with this Item and can not be purchased at this time. Please check back later.').translate();

				if (model.cartItemId)
				{
					cart_promise = cart.updateItem(model.cartItemId, model).done(function ()
					{
						if (cart.getLatestAddition())
						{
							if (self.$containerModal)
							{
								self.$containerModal.modal('hide');
							}

							if (layout.currentView instanceof require('Cart').Views.Detailed)
							{
								layout.currentView.showContent();
							}
						}
						else
						{
							self.showError(error_message);
						}
					});
				}
				else
				{
					cart_promise = cart.addItem(model).done(function ()
					{
						if (cart.getLatestAddition())
						{
							layout.showCartConfirmation();
						}
						else
						{
							self.showError(error_message);
						}
					});
				}

				// disalbes the btn while it's being saved then enables it back again
				if (e && e.currentTarget)
				{
					jQuery('input[type="submit"]', e.currentTarget).attr('disabled', true);
					cart_promise.always(function () {
						jQuery('input[type="submit"]', e.currentTarget).attr('disabled', false);
					});
				}
			}
            
			//clear all checks
            $('.itemCompareChecked').attr('checked', false);

		}
		// @method getBreadcrumbPages
		// It will generate an array suitable to pass it to the breadcrumb macro
		// It looks in the category facet value
		// @return {Array<Object>}
	,	getBreadcrumbPages: function ()
		{
			/*category_string = this.translator.getFacetValue('category')*/
			
			//Generates breadcrumbs based on the current MCategories and Brand facet values
			var	self = this
			,	breadcrumb = []
			,	MCategory = _.find(this.translator.facets, function(facet){return facet.url == 'MCategories'})
			,	MCategories = MCategory ? _.find(this.translator.facetsLabels, function(facet){return facet.url == MCategory.url}) : null
			,	MCategoryConfig = MCategory ? _.find(MCategories.values, function(value){return value.url == MCategory.value[0]}) : null
			,	brand = _.find(this.translator.facets, function(facet){return facet.url == 'Brand'})
			,	brandCategories = brand ? _.find(this.translator.facetsLabels, function(facet){return facet.url == brand.url}) : null
			,	brandConfig = [];
			
			//Adds to the array containing all of the brand facet values
			if (brand && brandCategories) {
				_.each(brand.value, function(val){
					brandConfig.push(_.find(brandCategories.values, function(value){return value.url == val;}));
				});
			}

			//Adds the selected MCategory to the Breadcrumb. Doesn't support multiple values
			if (MCategoryConfig && MCategories) {
				breadcrumb.push({
						href: '/' + MCategories.url + Configuration.facetDelimiters.betweenFacetNameAndValue + MCategoryConfig.url
					,	text: _(MCategoryConfig.label).translate()
				});
			}
			//Adds the brands to the breadcrumbs. Supports multiple selected brands. This should already be sorted by alpha.
			if (brandConfig.length > 0 && brandCategories) {
				_.each(brandConfig, function(brand){
					if (brand) {
						breadcrumb.push({
								href: '/' + brandCategories.url + Configuration.facetDelimiters.betweenFacetNameAndValue + brand.url
							,	text: _(brand.label).translate()
						});
					};
				});
			}
			
			if (!MCategoryConfig && brandConfig.length <= 0)
			{
				if (self.translator.getOptionValue('keywords'))
				{
					breadcrumb.push({
						href: '#'
					,	text: _('Search Results').translate()
					});
				}
				else
				{
					breadcrumb.push({
						href: '#'
					,	text: _('Shop').translate()
					});
				}
			}

			/*if (category_string)
			{
				var category_path = '';
				_.each(Categories.getBranchLineFromPath(category_string), function (cat)
				{
					category_path += '/'+cat.urlcomponent;

					breadcrumb.push({
						href: category_path
					,	text: _(cat.itemid).translate()
					});
				});

			}
			else if (this.translator.getOptionValue('keywords'))
			{
				breadcrumb.push({
					href: '#'
				,	text: _('Search Results').translate()
				});
			}
			else
			{
				breadcrumb.push({
					href: '#'
				,	text: _('Shop').translate()
				});
			}*/

			return breadcrumb;
		}

		// @method toggleFacetNavigation Hides/Shows the facet navigation area
	,	toggleFacetNavigation: function ()
		{
			this.$el.toggleClass('narrow-by');
			this.toggleNavigationListener(this.$el.hasClass('narrow-by'));
		}

		// @method toggleNavigationListener
		// adds/removes event listeners to the HTML to hide the facet navigation area
		// @param {Boolean} isOn
	,	toggleNavigationListener: function (isOn)
		{
			var self = this
			,	touch_started = null;

			// turn listeners on
			if (isOn)
			{
				jQuery('html')
					// we save the time when the touchstart happened
					.on('touchstart.narrow-by', function ()
					{
						touch_started = new Date().getTime();
					})
					// code for touchend and mousdown is the same
					.on('touchend.narrow-by mousedown.narrow-by', function (e)
					{
						// if there wasn't a touch event, or the time difference between
						// touch start and touch end is less that 200 miliseconds
						// (this is to allow scrolling without closing the facet navigation area)
						if (!touch_started || new Date().getTime() - touch_started < 200)
						{
							var $target = jQuery(e.target);

							// if we are not touching the narrow by button or the facet navigation area
							if (!$target.closest('[data-toggle="facet-navigation"]').length && !$target.closest('#faceted-navigation').length)
							{
								// we hide the navigation
								self.toggleFacetNavigation();
							}
						}
					});
			}
			else
			{
				jQuery('html')
					// if the navigation area is hidden, we remove the event listeners from the HTML
					.off('mousedown.narrow-by touchstart.narrow-by touchend.narrow-by');
			}
		}

	,	render: function ()
		{
			this._render();
		}

	,	childViews: {
			
			'Facets.FacetedNavigation': function (options)
			{
				var exclude = _.map((options.excludeFacets || '').split(','), function (facet_id_to_exclude)
					{
						return jQuery.trim( facet_id_to_exclude );
					})
				,	has_categories = !!(this.category && this.category.categories)
				,	has_items = this.model.get('items').length
				,	has_facets = has_items && this.model.get('facets').length
				,	applied_facets = this.translator.cloneWithoutFacetId('category').facets
				,	has_applied_facets = applied_facets.length;

				return new FacetsFacetedNavigationView({
					categoryItemId: this.category && this.category.itemid
				,	clearAllFacetsLink: this.translator.cloneWithoutFacets().getUrl()
				,	hasCategories: has_categories
				,	hasItems: has_items

					// facets box is removed if don't find items
				,	hasFacets: has_facets

				,	hasCategoriesAndFacets: has_categories && has_facets

					// Categories are not a real facet, so lets remove those
				,	appliedFacets: applied_facets

				,	hasFacetsOrAppliedFacets: has_facets || has_applied_facets

				,	translatorUrl: this.translator.getUrl()

				,	translatorConfig: this.options.translatorConfig
				,	facets: _.filter(this.model.get('facets'), function (facet)
					{
						return !_.contains(exclude, facet.id);
					})

				,	totalProducts: this.model.get('total')
				,	keywords: this.translator.getOptionValue('keywords')
				});
			}

		,	'Facets.FacetsDisplay': function()
			{
				var facets = this.translator.cloneWithoutFacetId('category').getAllFacets().sort(function (a, b) {
					return b.config.priority - a.config.priority;
				});

				return new FacetsFacetsDisplayView({
					facets: facets
				,	translator: this.translator
				});
			}

		,	'Facets.ItemListDisplaySelector': function()
			{
				return new FacetsItemListDisplaySelectorView({
					configClasses: 'pull-right'
				,	options: this.options.application.getConfig('itemsDisplayOptions')
				,	translator: this.translator
				});
			}

		,	'Facets.ItemListSortSelector': function()
			{
				return new FacetsItemListSortSelectorView({
					options: this.options.application.getConfig('sortOptions')
				,	translator: this.translator
				});
			}

		,	'Facets.ItemListShowSelector': function()
			{
				return new FacetsItemListShowSelectorView({
					options: this.options.application.getConfig('resultsPerPage')
				,	translator: this.translator
				});
			}

		,	'Facets.FacetedNavigation.Item': function (options)
			{
				var facet_config = this.translator.getFacetConfig(options.facetId)
				,	contructor_options = {
						model: new Backbone.Model(_.findWhere(this.model.get('facets'), {id: options.facetId}))
					,	translator: this.translator
					};

				if (facet_config.template)
				{
					contructor_options.template = facet_config.template;
				}

				return new FacetsFacetedNavigationItemView(contructor_options);
			}

		,	'Facets.Items': function()
			{
				var self = this
				,	display_option = _.find(Configuration.itemsDisplayOptions, function (option)
					{
						return option.id === self.options.translator.getOptionValue('display');
					});

				return new BackboneCollectionView({
					childTemplate: display_option.template
				,	childView: FacetsItemCellView
				,	viewsPerRow: parseInt(display_option.columns, 10)
				,	collection: this.model.get('items')
				,	cellTemplate: facets_items_collection_view_cell_tpl
				,	rowTemplate: facets_items_collection_view_row_tpl
				,	template: facets_items_collection_tpl
				,	context: {
						keywords: this.translator.getOptionValue('keywords')
					}
				});
			}

		,	'Facets.Items.Empty': function()
			{
				return new FacetsEmptyView({
					keywords: this.translator.getOptionValue('keywords')
				});
			}

		,	'GlobalViews.Pagination': function()
			{
				var translator = this.translator;

				return new GlobalViewsPaginationView({
					currentPage: translator.getOptionValue('page')
				,	totalPages: this.totalPages
				,	pager: function (page) {
						return translator.cloneForOption('page', page).getUrl();
					}
				});
			}
		,	'RecentlyViewed.Items' : function ()
			{
				return new RecentlyViewedItemsView({ application: this.application });
			}
		}

		//@method getContext @returns {Facets.Browse.View.Context}
	,	getContext: function()
		{
			//The category name is used within the H1 tag for facet pages that should be indexed by crawlers, Brand takes precedence over MCategory when combined
			var	self = this
			,	category = _.find(this.translator.facets, function(facet){return facet.url == 'MCategories' || facet.url == 'Brand'})
			,	MCategories = {}
			,	categoryConfig = {}
			,	current_facets = ''
			,	current_parameters = '?order=' + this.translator.options.order + '&show=' + this.translator.options.show + '&display=' + this.translator.options.display + (this.translator.options.keywords ? '&keywords=' + this.translator.options.keywords : '')
			,	pricelevel = /pricelevel/i;
			
			//Generates part of the url for the Hide Out of Stock feature
			_.each(this.translator.facets, function(facet){
				//Check that the current facet is not IsInStock or a pricelevel facet
				//Then loop through the facet values and append them to the necessary part of the url
				//Note that variable current_facet_url is used for the facet url as well as the facet value urls
				if(facet.url !== 'IsInStock' && !(pricelevel.test(facet.url))){
					var current_facet_url = facet.url;
					current_facets += '/' + current_facet_url +  '+';
					_.each(facet.value, function(value, index){
						current_facet_url = value + ((index !== facet.value.length - 1) ? ',' : '');
						current_facets += current_facet_url
					});
				//Check if the current facet is a pricelevel facet then perform this if so
				} else if (pricelevel.test(facet.url)){
					current_facets += '/' + facet.url + '+' + facet.value.from + 'to' + facet.value.to;
				}
			});
			
			//This is for search result pages. Sets current_facets to 'search' if there are keywords,
			//Hide Out of Stock is checked,
			//and there are no currently applied facets. 
			if (!!(this.translator.options.keywords) && SC.ENVIRONMENT.stockChecked === 'checked' && !current_facets) {
				current_facets = 'search';
			}
			
			//Checks if this is a MCategories page or Brand page and assigns MCategories variable as appropriate
			if (category ? category.url == 'MCategories' : false){
				MCategories = _.find(this.translator.facetsLabels, function(facet){return facet.url == 'MCategories'})
			} else if (category ? category.url == 'Brand' : false){
				MCategories = _.find(this.translator.facetsLabels, function(facet){return facet.url == 'Brand'})
			}
			
			//Creates the the object which contains the display title
			categoryConfig = _.find(MCategories.values, function(value){return value.url == (category ? category.value : null)})

            /*if (SC.ENVIRONMENT.checked) {
                this.toggleLoad();
            }*/
			
			// @class Facets.Browse.View.Context
			return {
				// @property {Number} total
				total: this.model.get('total')

				// @property {Boolean} isTotalProductsOne
			,	isTotalProductsOne: this.model.get('total') === 1

				// @property {String} title
			,	title: categoryConfig ? categoryConfig.label : '' /*this.getTitle() this produced the URL value. We would rather display the display value or label*/

				// @property {Boolean} hasItemsAndFacets
			,	hasItemsAndFacets: !!(this.model.get('items').length && this.model.get('facets').length)

				// @property {Boolean} collapseHeader
			,	collapseHeader: !!(this.collapsable_elements['facet-header'].collapsed)

				// @property {String} keywords
			,	keywords: this.translator.getOptionValue('keywords')

				// @property {Boolean} showResults
			,	showResults: _.isNull(this.translator.getOptionValue('keywords')) ? true : (this.model.get('total') > 0)

				// @property {Boolean} isEmptyList
			,	isEmptyList:  (this.model.get('total') <= 0)

			,	itemCompare: this.itemCompare

			,	checked: SC.ENVIRONMENT.stockChecked ? SC.ENVIRONMENT.stockChecked : false
				// The display title
			,	category: categoryConfig ? categoryConfig.label : ''
			
			,	currentFacets: current_facets
			
			,	currentParameters: current_parameters
			
			,	inStockFacet: SC.ENVIRONMENT.stockChecked === 'checked' ? '' : '/IsInStock+false'
			};
			// @class Facets.Browse.View
		}
		
	,	instock: false
	,	outofstock: false
	,	originalModel: false

	/*,	setStock: function(e)
		{
			//Set the global variable to check e.currentTarget.checked;
			SC.ENVIRONMENT.stockChecked = $('#stock-toggle').prop('checked') ? 'checked' : false;
			if (!SC.ENVIRONMENT.stockChecked){
				SC.ENVIRONMENT.totalSearched = 0;
			}
			if (SC.ENVIRONMENT.stockChecked === 'checked'){
				//Save the original item models to the view
				this.originalModel = this.model.attributes.items.models;
				SC.ENVIRONMENT.lastInStockPosition = ((this.translator.options.page - 1) * parseInt(this.translator.options.show));
				this.toggleStock();
			} else {
				//Resets the item models and re-renders view
                this.model.attributes.items.models = this.originalModel;
				this.model.attributes.items.length = this.originalModel.length;
				SC.ENVIRONMENT.stockChecked = false;
				SC.ENVIRONMENT.lastInStockPosition = 0;
				SC.ENVIRONMENT.lastOutStockPosition = 0;
				if(!this.originalModel){
					location.reload();
				} else {
					this.render();
				}
			}
		}*/
	
	/*,	toggleStock: function(e, offsetP)
		{
			var self = this
			,	inStockArray = []
			,	outStockArray = []
			,	currentIteration = 0
			,	lastInStockPosition = SC.ENVIRONMENT.lastInStockPosition || 0
			,	lastOutStockPosition = SC.ENVIRONMENT.lastOutStockPosition || 0
			,	inStockUrl = {
					pageNumb: self.translator.options.page
				,	showNumb: parseInt(self.translator.options.show)
				,	order: self.translator.options.order
				,	offset: 0
				,	facets: ''
				};
			
			//Generate the facets for the search from the current selected facets
			_.each(this.translator.facets, function(facet){
				var facetsUrl = '&'
					+ facet.url
					+ '='
					+ _.each(facet.value, function(value, index){return value + ((index !== facet.value.length - 1) ? ',' : '')});
				inStockUrl.facets += facetsUrl;
			});
			
			if(inStockUrl.pageNumb > 1){
				inStockUrl.offset = lastInStockPosition;
			}

			//Function to get the in stock items
			function getInStockItems(){
				console.log('getInStockItems');
				var allItems = new Backbone.Collection;
				allItems.url = 'http://www.primaryarms.com/api/items?c=3901023&country=US&currency=USD&fieldset=search&language=en&n=2&pricelevel=1'
					+ inStockUrl.facets
					+ '&limit='
					+ inStockUrl.showNumb
					+ '&offset='
					+ inStockUrl.offset
					+ '&sort='
					+ inStockUrl.order;
				
				allItems.fetch({
					success: function(model, response, options){
						var lastInStock = 0
						,	items = _.each(response.items, function(item, index){item.index = index})
						,	inStockItems = _.filter(items, function(item){
								return item.isinstock === true;
							});
						
						//console.log(response.items);
						//console.log(inStockItems);
						//Push each in stock item into array
						for(var i = 0; i < inStockItems.length; i++){
							//Check if inStockArray is equal to the Show products #
							if(inStockArray.length === inStockUrl.showNumb){
								break;
							}
							var item = inStockItems[i]
							,	model = new ItemDetailsModel(inStockItems[i]);
							inStockArray.push(model);
							
							lastInStock = parseInt(item.index);
						}
						//console.log(inStockArray);
						
						//Save the index for the last in stock item, used for generating offset for pagination
						lastInStockPosition = lastInStock + inStockUrl.offset + 1;
						//console.log('lastInStockPosition ' + lastInStockPosition);
						
						//increment the total # of items searched
						//SC.ENVIRONMENT.totalSearched = (Math.max(lastInStockPosition, lastOutStockPosition) + (SC.ENVIRONMENT.totalSearched ? SC.ENVIRONMENT.totalSearched : 0));
						SC.ENVIRONMENT.totalSearched = inStockUrl.offset + inStockUrl.showNumb
						
						console.log(inStockArray.length);console.log(inStockUrl.showNumb);console.log(SC.ENVIRONMENT.totalSearched);console.log(response.total);
						//Check if inStockArray is less than the Show products # or if the total items searched is greater than the total items available, recurse if less
						if (inStockArray.length < inStockUrl.showNumb && SC.ENVIRONMENT.totalSearched < response.total){
							inStockUrl.offset += inStockUrl.showNumb;
							getInStockItems();
						//Check if in stock array length is less than the amount to show and push in out of stock items if necessary
						} else if (inStockArray.length < inStockUrl.showNumb){
							inStockUrl.offset = 0;
							getOutStockItems();
						} else {
							//If in stock array length is equal to the amount to show then set the models and re-render
							self.model.attributes.items.models = inStockArray;
							self.model.attributes.items.length = self.model.attributes.items.models.length;
							SC.ENVIRONMENT.stockChecked = 'checked';
							SC.ENVIRONMENT.lastInStockPosition = lastInStockPosition;
							SC.ENVIRONMENT.lastOutStockPosition = lastOutStockPosition;
							SC.ENVIRONMENT.previousPage = self.translator.options.page;
							self.render();
						}
					}
				});
			}
			
			//Function to get the out of stock items
			function getOutStockItems(){
				console.log('getOutStockItems');
				var allItems = new Backbone.Collection;
				allItems.url = 'http://www.primaryarms.com/api/items?c=3901023&country=US&currency=USD&fieldset=search&language=en&n=2&pricelevel=1'
					+ inStockUrl.facets
					+ '&limit='
					+ inStockUrl.showNumb
					+ '&offset='
					+ inStockUrl.offset
					+ '&sort='
					+ inStockUrl.order;
				
				allItems.fetch({
					success: function(model, response, options){
						var lastOutStock = 0
						,	items = _.each(response.items, function(item, index){item.index = index})
						,	outStockItems = _.filter(items, function(item){
								return item.isinstock === false;
							});
							
						//Push each out stock item into array
						for(var i = 0; i < outStockItems.length; i++){
							var item = outStockItems[i]
							,	model = new ItemDetailsModel(outStockItems[i]);
							outStockArray.push(model);
							
							lastOutStock = parseInt(item.index);
						}
						
						//Add to the inStockArray
						for(var i = 0; inStockArray.length < inStockUrl.showNumb && outStockArray[i]; i++){
							inStockArray.push(outStockArray[i]);
						}
						
						//Save the index for the last in stock item, used for generating offset for pagination
						lastOutStockPosition = lastOutStock + inStockUrl.offset + 1;
						
						//increment the total # of items searched
						//SC.ENVIRONMENT.totalSearched = (Math.max(lastInStockPosition, lastOutStockPosition) + (SC.ENVIRONMENT.totalSearched ? SC.ENVIRONMENT.totalSearched : 0));
						SC.ENVIRONMENT.totalSearched = inStockUrl.offset + inStockUrl.showNumb
						
						//console.log(inStockArray.length);console.log(inStockUrl.showNumb);console.log(SC.ENVIRONMENT.totalSearched);console.log(response.total);
						//Check if inStockArray is less than the Show products # or if the total items searched is greater than the total items available, recurse if less
						if (inStockArray.length < inStockUrl.showNumb && SC.ENVIRONMENT.totalSearched < response.total){
							inStockUrl.offset += inStockUrl.showNumb;
							getOutStockItems();
						//Check if in stock array length is less than the amount to show and push in out of stock items if necessary
						} else {
							//If in stock array length is equal to the amount to show then set the models and re-render
							self.model.attributes.items.models = inStockArray;
							self.model.attributes.items.length = self.model.attributes.items.models.length;
							SC.ENVIRONMENT.stockChecked = 'checked';
							SC.ENVIRONMENT.lastInStockPosition = lastInStockPosition;
							SC.ENVIRONMENT.lastOutStockPosition = lastOutStockPosition;
							SC.ENVIRONMENT.previousPage = self.translator.options.page;
							self.render();
						}
					}
				});
			}
			
			if (SC.ENVIRONMENT.lastOutStockPosition && SC.ENVIRONMENT.lastOutStockPosition !== 0){
				inStockUrl.offset = SC.ENVIRONMENT.lastOutStockPosition;
				if(inStockUrl.offset >= self.model.get('total') || self.translator.options.page < SC.ENVIRONMENT.previousPage){
					console.log('greater');
					inStockUrl.offset = ((self.translator.options.page - 1) * parseInt(self.translator.options.show));
					getInStockItems();
				} else {
					getOutStockItems()
				}
			} else {
				getInStockItems();
			}
		}*/

	/*,	toggleLoad: function()
		{
            var items = this.model.get('items');
            var instock = new Array();
            var outofstock = new Array();
            for (var i = 0; i < items.models.length; i++) {
                if (items.models[i].get('_isInStock')) {
                    instock.push(items.models[i]);
                } else {
                    outofstock.push(items.models[i]);
                }
            }
            if (!this.instock) {
                this.instock = instock;
            }
            if (!this.outofstock) {
                this.outofstock = outofstock;
            }
            if (!this.originalModel) {
                this.originalModel = this.model.attributes.items.models;
            }
            

            if (SC.ENVIRONMENT.checked == 'unchecked') {
                this.model.attributes.items.models = this.instock;
            } else {
                this.model.attributes.items.models = this.originalModel;
            }
            this.model.attributes.items.length = this.model.attributes.items.models.length;
		}*/

	,	itemCompare: ''

	,	itemCompareObj: []

	,	countChecked: function(e)
		{
			var ctr = 0;
			$('input[name="itemCompareChecked"]:checked').each(function() {
				ctr++
            });
            if(ctr > 1) {
            	$(".itemCompareCont").css("display","block");
            } else{
            	$(".itemCompareCont").css("display","none");
            }
		}

	,	showItemCompare: function(e)
        {
            var itemIdArray = [],
                itemArray = [],
                itemClasses = [];
            $('input[name="itemCompareChecked"]:checked').each(function() {
                itemIdArray.push(this.value);
                itemClasses.push(this.id);
            });
            if (itemIdArray.length > 1) {
                var items = this.model.get('items');
                items.each(function(item) {
                    var itemid = item.get('internalid');
                    for (var i = 0; i < itemIdArray.length; i++) {
                        if (itemIdArray[i] == itemid) {
                            itemArray.push(item);
                        }
                    }
                });
                this.itemCompareObj = itemArray;
                var html = this.tableHtml(itemArray);
                this.itemCompare = html;
                this.render();             
                $('#compareModal').modal();
                $('.modal-dialog').css("width", "100%");
                for (var i = 0; i < itemClasses.length; i++) {
                    $('#' + itemClasses[i]).attr('checked', true);
                }
                if(itemClasses.length > 1){
            		$(".itemCompareCont").css("display","block");
                }
            }
        }
		
	,	clearItemCompare: function()
		{
			$('input[name="itemCompareChecked"]:checked').attr('checked', false);
			this.countChecked();
		}

    ,	removeItem: function(e)
    	{
            var itemClasses = [];
            var removeItemId = (e.target).value;
            var itemArray = this.itemCompareObj;
            var updatedItemArray = new Array();
            for (var i = 0; i < itemArray.length; i++) {
                if (itemArray[i].get('internalid') != removeItemId) {
                    updatedItemArray.push(itemArray[i]);
                    itemClasses.push('checked' + itemArray[i].get('internalid'));
                }
            }
            if (updatedItemArray.length > 0) {
                this.itemCompareObj = updatedItemArray;
                var html = this.tableHtml(updatedItemArray);
                this.itemCompare = html;
                this.render();
                $('#compareModal').modal();
                $('.modal-dialog').css("width", "100%");
                for (var i = 0; i < itemClasses.length; i++) {
                    $('#' + itemClasses[i]).attr('checked', true);
                }
                if(itemClasses.length >1){
            		$(".itemCompareCont").css("display","block");
                }
            } else {
                this.itemCompareObj = [];
                $('#compareModal').modal('hide');
                $(".itemCompareChecked").attr('checked', false);
            }
    	}

    ,	tableHtml: function(itemArray)
    	{
    		var desktop = this.desktop(itemArray);
    		var mobile = this.mobile(itemArray);
    		var html = ''
			+	'<div class="desktop-table hidden-sm hidden-xs">'+ desktop+ '</div>'
			+	'<div class="mobile-table" >'+ mobile + '</div>'
			
			//Not sure what this does
			// +	'</div>';
    		
    		return html;
    	}

    ,	mobile: function(itemArray)
    	{
    		var html = '<table class="item-compare-table">';
            for (var i = 0; i < itemArray.length; i++) {
                var imageUrl = itemArray[i].get('_thumbnail').url,
                    itemName = itemArray[i].get('itemid'),
                    storedescription = itemArray[i].get('storedescription'),
                    price = itemArray[i].get('onlinecustomerprice_formatted'),
                    internalid = itemArray[i].get('internalid'),
                    inStock = itemArray[i].get('isinstock');
				var ipName = 'ipView-' + [i];
				this.childViews[ipName] = function (e)
				{
					var str = e.view;
					var splt = str.split('-');
					var num = splt[1];
					var model = itemArray[num];
					return new ItemViewsPriceView({
						model: model
					,	origin: 'PDPQUICK'
					});
				}
				var igName = 'igView-' + [i];
				this.childViews[igName] = function (e)
				{
					var str = e.view;
					var splt = str.split('-');
					var num = splt[1];
					var model = itemArray[num];
					return new ItemDetailsImageGalleryView({images: model.get('_images', true)});
				}
                var imgsize = itemArray.length > 3 ? '' : 'style="width:50%;"';
                var mod = i%2;
                if(mod==0){
	                var tr1 = '<tr>',
	                tr2 = '<tr>',
	                tr3 = '<tr>',
	                tr4 = '<tr>',
	                tr5 = '<tr>',
	                tr6 = '<tr>',
	                tr7 = '<tr>';
                }

                tr1 += '<td style="width:' + (100 / itemArray.length) + '%; border-bottom: none;" align="center"><div class="'+igName+'"data-view="'+ igName +'" style="max-width: 300px;"></div></td>';
				//'
                tr2 += '<td align="center"><button type="button" class="item-compare-remove-item" aria-label="Remove" value="' + internalid + '">&times; Remove</button></td>';
                tr3 += '<td>' + itemName + '</td>';
                tr4 += '<td>' + storedescription + '</td>';
                tr5 += '<td>' + '<div class="'+ipName+'"data-view="'+ ipName +'"></div>' + '</td>';
                if (inStock) {
                    tr6 += '<td><p class="item-views-stock-msg-in"><i class="fa fa-check-circle"></i> In Stock </p></td>';
                    tr7	+= '<td><form class="facets-item-cell-grid-add-to-cart" data-toggle="add-to-cart">' 
                    	+ '<input class="facets-item-cell-grid-add-to-cart-itemid" type="hidden" value="' + internalid + '" name="item_id"/>'
                    	+ '<div class="row">'
                    	+ '<div class="col-sm-3 col-xs-3"><input name="quantity" class="facets-item-cell-grid-add-to-cart-quantity" type="number" min="1" value="1" style="min-width:60px !important; width:60px !important; display:inline !important; margin-right: 10px !important"/></div>' 
                    	+ '<div class="col-sm-9 col-xs-9"><input type="submit" class="facets-item-cell-grid-add-to-cart-button" value="Add to Cart" id="add-cart-button"/></div>' 
                    	+ '</div>'
                    	+ '</form>' 
                    	+ '</td>';
                } else {
                    tr6 += '<td><p class="item-views-stock-msg-out"><i class="fa fa-times-circle"></i><span class="item-views-stock-msg-out-text"> Out of Stock</span></p></td>';
                    tr7	+= '<td><button data-type="" data-action="trigger-email" class="item-details-back-in-stock-button" id="email-stock-'+internalid+'-m'+'" value="' + internalid +'-m'+ '">Email Me When In Stock</button>'
                    	+ '<div id="message-'+internalid+'-m'+'" class="email-message" style="display:none"></div>'
                    	+ '<div id="email-form-'+internalid+'-m'+'" class="form-group" style="display:none">'
                    	+ '<label for="email-input'+internalid+'-m'+'">Enter your email address:</label>'
                    	+ '<input type="email"class="email-input" id="email-input'+internalid+'-m'+'" name="email">'
						+ '<p><label><input class="bis-mailing-list-check'+internalid+'" id="bis-mailing-list-check" type="checkbox">Click here to receive special offers, and to hear about new products and brands.</label><p>'
                    	+ '<button data-type="" data-action="send-email" class="item-details-send-button" id="email-send-'+internalid+'-m'+'" value="' + internalid+'-m'+ '">Send</button>'
						+ '<img class="email-sign-up-direct-add'+internalid+'" src="" width="0" height="0" border="0" alt="" style="display: none"/>'
                    	+ '</div>'
                    	+ '</td>';
                }
              	if(mod==1){
	            	tr1 += '</tr>';
		            tr2 += '</tr>';
		            tr3 += '</tr>';
		            tr4 += '</tr>';
		            tr5 += '</tr>';
		            tr6 += '</tr>';
		            tr7 += '</tr>';
		            html += tr1 + tr2 + tr3 + tr4 + tr5 + tr6 + tr7;
            	}
            	if(i == itemArray.length-1 && mod == 0){
            		tr1 += '<td style="border-bottom:none;"></td></tr>';
		            tr2 += '<td></td></tr>';
		            tr3 += '<td></td></tr>';
		            tr4 += '<td></td></tr>';
		            tr5 += '<td></td></tr>';
		            tr6 += '<td></td></tr>';
		            tr7 += '<td></td></tr>';
		            html += tr1 + tr2 + tr3 + tr4 + tr5 + tr6 + tr7;
            	}
            } 
          
            
            html += '</table>';
            return html;
    	}
		
    ,	captureEmail: function(e)
    	{
    		var itemid = (e.target).value;
    		$('#email-form-'+itemid).css("display","block");
    		$('#email-stock-'+itemid).css("display","none");
    	}

	,	desktop: function(itemArray)
    	{	
            var html = '<table class="item-compare-table">',
                tr1 = '<tr>',
                tr2 = '<tr>',
                tr3 = '<tr>',
                tr4 = '<tr>',
                tr5 = '<tr>';
            for (var i = 0; i < itemArray.length; i++) {
                var imageUrl = itemArray[i].get('_thumbnail').url,
                    itemName = itemArray[i].get('itemid'),
                    storedescription = itemArray[i].get('storedescription'),
                    price = itemArray[i].get('onlinecustomerprice_formatted'),
                    internalid = itemArray[i].get('internalid'),
                    inStock = itemArray[i].get('isinstock');
				var ipName = 'ipView-' + [i];
				this.childViews[ipName] = function (e)
				{
					var str = e.view;
					var splt = str.split('-');
					var num = splt[1];
					var model = itemArray[num];
					return new ItemViewsPriceView({
						model: model
					,	origin: 'PDPQUICK'
					});
				}
				var igName = 'igView-' + [i];
				this.childViews[igName] = function (e)
				{
					var str = e.view;
					var splt = str.split('-');
					var num = splt[1];
					var model = itemArray[num];
					return new ItemDetailsImageGalleryView({images: model.get('_images', true)});
				}
                var imgsize = itemArray.length > 3 ? '' : 'style="width:50%;"';

                tr1 += '<td style="width:' + (100 / itemArray.length) + '% " align="center"><div class="'+igName+'"data-view="'+ igName +'" style="max-width: 300px;"></div><br><br>' 
                	//'
					+ '<button type="button" class="item-compare-remove-item" aria-label="Remove" value="' + internalid + '">&times; Remove</button></td>';
                tr2 += '<td>' + itemName + '</td>';
                tr3 += '<td>' + storedescription + '</td>';
                tr4 += '<td>' + '<div class="'+ipName+'"data-view="'+ ipName +'"></div>' + '</td>';
                if (inStock) {
                    tr5 += '<td class="lastTR">' 
                    	+ '<p class="item-views-stock-msg-in"><i class="fa fa-check-circle"></i> In Stock </p>'
                    	+ '<form class="facets-item-cell-grid-add-to-cart" data-toggle="add-to-cart">' 
                    	+ '<input class="facets-item-cell-grid-add-to-cart-itemid" type="hidden" value="' + internalid + '" name="item_id"/>' 
                    	+ '<input name="quantity" class="facets-item-cell-grid-add-to-cart-quantity" type="number" min="1" value="1" style="min-width:60px !important; width:60px !important; display:inline !important; margin-right: 10px !important"/>' 
                    	+ '<input type="submit" class="facets-item-cell-grid-add-to-cart-button" value="Add to Cart" id="add-cart-button"/>' 
                    	+ '</form>' 
                    	+ '</td>';
                } else {
                    tr5 += '<td class="lastTR">'
                    	+ '<p class="item-views-stock-msg-out"><i class="fa fa-times-circle"></i><span class="item-views-stock-msg-out-text"> Out of Stock</span></p>'
                    	+ '<p><button data-type="" data-action="trigger-email" class="item-details-back-in-stock-button" id="email-stock-'+internalid+'" value="' + internalid + '">Email Me When In Stock</button></p>'
                    	+ '<div id="message-'+internalid+'" class="email-message" style="display:none"></div>'
                    	+ '<div id="email-form-'+internalid+'" class="form-group" style="display:none">'
                    	+ '<label for="email-input'+internalid+'">Enter your email address:</label>'
                    	+ '<input type="email"class="email-input" id="email-input'+internalid+'" name="email">'
						+ '<p><label><input class="bis-mailing-list-check'+internalid+'" id="bis-mailing-list-check" type="checkbox">Click here to receive special offers, and to hear about new products and brands.</label><p>'
                    	+ '<button data-type="" data-action="send-email" class="item-details-send-button" id="email-send-'+internalid+'" value="' + internalid + '">Send</button>'
                    	+ '<img class="email-sign-up-direct-add'+internalid+'" src="" width="0" height="0" border="0" alt="" style="display: none"/>'
						+ '</div>'
                    	+ '</td>';
                }
            } 
            tr1 += '</tr>';
            tr2 += '</tr>';
            tr3 += '</tr>';
            tr4 += '</tr>';
            tr5 += '</tr>';
            html += tr1 + tr2 + tr3 + tr4 + tr5;
            html += '</table>';
            return html;
    	}

    ,	sendEmail: function(e)
    	{
    		var itemid = (e.target).value
    		,	item = itemid
    		,	splt = itemid.indexOf('-m')
			,	self = this;
			
    		if(splt != -1){
    			item = item.substring(0,splt);
    		}
    		var email = $('#email-input'+itemid).val();
    		var emailBackInStock = new EmailBackInStock();
			emailBackInStock.save({email:email, itemid:item}, {
    			error: function(resp) {
    			$('#message-'+itemid).css("display","block");
    			$('#message-'+itemid).css("color","red");
    			$('#message-'+itemid).text("Please enter a valid email");
   				}
			}).done(function (resp){
    			$('#email-form-'+itemid).css("display","none");
    			$('#message-'+itemid).css("display","block");
    			$('#message-'+itemid).css("color","black");
    			$('#message-'+itemid).text("Thank you!");
				
				//Callback function to submit the Bronto form only if checkbox is checked
				if ($('.bis-mailing-list-check:checked').val() == 'on')
				{
					self.sendBrontoComplete(email, itemid);
				}
        	});
    	}
	
	,	sendBrontoComplete: function(email, itemid){
			//Generates the direct add url
			var url = 'http://info.primaryarms.com/public/?q=direct_add&fn=Public_DirectAddForm&id=bqsvzgbdbvzyrsnzxousbwwqanlbbfd&email='+email+'&list1=0bc603ec0000000000000000000000052b6b&list2=0bd203ec00000000000000000000000c25f1';
			
			//Modify the src url and trigger click event on the direct add image
			$('.email-sign-up-direct-add'+itemid).attr('src', url);
			$('.email-sign-up-direct-add'+itemid).trigger('click');
		}

	,	clearMessage: function()
    	{
    		$('.email-message').css("display","none");
    	}
	});
});
