/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module ItemRelations
define('AddonOptionsItems.View'
,   [	'Backbone.CollectionView'
	,	'ItemViews.AddonOptions.View'
	,	'AddonOptionsItems.Collection'
	,	'SC.Configuration'
	
	,	'addon_options_items.tpl'
	
	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Utils'
	]
,   function (
		BackboneCollectionView
	,	ItemViewsAddonOptionsView
	,	AddonOptionsItemsCollection
	,	Configuration
	
	,	addon_options_items_tpl	
	,	Backbone
	,	jQuery
	,	_
   )
{
	
   'use strict';

   return BackboneCollectionView.extend({
	
		
		events: {
			'click [type="checkbox"]': 'setAddonOption'
		}
		
	,	initialize: function (options) {
			var application = this.options.application
			,	is_sca_advance = application.getConfig('siteSettings.sitetype') === 'ADVANCED'
			,	collection = is_sca_advance ? new AddonOptionsItemsCollection({AddonOptionsItems: this.options.AddonOptionsItems, itemsIds: this.options.itemsIds, AddonOptionsPrices: this.options.AddonOptionsPrices}) : new Backbone.Collection()
			,	self = this;
			
			BackboneCollectionView.prototype.initialize.call(this, {
				collection: collection
			,	viewsPerRow: Infinity
			,	childView: ItemViewsAddonOptionsView
			,	template: addon_options_items_tpl
			});
			
			if (is_sca_advance)
			{
				this.options.application.getLayout().once('afterAppendView', function ()
				{
					self.collection.fetchItems().done(function(){
						
						self.setUp();
					});
				});
			}
		}
		
	,	setUp: function() {
			var self = this
			,	parent_id = this.options.itemsIds;

			//Set the option price and type on the model
			_.each(this.collection.AddonOptionsPrices, function(priceObject) {
				//Find the model for each of the addon items
				var addonOptionItem = _.find(self.collection.models, function(item){ return item.id == priceObject.item; });
				//Add additional attributes to the model
				if (addonOptionItem) {
					var price = priceObject.price;
					var priceStr = parseFloat(price).toFixed(2);
					addonOptionItem.attributes.optionPrice = parseFloat(price).toFixed(2);
					addonOptionItem.attributes.optionType = priceObject.productType;
					addonOptionItem.attributes.groupId = priceObject.productType.replace(/\s/ig, '-').toLowerCase();
					addonOptionItem.attributes.moreThanOnlinecustomerprice = addonOptionItem.get('onlinecustomerprice') <= parseFloat(price) ? false : true
					addonOptionItem.itemOptions.custcol_child_price = {};
					addonOptionItem.itemOptions.custcol_child_price.internalid = priceStr;
					addonOptionItem.itemOptions.custcol_child_price.label = priceStr;
					addonOptionItem.itemOptions.custcol_pco = {};
					addonOptionItem.itemOptions.custcol_pco.internalid = parent_id.toString();
					addonOptionItem.itemOptions.custcol_pco.label = parent_id.toString();
				}
			});
			
			//Group the items by type
			var items = this.collection.models
			,	arrayGrouped = []
			,	itemsInStock = _.filter(items, function(item){return item.get('isinstock');})
			,	itemsGrouped = _.groupBy(itemsInStock, function(item){return item.get('optionType');});

			//Loop through the groups and sortby price within the groups then set up for template looping
			_.each(itemsGrouped, function(group) {
				var groupSorted = _.sortBy(group, function(item){return item.get('optionPrice');})
				, obj = {};
				obj.group = groupSorted;
				arrayGrouped.push(obj);
			});
			
			//Sort group types by alpha
			this.itemsGrouped = _.sortBy(arrayGrouped, function(array){return array.group[0].get('optionType');});
			this.render();
		}
		
	,	setAddonOption: function(e) {
			var itemId = e.currentTarget.id
			,	addon = this.collection.get(itemId)
			,	addonGroup = $(e.currentTarget).attr('group');
			
			//Set unselected items to half opacity
			$('#' + addonGroup + ' > div').toggleClass('opacity-half');
			//Set unselected items input to disabled
			$('#' + addonGroup + ' [type="checkbox"]').prop('disabled', function(i, val){ return !val;});
			//Re-set selected item opacity
			$('#' + addonGroup + '-' + itemId).toggleClass('opacity-half');
			//Re-set selected item input disabled to enabled so users can unselect
			$('#' + itemId).removeProp('disabled');
			//Re-set checked items (in case of hacking)
			$('#' + addonGroup + ' [type="checkbox"]').prop('checked', false);
			
			if (addon.get('checked') == false || addon.get('checked') == undefined)
			{
				addon.set('checked', true);
				$('#' + itemId).prop('checked', true);
			} else {
				addon.set('checked', false);
				$('#' + itemId).prop('checked', false);
			}
			
			//Verify that there is only one selected item in the group. If more than 1 then unset all check attributes and html properties (in case of hacking)
			var collectionGroup = _.filter(this.collection.models, function(item){return item.get('optionType') == addonGroup;})
			,	selectedWithinGroup = _.filter(collectionGroup, function(item){return item.get('checked') == true});
			
			if (selectedWithinGroup.length > 1)
			{
				_.each(collectionGroup, function(item){item.set('checked', false);});
				$('#' + addonGroup + ' [type="checkbox"]').prop('checked', false);
			}
			
			//Display the selected items sku and price in push pane header
			var sku = addon.get('_sku')
			,	option_price = addon.get('optionPrice');
			if(!$('a[data-target="#group-' + addonGroup + '"] div#selected-option-text-' + sku).length){
				$('a[data-target="#group-' + addonGroup + '"]').append('<div id="selected-option-text-' + sku + '" style="float: right">' + sku + ' <span class="item-pa-options-sale">$' + option_price + '</span></div>');
			} else {
				$('div#selected-option-text-' + sku).remove();
			}
			
			//this.getSubAddonOptions(addon);
		}
		
	,	getSubAddonOptions: function(addon)
		{
		}
		
	,	getContext: function () {
			return {
				items: this.itemsGrouped
			}
		}
	});
});
