define('AddonOptionsItems.Collection',
  [
		'Singleton'
	,	'SC.Configuration'
	,	'ItemDetails.Collection'
	
	,	'Backbone'
	,	'underscore'
	,	'Utils'
  ],
  function (
		Singleton
	,	Configuration
	,	ItemDetailsCollection
	
	,	Backbone
	,	_
	,	Utils
	)
{
	'use strict';
	
    return ItemDetailsCollection.extend({
		
		initialize: function (options)
		{
			this.searchApiMasterOptions = Configuration.searchApiMasterOptions.itemOptions;
			this.itemsIds = _.isArray(options.itemsIds) ? _.sortBy(options.itemsIds, function (id){return id;}) : [options.itemsIds];
			
			this.AddonOptionsItems = options.AddonOptionsItems.join(',');
			this.AddonOptionsPrices = options.AddonOptionsPrices;
		}

		//@method fetchItems @return {jQuery.Deferred}
	,	fetchItems: function ()
		{
			return this.fetch({data:{id: this.AddonOptionsItems}});
		}
		
		// http://backbonejs.org/#Model-parse
	,	parse: function (response)
		{
			return _.compact(response.items) || [];
		}
    });
  }
);