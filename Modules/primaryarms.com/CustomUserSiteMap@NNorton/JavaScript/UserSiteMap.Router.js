//MyNewModule.Router.js
define('UserSiteMap.Router'
,   [
		'UserSiteMap.View'
	,	'UserSiteMap.Model'
	,   'Backbone'
   ]
,   function (
		UserSiteMapView
	,	Model
	,	Backbone
   )
{
   'use strict';

   //@class Address.Router @extend Backbone.Router
   return Backbone.Router.extend({

      routes: {
         'site-map': 'map'
      }

   ,   initialize: function (application)
      {
         this.application = application;
      }

    ,   map: function ()
      {
		   var model = new Model();
           var view = new UserSiteMapView({application: this.application, model: model});
			//wait to render the content for the model to be ready
			model.fetch().done(function() {
			   view.showContent();
		   });
        }
   });
});