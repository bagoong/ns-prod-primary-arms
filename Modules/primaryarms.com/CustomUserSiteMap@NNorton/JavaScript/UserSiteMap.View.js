define(
   'UserSiteMap.View'
,   [
		'Facets.Model'
	,	'user_site_map.tpl'
	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	]
,   function (
		FacetsModel
	,	template
	,	Backbone
	,	jQuery
	,	_
   )
{
   'use strict';

   return Backbone.View.extend({

		template: template
		
	,	title: _('Site Map').translate()
		
	,	getBreadcrumbPages: function() {
      		return [{text: this.title, href: '/site-map'}];
    	}
	
	,	getContext: function ()
		{
			var self = this
			,	facets = this.model.get('facets')
			,	m_categories = _.filter(facets, function(facet){return facet.url === 'MCategories'})
			,	categories_1 = _.filter(facets, function(facet){return facet.url === '1'})
			,	categories_2 = _.filter(facets, function(facet){return facet.url === '2'})
			,	brands = _.filter(facets, function(facet){return facet.url === 'Brand'})
			,	facet_categories = []

			m_categories[0].title = 'Main Categories'
			categories_1[0].title = 'Categories'
			categories_2[0].title = 'Sub Categories'
			brands[0].title = 'Brands'
			facet_categories = m_categories.concat(categories_1, categories_2, brands);

			return {
				categories: facet_categories
				//Prints out all facets
				//facets: this.model.get('facets')
			};
		}
	});
});
