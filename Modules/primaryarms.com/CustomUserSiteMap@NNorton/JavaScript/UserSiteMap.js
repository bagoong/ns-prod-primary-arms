define('UserSiteMap'
,   [
      'UserSiteMap.Router'
   ]
,   function (
      UserSiteMapRouter
   )
{
   'use strict';

   return   {
      mountToApp: function (application)
      {
         // Initializes the router
         return new UserSiteMapRouter(application);
      }
   };
});