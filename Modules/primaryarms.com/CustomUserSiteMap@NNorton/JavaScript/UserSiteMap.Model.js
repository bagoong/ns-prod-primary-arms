define('UserSiteMap.Model',
  [
		'Backbone'
	,	'underscore'
	,	'Utils'
  ],
  function (
  		Backbone
	,	_
	,	Utils
	)
{
	'use strict';
	
    return Backbone.Model.extend({
      		urlRoot: '../../api/items?c=3901023&country=US&currency=USD&include=facets&language=en&limit=0&n=2&offset=0&pricelevel=5&facet.exclude=onlinecustomerprice,custitem_accessory_attachment,custitem_action,custitem_adjustable,custitem_ambidextrous,custitem_armor_type,custitem_assembly,custitem_barrel_length,custitem_bullet_weight,custitem_caliber_gauge,custitem_caliber_marking,custitem_carrier_style,custitem_color,custitem_decibel_reduction,custitem_fitment,custitem_fixed_folding,custitem_focal_plane,custitem_forward_cant,custitem_frame_size,custitem_gas_system_length,custitem_illuminated,custitem_latch_size,custitem_leg_style,custitem_magnification,custitem_material,custitem_night_vision_compatible,custitem_optic_type,custitem_pa_featured_products,custitem_pa_staff_picks,custitem_pa_top_selling,custitem_padded,custitem_platform,custitem_position,custitem_quick_release,custitem_ring_height,custitem_storage,custitem_style,custitem_thread_pattern,custitem_throw,custitem_tube_diameter,custitem_type,custitem_user_serviceable,onlinecustomerprice'
   		 });
  }
);