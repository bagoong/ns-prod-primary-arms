<div style="background: #D9EDF7; text-align:center; padding: 20px; margin-bottom: 15px;">
    <p style="color: #31708F; font-size: 30px"><i class="fa fa-info-circle"></i> Welcome to our new site</p>
    <p style="color: #31708F">If you are a returning customer, for security reasons,<br> please <a href="/forgot-password" style="color: #2F5871; font-size: 20px;">CLICK HERE</a> to re-enter your password.</p>
</div>
<div id="login-message" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content" style="background: #D9EDF7">
      <div class="modal-header">
        <div class="row">
          <div class="col-xs-12" style="text-align: right;">
            <button type="button" class="global-views-modal-content-header-close" data-dismiss="modal" aria-hidden="true" aria-label="Close" style="color: #2F5871;"> × </button>
          </div>
          <div class="col-xs-12">
            <div style="text-align:center; padding: 20px; margin-bottom: 15px;">
              <p style="color: #31708F; font-size: 30px"><i class="fa fa-info-circle"></i> Welcome to our new site</p>
              <p style="color: #31708F">If you are a returning customer, for security reasons,<br> please <a href="/forgot-password" style="color: #2F5871; font-size: 20px;">CLICK HERE</a> to re-enter your password.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
