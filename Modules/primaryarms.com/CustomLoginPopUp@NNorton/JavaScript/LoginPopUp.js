define('LoginPopUp'
,	[	
		'LoginRegister.View'
	,	'PluginContainer'
	,	'LoginPopUp.View'
	,	'underscore'

	]
,	function (
		LoginRegisterView
	,	PluginContainer
	,	LoginPopUpView
	,	_

	)
{
	'use strict';

	return {
		
		mountToApp: function (application)
		{
			var layout = application.getLayout();
			
			LoginRegisterView.prototype.preRenderPlugins = LoginRegisterView.prototype.preRenderPlugins || new PluginContainer();
			
			LoginRegisterView.prototype.preRenderPlugins.install({
				name: 'LoginPopUp'
			,	execute: function ($el, view)
				{
					//Cookie set in Footer.View.js and OrderWizard.Step.js
					var hasSeen = _.getCookie('rstpwpalogt');
					
					//Check for cookie
					if(hasSeen !== 'true'){
					$el
						.find('.login-register-header')//Make sure that this looks for a child of $el otherwise it doesn't work
						.before('<div data-view="LoginPopUp"></div>');
					return $el; 
					}
				}
			});

			LoginRegisterView.prototype.childViews.LoginPopUp = function()
			{
				var view = new LoginPopUpView();
				return view;
			};
		}
	};
});
