define(
	'LoginPopUp.View'
,	[	
		'login_pop_up.tpl'
	,	'Backbone'
	,	'underscore'
	,	'jQuery'
	]
,	function (
		template
	,	Backbone
	,	_
	,	jQuery
	)
{
	'use strict';

	return Backbone.View.extend({

		template: template
		
	,	initialize: function()
		{
			$(document).ready(function(e) {
				$('#login-message').modal('show');
            });
		}
	});
});
