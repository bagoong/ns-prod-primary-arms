define(
	'ResellerRatings.View'
,	[	
		'reseller_ratings.tpl'
	,	'Backbone'
	,	'underscore'
	]
,	function (
		template
	,	Backbone
	,	_
	)
{
	'use strict';

	return Backbone.View.extend({

		template: template

		//@method getContext @return Address.View.Context
	,	getContext: function ()
		{
			return {
				model: this.model
			};
		}
	});
});
