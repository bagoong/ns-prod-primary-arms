
define('ResellerRatings'
,	[	
		'underscore'

	,	'Footer.View' 
	,	'PluginContainer'
	,	'ResellerRatings.View'
	]
,	function (
		_
	,	FooterView
	,	PluginContainer
	,	ResellerRatingsView
	)
{
	'use strict';

	return	{
		loadScript: function ()
		{
			jQuery(document).find('[src="https://widget.resellerratings.com/widget/javascript/review/Primary_Arms.js"]').remove();
			jQuery(document).find('#RR_Reviews_Widget').remove();
			jQuery(document).ready(function(e) {
                jQuery(document).find('#RR_Reviews_Widget').remove();
            });
			
			if (SC.ENVIRONMENT.jsEnvironment === 'browser') 
			{
				jQuery('body').append(jQuery('<script src="https://widget.resellerratings.com/widget/javascript/review/Primary_Arms.js"></script>'));
			}
			
		}

	,	mountToApp: function (application)
		{
			// install the plugin container in the Itemdetails.View class 
			FooterView.prototype.preRenderPlugins = FooterView.prototype.preRenderPlugins || new PluginContainer();

			// install a plugin that will add a box in the PDP, right before before .item-details-main-bottom-banner
			FooterView.prototype.preRenderPlugins.install({
				name: 'ResellerRatings'
			,	execute: function ($el, view)
				{
					$el
						.find('.RR_Reviews_Widget_Wrapper_Container')
						.append('<div id="RR_Reviews_Widget_Wrapper_Content" class="RR_Reviews_Widget_Wrapper_Content collapse"><div id="RR_Reviews_Widget_Wrapper" class="RR_Reviews_Widget_Wrapper" ></div><a data-toggle="collapse" data-target="#RR_Reviews_Widget_Wrapper_Content" aria-expanded="false" aria-controls="accordion-id"><i class="fa fa-times-circle" style="font-size: 25px; float: right; color: #e0e0e0;"></i></a></div>');
					return $el; 
				}
			});

			FooterView.prototype.childViews.ResellerRatings = function()
			{
				var view = new ResellerRatingsView({model: this.model});
				return view;
			};
			
			application.getLayout().once('afterAppendView', jQuery.proxy(this, 'loadScript'));
		}

	};
});
