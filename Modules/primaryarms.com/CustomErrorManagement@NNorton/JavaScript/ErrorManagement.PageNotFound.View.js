/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

/* global nsglobal:true */
// @module ErrorManagement
define(
	'ErrorManagement.PageNotFound.View'
,	[
		'error_management_page_not_found.tpl'
		
	,	'ErrorManagement.View'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	
	,	'Utils'
	]
,	function(
		error_management_page_not_found_tpl
		
	,	ErrorManagementView

	,	Backbone
	,	jQuery
	,	_
	)
{
	'use strict';

	// @class ErrorManagement.PageNotFound.View @extends Backbone.View
	return ErrorManagementView.extend({

		template: error_management_page_not_found_tpl
	,	attributes: {
			'id': 'page-not-found'
		,	'class': 'page-not-found'
		}
	,	title: _('Page not found').translate()
	,	page_header: _('Page not found').translate()
		
	,	initialize: function ()
		{
			if (SC.ENVIRONMENT.jsEnvironment === 'server')
			{
				nsglobal.statusCode = 404;
			}
			
			var self = this
			,	url = SC.ENVIRONMENT.currentHostString
			this.staffpicksdata = {};
			var staffpickscall = jQuery.ajax({
				url: "/api/items?country=US&currency=USD&custitem_pa_staff_picks=true&fieldset=details&include=facets&language=en&limit=6&pricelevel=5"
			,	success: function(data) {
					self.staffpicksdata = data;
					self.render();
				}
			});
		}

		// @method getContext @returns {ErrorManagement.PageNotFound.View.Context}
	,	getContext: function()
		{
			var self = this;
			_.each(self.staffpicksdata.items, function (item){
				var price = item.onlinecustomerprice_detail.priceschedule ? item.onlinecustomerprice_detail.priceschedule[1].price : item.onlinecustomerprice_detail.onlinecustomerprice
                ,	original_price = item.onlinecustomerprice_detail.priceschedule ? item.onlinecustomerprice_detail.priceschedule[0].price : item.onlinecustomerprice_detail.onlinecustomerprice
                ,	map = item.custitem_map_type;
				
				if (price < original_price){
					item.showComparePrice = true;
				}
				if (map === 'PRICE IN CART'){
					item.isMapTypePriceInCart = true;
				}

			});
			
            var price_level = SC.SESSION ? SC.SESSION.priceLevel : SC.DEFAULT_SESSION.priceLevel
            ,	is_dealer = (price_level === '2' || price_level === '6' || price_level === '7');
				
			// @class ErrorManagement.PageNotFound.View.Context
			return {
				// @property {String} title 
				title: this.title
				// @property {String} pageHeader 
			,	pageHeader: this.page_header
			
			,	staffPicks: this.staffpicksdata ? this.staffpicksdata.items : ''
			
			,	isDealer: is_dealer
			
			};
		}

	});
});