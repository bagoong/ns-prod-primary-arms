{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="error-management-page-not-found">
    <div class="error-management-page-not-found-header">
	{{#if pageHeader}}
		<h1>{{pageHeader}}</h1>
	{{/if}}

	   <div id="main-banner" class="error-management-page-not-found-main-banner"></div>
    </div>
    <div id="page-not-found-content" class="error-management-page-not-found-content">
    	{{translate "Looks like the page you were looking for moved, got deleted or cannot be found. While your search didn't match any of our products, you may be interested in these customer favorites. If you would like help with finding the right product to meet your needs, please call 713-344-9600."}}
    </div>
    
	<div class="row-fluid row">
		{{#each staffPicks}}
			<div class="merch-zone-col">
				<div class="item-cell item-cell-grid" itemprop="itemListElement" itemscope itemtype="http://schema.org/Product">
                	<div class="merch-zone-image">
                    	<meta itemprop="url" content="{{this.urlcomponent}}">
                    	<a class="thumbnail" href="{{this.urlcomponent}}" onclick="dataLayer.push({'event': 'productClick', 'data': {'name': '<%= title_fix %>', 'sku': '<%= item.displayname %>', 'price': '<%= item.onlinecustomerprice_detail.onlinecustomerprice %>', 'list': '<%= data.title %>', 'position': <%= (index + 1) %>, 'category': '/', 'brand': '<%= item.manufacturer %>'}});" >
                            <img width="300" height="200" src="{{this.itemimages_detail.urls.0.url}}" alt="{{this.itemimages_detail.urls.0.altimagetext}}" itemprop="image"/>
                        </a>
                    </div>
                	<div class="merch-zone-content-container">
                    	<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                            <div class="merch-zone-item-stock-status">
                                {{#if this.isinstock}}
                                    <span class="merch-zone-in"><i class="fa fa-check-circle"></i> In Stock</span>
                                {{else}}
                                	{{#if this.isbackorderable}}
                                    	{{#if this.custitem_pre_orderable}}
                                            <div class="merch-zone-in">Available for Pre-Order</div>
                                        {{else}}
                                            {{#if this.showoutofstockmessage}}
                                                <div class="merch-zone-out"><i class="fa fa-check-circle"></i> Out of Stock (Backordered)</div>
                                            {{else}}
                                                <span class="merch-zone-in"><i class="fa fa-check-circle"></i> In Stock</span>
                                            {{/if}}
                                        {{/if}}
                                    {{else}}
                                    	<span class="merch-zone-out"><i class="fa fa-times-circle"></i> Out of Stock</span>
                                    {{/if}}
                                {{/if}}
                                {{#if showStockDescription}}
                                    <p class="item-views-stock-msg-description {{stockInfo.stockDescriptionClass}}">
                                        <i class="item-views-stock-icon-description"></i>
                                        {{stockDescription}}
                                    </p>
                                {{/if}}
                            </div>
                            <div class="merch-zone-item-title-container">
                                <span id="merch-zone-item-title-heading">
                                    <a  href="{{this.urlcomponent}}" onclick="dataLayer.push({'event': 'productClick', 'data': {'name': '<%= title_fix %>', 'sku': '<%= item.displayname %>', 'price': '<%= item.onlinecustomerprice_detail.onlinecustomerprice %>', 'list': '<%= data.title %>', 'position': <%= (index + 1) %>, 'category': '/', 'brand': '<%= item.manufacturer %>}});"><span itemprop="name">{{this.pagetitle}}</span></a>
                                </span>
                            </div>
                            <div class="view-more-button-container">
                                <div class="facet-item-view-more">
                                    <a class="facet-item-view-more-button" href="{{this.urlcomponent}}" onclick="dataLayer.push({'event': 'productClick', 'data': {'name': '<%= title_fix %>', 'sku': '<%= item.displayname %>', 'price': '<%= item.onlinecustomerprice_detail.onlinecustomerprice %>', 'list': '<%= data.title %>', 'position': <%= (index + 1) %>, 'category': '/', 'brand': '<%= item.manufacturer %>'}});">View Details</a>
                                </div>
                                <div>
                                    
                                {{#if this.showComparePrice}}
                                    {{#if this.isMapTypePriceInCart}}
                                        {{#if isDealer}}
                                            <p class="item-views-price-lead-p"><span class="item-views-price-lead-sale">{{this.onlinecustomerprice_detail.priceschedule.1.price_formatted}}</span></p>
                                        {{else}}
                                            <p class="item-views-price-lead-p"><span class="item-views-price-lead-sale">Please add to cart for sale price</span></p>
                                        {{/if}}
                                    {{else}}
                                        <p class="item-views-price-lead-p"><span class="item-views-price-lead-sale">{{this.onlinecustomerprice_detail.priceschedule.1.price_formatted}}</span></p>
                                    {{/if}}
                                    <small class="item-views-price-old">
                                        {{this.onlinecustomerprice_detail.priceschedule.0.price_formatted}}
                                    </small>
                                {{else}}
                                    <p class="item-views-price-lead-p"><span class="item-views-price-lead">{{this.onlinecustomerprice_formatted}}</span></p>
                                {{/if}}
                                    
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		{{/each}}
	</div>
</div>