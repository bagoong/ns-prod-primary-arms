{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div style="float: left; width: 25%;" class="item-pa-options-item-wrapper" itemprop="itemListElement" data-item-id="{{itemId}}" data-track-productlist-list="{{track_productlist_list}}" data-track-productlist-category="{{track_productlist_category}}" data-track-productlist-position="{{track_productlist_position}}" data-sku="{{sku}}">

    <a class="item-pa-options-image" {{linkAttributes}}>
		<img src="{{resizeImage thumbnailURL 'thumbnail'}}" alt="{{thumbnailAltImageText}}" />
	</a>
    
	<!--{{#if showRating}}
	<div class="item-views-related-item-rate" data-view="Global.StarRating">
	</div>
	{{/if}}-->
	
    <a {{linkAttributes}} class="item-views-related-item-title">
		<span itemprop="name">{{this.sku}}</span>
	</a>
    
	<div class="item-views-related-item-price" data-view="Item.Price">
	</div>

</div>
