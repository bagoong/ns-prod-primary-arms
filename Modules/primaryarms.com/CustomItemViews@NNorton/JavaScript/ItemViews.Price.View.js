/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module ItemViews
define(
	'ItemViews.Price.View'
,	[
		'item_views_price.tpl'

	,	'Profile.Model'
	,	'Session'
	,	'SC.Configuration'

	,	'Backbone'
	,	'underscore'
	]
,	function(
		item_views_price_tpl

	,	ProfileModel
	,	Session
	,	Configuration

	,	Backbone
	,	_
	)
{
	'use strict';

	// @class ItemViews.Price.View @extends Backbone.View
	var ItemViewsPriceView = Backbone.View.extend(
	{
		//@property {Function} template
		template: item_views_price_tpl

		//@method getUrlLogin Get the login URL contains an origin hash parameter indicating the current URL to came back after login
		//@return {String}
	,	getUrlLogin: function ()
		{
			var url = Session.get('touchpoints.login') + '&origin=' + (Configuration.get('currentTouchpoint') || 'home') + '&origin_hash=';

			return url + encodeURIComponent(this.options.origin === 'PDPQUICK' ? this.model.get('_url').replace('/', '') : Backbone.history.fragment);
		}

		// @method getContext @returns {ItemViews.Price.View.Context}
	,	getContext: function ()
		{
			var self = this
			,	price_container_object = this.model.getPrice()
			,	is_price_range = !!(price_container_object.min && price_container_object.max)
			,	showComparePrice = false
			,	is_cart = false
			,	price_level = SC.SESSION ? SC.SESSION.priceLevel : SC.DEFAULT_SESSION.priceLevel
			,	is_dealer = (price_level === '2' || price_level === '6' || price_level === '7')
			,	map_type = this.model.get('custitem_map_type')
			,	is_price_in_cart = map_type === 'PRICE IN CART';

			if (this.options.linePrice && this.options.linePriceFormatted)
			{
				price_container_object.price = this.options.linePrice;
				price_container_object.price_formatted = this.options.linePriceFormatted;
			}

			if (!this.options.hideComparePrice)
			{
				showComparePrice = is_price_range ? price_container_object.max.price < price_container_object.compare_price : price_container_object.price < price_container_object.compare_price;
			}
			
			if (this.options.origin === 'ITEMVIEWCELL' || this.options.origin === 'PDPCONFIRMATION')
			{
				is_cart = true;
			}
			
			//If this is a gift certificate then set the gift certificate amount on the price object, otherwise this displays $0 or $1 in Purchase History
			if(this.model.get('itemtype') === 'GiftCert')
			{
				var gift_cert_option = _.find(self.model.get('options'), function(option){return option.id === 'CUSTCOL_GIFT_CERTIFICATE_AMOUNT_2'});
				if (gift_cert_option)
				{
					price_container_object.price = parseFloat(gift_cert_option.value);
				}
			}

			//@class ItemViews.Price.View.Context
			return {
					// @property {Boolean} isPriceEnabled
					isPriceEnabled: !ProfileModel.getInstance().hidePrices()
						// @property {String} urlLogin
				,	urlLogin: this.getUrlLogin()
					// @property {Boolean} isPriceRange
				,	isPriceRange: is_price_range
					// @property {Boolean} showComparePrice
				,	showComparePrice: showComparePrice
					// @property {Boolean} isInStock
				,	isInStock: this.model.getStockInfo().isInStock
						// @property {ItemDetails.Model} model
				,	model: this.model
					// @property {String} currencyCode
				,	currencyCode: SC.getSessionInfo('currency') ? SC.getSessionInfo('currency').code : ''
					// @property {String} priceFormatted
				,	priceFormatted: price_container_object.price_formatted || ''
					// @property {String} comparePriceFormatted
				,	comparePriceFormatted: price_container_object.compare_price_formatted || ''
					// @property {String} minPriceFormatted
				,	minPriceFormatted: price_container_object.min ? price_container_object.min.price_formatted : ''
					// @property {String} maxPriceFormatted
				,	maxPriceFormatted: price_container_object.max ? price_container_object.max.price_formatted : ''
					// @property {Number} price
				,	price: price_container_object.price ? price_container_object.price.toFixed(2) : 0
					// @property {Number} comparePrice
				,	comparePrice: price_container_object.compare_price ? price_container_object.compare_price : 0
					// @property {Number} minPrice
				,	minPrice: price_container_object.min ? price_container_object.min.price : 0
					// @property {Number} maxPrice
				,	maxPrice: price_container_object.max ? price_container_object.max.price : 0
					// @property {Boolean} showHighlightedMessage
				,	showHighlightedMessage:	_.indexOf(ItemViewsPriceView.highlightedViews, this.options.origin) >= 0
				
				//,	isLessThanMap: (price_container_object.price ? price_container_object.price : 0) < this.model.get('custitem_pa_map')
				
				,	isMapTypePriceInCart: is_price_in_cart
				
				,	strikethroughprice: price_container_object.compare_price ? price_container_object.compare_price.toFixed(2) : price_container_object.price
				
				,	isDealer: is_dealer
				
				,	isCart: is_cart
				
				,	isOptionContentPrice: !!(this.className === ' item-details-options-content-price')
				
				,	hasRating: !!(this.model.get('custitem_ns_pr_rating'))
			};
			//@class ItemViews.Price.View
		}
	}
,	{
		//@property {Array<String>} highlightedViews Contains the list of all origins that in case of using the "LOGIN TO SEE PRICES" feature must render a highlighted notification message
		//@static
		highlightedViews:  ['PDPQUICK', 'PDPFULL']
	});

	return ItemViewsPriceView;
});

//@class ItemViews.Price.View.Initialize.Options
//@property {String?} origin Possible values are:
//	PDPCONFIRMATION 		For the PDP confirmation message
//	ITEMCELL 				For each item being shown in the item list (ex your-domain.com/search)
//	PDPQUICK 				For a PDP being shown on a quick view form
//	PDPFULL 				Full PDP view
//	RELATEDITEM				Related Item
//	PRODUCTLISTDETAILSLATER	Used to render each item that was saved for later
//	PRODUCTLISTDETAILSFULL	Used to render each item that is shown inside My Account in the details of a particular Product List
//	PRODUCTLISTDETAILSEDIT	Used when rendering the form to edit an item inside a product list
//	ITEMVIEWCELL			For all list where items are rendered
//
//@property {Number?} linePrice Specify this value if you want to use a different price rather than the one of the line
//@property {String?} linePriceFormatted This string valid must be present if you want to use a custom price containing the formatted value of linePrice
//@property {Boolean?} hideComparePrice
//@property {ItemDetails.Model} model