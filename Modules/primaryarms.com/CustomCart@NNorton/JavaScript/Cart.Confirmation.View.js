/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Cart
define('Cart.Confirmation.View'
,	[
		'ItemViews.Price.View'
	,	'Backbone.CompositeView'
	,	'ItemViews.SelectedOption.View'
	,	'Backbone.CollectionView'

	,	'cart_confirmation_modal.tpl'

	,	'jQuery'
	,	'Backbone'
	,	'underscore'
	,	'Utils'
	]
,	function (
		ItemViewsPriceView
	,	BackboneCompositeView
	,	ItemViewsSelectedOptionView
	,	BackboneCollectionView

	,	cart_confirmation_modal_tpl

	,	jQuery
	,	Backbone
	,	_
	)
{
	'use strict';

	// @class Cart.Confirmation.View Cart Confirmation view @extends Backbone.View
	return Backbone.View.extend({

		// @property {Function} template
		template: cart_confirmation_modal_tpl

		// @property {String} title
	,	title: _('Added to Cart').translate()

	,	modalClass: 'global-views-modal-large'
	
		// @property {String} page_header
	,	page_header: _('Added to Cart').translate()

		// @property {Object} attributes
	,	attributes: {
			'id': 'shopping-cart'
		,	'class': 'add-to-cart-confirmation-modal shopping-cart-modal'
		}

		// @property {Object} events
	,	events: {
			'click [data-trigger=go-to-cart]': 'dismisAndGoToCart'
		}

		// @method initialize
	,	initialize: function (options)
		{
			this.model = options.model;
			this.line = this.model.getLatestAddition();
			this.lines = this.model.get('lines');
			this.lineitems = _.map(this.lines.models, function(line){return line.get('item');});

			var self = this
			,	optimistic = this.model.optimistic;

			if (optimistic && optimistic.promise && optimistic.promise.state() === 'pending')
			{
				this.line = options.model.optimisticLine;
				delete this.model.optimisticLine;

				optimistic.promise.done(function ()
				{
					self.line = options.model.getLatestAddition();
					self.render();
				});
			}

			BackboneCompositeView.add(this);
		}

		// @method dismisAndGoToCart
		// Closes the modal and calls the goToCart
	,	dismisAndGoToCart: function (e)
		{
			e.preventDefault();
			this.$containerModal.modal('hide');
			this.options.layout.goToCart();
		}

		// @property {Object} childViews
	,	childViews: {
				'Item.Price': function ()
				{
				return new ItemViewsPriceView({
					model: this.line.get('item')
				,	origin: 'PDPCONFIRMATION'
				});
				}
			/*,	'Item.SelectedOptions': function (options)
				{
					var original_model = _.find(this.lines.models, function(model, index){return index === options.index})
					,	model = {};
					model = original_model.get('item');
					var optionArray = original_model.get('item').getPosibleOptions();
					console.log(this);
					console.log(original_model);
					console.log(this.lines.models);
					console.log(optionArray);
					model.attributes.options = optionArray;
					return new BackboneCollectionView({
						collection: new Backbone.Collection(this.line.get('item').getPosibleOptions())
					,	childView: ItemViewsSelectedOptionView
					,	viewsPerRow: 1
					,	childViewOptions: {
							cartLine: this.line //model//
						}
					});
				}*/
		}

		// @method getContext
		// @return {Cart.Confirmation.View.Context}
	,	getContext: function()
		{
			var item = this.line.get('item')
			,	lines = this.model.get('lines')
			,	summary = this.model.get('summary');
			
			_.each(lines.models, function(line){
				var current_price = line.attributes.item.attributes.onlinecustomerprice_detail.onlinecustomerprice;
				var compare_price = (line.attributes.item.attributes.onlinecustomerprice_detail.priceschedule ? line.attributes.item.attributes.onlinecustomerprice_detail.priceschedule[0].price : current_price)
				if (current_price < compare_price) {
					return line.attributes.onsale = true;
				}
			});

			// @class Cart.Confirmation.View.Context
			return {
					// @property {OrderLine.Model} line
					line: this.line
					// @property {ItemDetails.Model} item
				,	item: item
					// @property {Boolean} showQuantity
				,	showQuantity: (item.get('_itemType') !== 'GiftCert') && (this.line.get('quantity') > 0)
					//@property {String} itemPropSku
				,	itemPropSku: (item.get('_sku'))
				
				,	lines: lines.models
				
				,	summaryTotal: summary.subtotal_formatted
				
				,	linesItemCount: summary.itemcount
			};
		}
		// @class Cart.Confirmation.View
	});

});
