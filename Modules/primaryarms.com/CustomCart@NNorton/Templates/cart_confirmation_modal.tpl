{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="cart-confirmation-modal">
{{#each lines}}
	<div class="cart-confirmation-line-item-container">
	<div class="cart-confirmation-modal-img">
		<img data-loader="false" src="{{resizeImage this.item._thumbnail.url 'main'}}" alt="{{this.item._thumbnail.altimagetext}}">
	</div>
	<div class="cart-confirmation-modal-details">
		<a href="{{item._url}}" class="cart-confirmation-modal-item-name">{{this.item.pagetitle}}</a>
				<!-- SKU -->
		<div class="cart-confirmation-modal-sku">
			<span class="cart-confirmation-modal-sku-label">{{translate 'SKU: '}}</span> 
			<span class="cart-confirmation-modal-sku-value">{{this.item._sku}}</span>
		</div>
		<div class="cart-confirmation-modal-price">
            {{#if this.onsale}}
				<p class="item-views-price-lead-p"><span class="item-views-price-lead-sale">{{this.rate_formatted}}</span></p>
            	<p>
                    <small class="item-views-price-old">
                        {{this.item._comparePriceAgainstFormated}}
                    </small>
                </p>
            {{else}}
            	<p>{{this.rate_formatted}}</p>
            {{/if}}          
		</div>
		<!-- Item Options -->
		<div class="cart-confirmation-modal-options">
			<div data-view="Item.SelectedOptions" data-index="{{@index}}"></div>
		</div>
		<!-- Quantity 
		{{#if showQuantity}}-->
		<div class="cart-confirmation-modal-quantity">
			<span class="cart-confirmation-modal-quantity-label">{{translate 'Quantity: '}}</span>
			<span class="cart-confirmation-modal-quantity-value">{{this.quantity}}</span>
		</div>
		<!--{{/if}}-->
        Amount {{this.amount_formatted}}
	</div>
    </div>
    {{/each}}
    <div class="cart-confirmation-summary">
    	<h4>
    		<span class="cart-confirmation-item-count">{{linesItemCount}}{{translate ' Items'}}</span>
        </h4>
        <h4>
    		<span class="cart-confirmation-subtotal">{{translate 'Subtotal: '}}{{summaryTotal}}</span>
        </h4>
    </div>
    <div class="cart-confirmation-modal-actions">
        <div class="cart-confirmation-modal-view-cart">
            <a href="/cart" class="cart-confirmation-modal-view-cart-button" data-trigger="go-to-cart">{{translate 'View Cart &amp; Checkout'}}</a>
        </div>
        <div class="cart-confirmation-modal-continue-shopping">
            <button class="cart-confirmation-modal-continue-shopping-button" data-dismiss="modal">{{translate 'Continue Shopping'}}</button>
        </div>
    </div>
</div>