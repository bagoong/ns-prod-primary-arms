/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module Home
define(
	'Home.View'
,	[
		'SC.Configuration'
	,	'Utilities.ResizeImage'

	,	'home.tpl'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Utils'
	]
,	function (
		Configuration

	,	resizeImage
	,	home_tpl

	,	Backbone
	,	jQuery
	,	_
	,	Utils
	)
{
	'use strict';

	//@module Home.View @extends Backbone.View
	return Backbone.View.extend({

		template: home_tpl

	,	title: _('AR-15 & AK-47 Parts and Accessories').translate()

	,	page_header: _('AR-15 & AK-47 Parts and Accessories').translate()

	,	attributes: {
			'id': 'home-page'
		,	'class': 'home-page'
		}

	,	events:
		{
			'click [data-action=slide-carousel]': 'carouselSlide'
		}

	,	initialize: function ()
		{
			var self = this;
			this.windowWidth = jQuery(window).width();
			this.on('afterViewRender', function()
			{
				_.initBxSlider(self.$('[data-slider]'), {
					nextText: '<a class="home-gallery-next-icon"></a>'
				,	prevText: '<a class="home-gallery-prev-icon"></a>'
				});
			});

			var windowResizeHandler = _.throttle(function ()
			{
				if (_.getDeviceType(this.windowWidth) === _.getDeviceType(jQuery(window).width()))
				{
					return;
				}
				this.showContent();

				_.resetViewportWidth();

				this.windowWidth = jQuery(window).width();

			}, 1000); 

			this._windowResizeHandler = _.bind(windowResizeHandler, this); 
			
			jQuery(window).on('resize', this._windowResizeHandler);
			
			//Push ajaxStop event to the dataLayer for Google Tag Manager
			//Set variable to check before pushing to prevent some cases of duplication
			var ajaxfirehome = false;
			
			$(document).ajaxStop(function(){
				if(!ajaxfirehome){
					//Set to true to prevent some duplication
					ajaxfirehome = true;
					//Wait one second and then push ajaxStop
					setTimeout(function(){
						dataLayer.push({'event': 'ajaxStop'});
					}, 1000);
				}
			});
			
			//Appends Sale button to DOM for mobile users if is not present.
			Backbone.on('saleDataAvailable', function(saleData){
				//If it's not in the DOM already
				if(!($('#home-main-sale-button').length)){
					//Create the area of all running sales urls
					var urlArray = [];
					_.each(saleData.values, function(value, index){
						urlArray.push((index === 0 ? '+' : ',') + value.url);
					});
					//Append to DOM
					$('[data-cms-area="home_banner_mobile_full_twelve_1_b"]')
						.before('<a href="/' + saleData.url + urlArray + '" data-touchpoint="home" data-hashtag="#/' + saleData.url + urlArray + '"><div id="home-main-sale-button"><h1>Sale</h1></div></a>');
				}
				
				//self.render() this method would sometimes cause CMS content to dissappear;
			});
		}

	,	destroy: function()
		{
			Backbone.View.prototype.destroy.apply(this, arguments); 
			jQuery(window).off('resize', this._windowResizeHandler);
		}

		// @method getContext @return Home.View.Context
	,	getContext: function()
		{
			var config = Configuration.homePage || {}
			,	isset_mobdesk = _.getCookie('pascadesktopmobile')
			,	urlArray = '';
			if(SC.saleData){
				_.each(SC.saleData.values, function(value, index){
					urlArray += (value.url + (index !== SC.saleData.values.length - 1 ? ',' : ''));
				});
			}
			
			return {
				// @class Home.View.Context
				// @property {String} imageResizeId
				imageHomeSize: Utils.getViewportWidth() < 768 ? 'homeslider' : 'main'
				// @property {String} imageHomeSizeBottom
			,	imageHomeSizeBottom: Utils.getViewportWidth() < 768 ? 'homecell' : 'main'
				// @property {Array} carouselImages
			,	carouselImages: config.carouselImages || []
				// @property {Array} bottomBannerImages
    		,	bottomBannerImages: config.bottomBannerImages || []
    			// @class Home.View
			
			,	screenSmall: _.isPhoneDevice() && isset_mobdesk !== "yes"
			
			,	saleData: SC.saleData
			
			,	saleURLArray: urlArray
			
			/*,	screenMedium: _.isTabletDevice()
			
			,	screenLarge: _.isDesktopDevice()*/
			
			};
		}

	});



});
