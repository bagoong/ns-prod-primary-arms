define(
   'ResellerRatingsReviews.View'
,   [
		'reseller_ratings_reviews.tpl'
	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	]
,   function (
		template
	,	Backbone
	,	jQuery
	,	_
   )
{
   'use strict';

   return Backbone.View.extend({

		template: template
		
	,	initialize: function () {
			var self = this;
			
			jQuery.ajax({
				url: 'http://widget.resellerratings.com/widget/snippet/?seller_id=37789'
			,	context: self
			}).done(function(data) {
				var	x = data.getElementsByTagName("overall_rating") ? data.getElementsByTagName("overall_rating")[0] : ''
				,	y = x ? x.childNodes[0] : ''
				,	overall_rating = y ? y.nodeValue : ''
				,	x = data.getElementsByTagName("overall_reviews") ? data.getElementsByTagName("overall_reviews")[0] : ''
				,	y = x ? x.childNodes[0] : ''
				,	overall_reviews = y ? y.nodeValue : ''
				,	reviews = data.getElementsByTagName("ROW")
				,	reviews_array = [];
							
				for (var i = 0; i < reviews.length; i++) {
					var review = {}
					,	reviewXML = reviews[i]
					,	a = reviewXML.getElementsByTagName("rating")[0] ? reviewXML.getElementsByTagName("rating")[0] : ''
					,	b = a ? a.childNodes[0] : ''
					,	c = b ? b.nodeValue : ''
					,	d = reviewXML.getElementsByTagName("title")[0] ? reviewXML.getElementsByTagName("title")[0] : ''
					,	e = d ? d.childNodes[0] : ''
					,	f = e ? e.nodeValue : ''
					,	g = reviewXML.getElementsByTagName("answers")[0] ? reviewXML.getElementsByTagName("answers")[0] : ''
					,	h = g ? g.childNodes[0] : ''
					,	z = g ? g.innerHTML : ''
					,	j = reviewXML.getElementsByTagName("date") ? reviewXML.getElementsByTagName("date")[0] : ''
					,	k = j ? j.childNodes[0] : ''
					,	l = k ? k.nodeValue : ''
					,	m = reviewXML.getElementsByTagName("link") ? reviewXML.getElementsByTagName("link")[0] : ''
					,	n = m ? m.childNodes[0] : ''
					,	o = n ? n.nodeValue : ''
					,	p = reviewXML.getElementsByTagName("comment") ? reviewXML.getElementsByTagName("comment")[0] : ''
					,	q = p ? p.childNodes[0] : ''
					,	r = q ? q.nodeValue : '';
					
					review.rating = parseInt(c);
					review.title = f;
					review.answers = z;
					review.date = l;
					review.url = o;
					review.comment = r;
					
					reviews_array.push(review);
				};
				
				self.overallReviews = overall_reviews;
				self.overallRating = overall_rating;
				self.reviews = reviews_array;
				self.render();
			});
		
		}
		
	,	title: _('Store Ratings and Reviews').translate()
		
	,	getBreadcrumbPages: function() {
      		return [{text: this.title, href: '/store-ratings-and-reviews'}];
    	}
	
	,	getContext: function ()
		{
			
			var model = this.model
			,	reviews = this.reviews;
			
			_.each(reviews, function (review) {
				var starsCount = (review.rating / 2);
				review.stars = _.range(Math.floor(starsCount));
			});
			
			return {
				reviews: reviews ? reviews : ''
			,	rating: this.overallRating
			,	totalReviews: this.overallReviews
			,	maxStars: _.range(5)
			};
		}
	});
});
