define('ResellerRatingsReviews.Collection',
  [
		'ResellerRatingsReviews.Model'
	,	'Backbone'
	,	'underscore'
	,	'Utils'
  ],
  function (
  		Model
	,	Backbone
	,	_
	,	Utils
	)
{
	'use strict';
	
    return Backbone.Model.extend({
		model: Model
	,	url: 'http://widget.resellerratings.com/widget/snippet/?seller_id=37789'
	
		parse: function (data) {
			console.log(data);
		}
    });
  }
);