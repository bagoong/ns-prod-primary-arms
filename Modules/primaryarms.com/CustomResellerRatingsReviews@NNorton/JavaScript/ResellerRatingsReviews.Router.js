//MyNewModule.Router.js
define('ResellerRatingsReviews.Router'
,   [
		'ResellerRatingsReviews.View'
	,   'Backbone'
   ]
,   function (
		ResellerRatingsReviewsView
	,	Backbone
   )
{
   'use strict';

   //@class Address.Router @extend Backbone.Router
   return Backbone.Router.extend({

      routes: {
         'store-ratings-and-reviews': 'reviews'
      }

   ,   initialize: function (application)
      {
         this.application = application;
      }

    ,   reviews: function ()
      {
           var view = new ResellerRatingsReviewsView({application: this.application});
			//wait to render the content for the model to be ready
		   view.showContent();
        }
   });
});