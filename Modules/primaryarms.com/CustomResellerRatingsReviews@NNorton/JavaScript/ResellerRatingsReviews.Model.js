define('ResellerRatingsReviews.Model',
  [
		'Backbone'
	,	'underscore'
	,	'Utils'
  ],
  function (
  		Backbone
	,	_
	,	Utils
	)
{
	'use strict';
	
    return Backbone.Model.extend({
      		urlRoot: 'http://zozo.primaryarms.com/api/items?1=Firearm-Parts&c=3901023&country=US&currency=USD&fieldset=search&include=facets&language=en&n=2&facet.exclude=custitem_1&offset=0&pricelevel=7&sort=quantityavailable%3Adesc'
   		 });
  }
);