<div class="container" style="padding-top: 15px;">
        <div class="row">
            <div class="col-xs-12" data-cms-area="store_reviews_main_a" data-cms-area-filters="path">
            </div>
            <div style="text-align: center;">
            	<h1>Store Rating: <span style="color: #ffcc00">{{rating}}</span> out of 10</h1>
                <h2 style="padding: 0px 20px 20px 20px;">Total reviews: {{totalReviews}}</h2>
                <p><a href="https://www.resellerratings.com/store/Primary_Arms">See all of our store reviews</a></p>
            </div>
        {{#each reviews}}
            <div class="col-xs-12" style="margin: 0px 0px 25px 0px">
            	<div style="clear: both; float: left; padding: 0px 0px 15px 0px; overflow: auto;">
                	<div class="global-views-star-rating" data-validation="control-group">
                    	<div class="global-views-star-rating-area" data-toggle="rater" data-validation="control" data-name="" data-max="5" data-value="{{this.stars}}">
                        	<div class="global-views-star-rating-area-empty"><div class="global-views-star-rating-area-empty-content">
                            	<i class="global-views-star-rating-empty"></i>
                                <i class="global-views-star-rating-empty"></i>
                                <i class="global-views-star-rating-empty"></i>
                                <i class="global-views-star-rating-empty"></i>
                                <i class="global-views-star-rating-empty"></i>
                                </div>
                                </div>
                                <meta itemprop="bestRating" content="5">
                                <div class="global-views-star-rating-area-fill" data-toggle="ratting-component-fill" style="width: 100%">
                                <div class="global-views-star-rating-area-filled">
                                <i class="global-views-star-rating-filled"></i>
                                <i class="global-views-star-rating-filled"></i>
                                <i class="global-views-star-rating-filled"></i>
                                <i class="global-views-star-rating-filled"></i>
                                <i class="global-views-star-rating-filled"></i>
                                </div></div></div> 
                                <meta itemprop="ratingValue" content="5">
                                <meta itemprop="reviewCount" content="0">
                                </div>
                </div><span><small>{{this.date}}</small></span>
                <h4 style="clear: both; float: left; padding: 0px 15px 0px 0px"><a href="{{this.url}}">{{this.answers}}</a></h4> 
                <div style="clear:both; padding: 10px 0px 0px 25px">
                    <div>"{{this.comment}}"</div>
                </div>
            </div>
            <div class="item-details-divider"></div>
            <div class="item-details-divider-desktop"></div>
        {{/each}}
         	<div style="text-align: center;">
                <p><a href="https://www.resellerratings.com/store/Primary_Arms">See all of our store reviews</a></p>
            </div>
        	<div class="col-xs-12" data-cms-area="store_reviews_main_a" data-cms-area-filters="path">
            </div>
        </div>
</div>