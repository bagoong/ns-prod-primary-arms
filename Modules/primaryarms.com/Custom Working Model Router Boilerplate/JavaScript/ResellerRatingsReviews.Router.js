//MyNewModule.Router.js
define('ResellerRatingsReviews.Router'
,   [
		'ResellerRatingsReviews.View'
	,	'ResellerRatingsReviews.Model'
	,   'Backbone'
   ]
,   function (
		ResellerRatingsReviewsView
	,	Model
	,	Backbone
   )
{
   'use strict';

   //@class Address.Router @extend Backbone.Router
   return Backbone.Router.extend({

      routes: {
         'store-ratings-and-reviews': 'reviews'
      }

   ,   initialize: function (application)
      {
         this.application = application;
      }

    ,   reviews: function ()
      {
		   var model = new Model();
           var view = new ResellerRatingsReviewsView({application: this.application, model: model});
			//wait to render the content for the model to be ready
			model.fetch().done(function() {
			   view.showContent();
		   });
        }
   });
});