define('ResellerRatingsReviews'
,   [
      'ResellerRatingsReviews.Router'
   ]
,   function (
      ResellerRatingsReviewsRouter
   )
{
   'use strict';

   return   {
      mountToApp: function (application)
      {
         // Initializes the router
         return new ResellerRatingsReviewsRouter(application);
      }
   };
});