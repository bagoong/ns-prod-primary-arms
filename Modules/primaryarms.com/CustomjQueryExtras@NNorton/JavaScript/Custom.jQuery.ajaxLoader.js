/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

/*
@module jQueryExtras

#jQuery.ajaxSetup

Changes jQuery's ajax setup defaults for adding the loading icon. Updates icon's placement on mousemove. 
*/

define('Custom.jQuery.ajaxLoader', ['jQuery','underscore','Utils'], function (jQuery,_)
{
	'use strict';

	jQuery(document).ready(function ()
	{
		var $body = jQuery(document.body)
			//Custom loading icon
		,	$custom_loading_icon = jQuery('#customloadingIndicator');
		
		if (!$custom_loading_icon.length)
		{
			//Create custom loading indicator
			$custom_loading_icon = jQuery('<div id="customloadingIndicator" class="cssload-container"><div class="cssload-speeding-wheel"></div></div>').hide();

			if (!_.result(SC, 'isPageGenerator'))
			{
				$custom_loading_icon.appendTo($body);
			}
		}
		
			if (!_.result(SC, 'isPageGenerator'))
			{
				$custom_loading_icon.appendTo($body);
			}
		
		//Global reference to custom indicator
		SC.$customloadingIndicator = $custom_loading_icon;

	});

	SC.loadingIndicatorShow = function ()
	{
		SC.$customloadingIndicator && SC.$customloadingIndicator.show();
	};

	SC.loadingIndicatorHide = function ()
	{
		SC.$customloadingIndicator && SC.$customloadingIndicator.hide();
	};

	// This registers an event listener to any ajax call
	var $document = jQuery(document)
		// http://api.jquery.com/ajaxStart/
		.ajaxStart(SC.loadingIndicatorShow)
		// http://api.jquery.com/ajaxStop/
		.ajaxStop(SC.loadingIndicatorHide);

    // fix to solve APM issue (timebrowser timing): https://confluence.corp.netsuite.com/display/SCRUMPSGSVCS/RUM+API+Issues+and+Enhancements
    if(_.result(SC.ENVIRONMENT, 'SENSORS_ENABLED'))
    {
        $document.ajaxStop(function()
        {
            if(typeof window.NLRUM !== 'undefined')
            {
                window.NLRUM.mark('done');
            }
        });
    }

	// http://api.jquery.com/jQuery.ajaxSetup/
	jQuery.ajaxSetup({
		beforeSend: function (jqXhr, options)
		{
			// BTW: "!~" means "== -1"
			if (!~options.contentType.indexOf('charset'))
			{
				// If there's no charset, we set it to UTF-8
				jqXhr.setRequestHeader('Content-Type', options.contentType + '; charset=UTF-8');
			}
		}
	});

});
