/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module SC
// @class SC.Configuration
// All of the applications configurable defaults

define(
	'SC.Configuration'
,	[

		'item_views_option_tile.tpl'
	,	'item_views_option_text.tpl'
	,	'item_views_selected_option.tpl'

	,	'underscore'
	,	'Utils'
	]

,	function (

		item_views_option_tile_tpl
	,	item_views_option_text_tpl
	,	item_views_selected_option_tpl

	,	_
	)
{

	'use strict';
	var ar15=[
				{
                    text: _('Barrels').translate()
                ,   href: '/MCategories+AR-15-Barrels'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Barrels'
					}
                }  
             ,  {
                    text: _('Bolt Carriers').translate()
                ,   href: '/MCategories+AR-15-Bolt-Carriers'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Bolt-Carriers'
					}
                }
                ,   {
                    text: _('Buffers').translate()
                ,   href: '/MCategories+AR-15-Buffers'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AR-15-Buffers'
					}
                }
            ,   {
                    text: _('Buffer Springs').translate()
                ,   href: '/MCategories+AR-15-Buffer-Springs'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Buffer-Springs'
			}
                }
            ,   {
                    text: _('Charging Handles').translate()
                ,   href: '/MCategories+AR-15-Charging-Handles'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Charging-Handles'
					}
                }
            ,   {
                    text: _('Complete Rifles').translate()
                ,   href: '/MCategories+AR-15-Complete-Guns'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Complete-Guns'
					}
                }
             ,   {
                    text: _('Complete Pistols').translate()
                ,   href: '/MCategories+AR-15-Pistols'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Pistols'
					}
                }
             ,   {
                    text: _('Gas Blocks').translate()
                ,   href: '/MCategories+AR-15-Gas-Blocks'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Gas-Blocks'
					}
                }
             ,   {
                    text: _('Grips').translate()
                ,   href: '/MCategories+AR-15-Grips'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Grips'
			}

                }
             ,  {
                    text: _('Lower Parts').translate()
                ,   href: '/MCategories+AR-15-Lower-Parts'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
						{
						touchpoint: 'home'
				,		hashtag: '#/MCategories+AR-15-Lower-Parts'
						}
                }
			,   {
                    text: _('Lower Receivers').translate()
                ,   href: '/MCategories+AR-15-Lower-Receivers'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Lower-Receivers'
			}
                }
			,   {
                    text: _('Magazines').translate()
                ,   href: '/MCategories+AR-15-Magazines'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AR-15-Magazines'
					}
                }
            ,   {
                    text: _('Magazine Releases').translate()
                ,   href: '/MCategories+AR-15-Magazine-Releases'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Magazine-Releases'
			}
                }
            ,   {
                    text: _('Part Kits').translate()
                ,   href: '/MCategories+AR-15-Part-Kits'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Part-Kits'
			}
                }
 			,   {
                    text: _('Rails & Handguards').translate()
                ,   href: '/MCategories+AR-15-Rails--AND--Handguards'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AR-15-Rails--AND--Handguards'
					}
                }
 			,   {
                    text: _('Rifle Kits').translate()
                ,   href: '/MCategories+AR-15-Rifle-Kits'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AR-15-Rifle-Kits'
					}
                }
             ,  {
                    text: _('Stocks').translate()
                ,   href: '/MCategories+AR-15-Stocks'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Stocks'
			}
                }
             
            
             ,   {
                    text: _('Stock Components').translate()
                ,   href: '/MCategories+AR-15-Stock-Component'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Stock-Component'
			}
                }
            ,   {
                    text: _('Triggers').translate()
                ,   href: '/MCategories+AR-15-Triggers'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Triggers'
			}
                }          
             ,  {
                    text: _('Upper Parts').translate()
                ,   href: '/MCategories+AR-15-Upper-Parts'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AR-15-Upper-Parts'
					}
                }
          
             ,  {
                    text: _('Upper Receivers').translate()
                ,   href: '/MCategories+AR-15-Upper-Receivers'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AR-15-Upper-Receivers'
					}
                }

	];
/*
	var navigationDummyCategories = [
		{
			text: _('Jeans').translate()
		,	href: '/search'
		,	'class': 'header-menu-level3-anchor'
		,	data: {
				touchpoint: 'home'
				, hashtag: '#search'
			}
		},
		{
			text: _('Sweaters').translate()
		,	href: '/search'
		,	'class': 'header-menu-level3-anchor'
		,	data: {
				touchpoint: 'home'
			,	hashtag: '#search'
			}
		},
		{
			text: _('Cardigan').translate()
		,	href: '/search'
		,	'class': 'header-menu-level3-anchor'
		,	data: {
				touchpoint: 'home'
			,	hashtag: '#search'
			}
		},
		{
			text: _('Active').translate()
		,	href: '/search'
		,	'class': 'header-menu-level3-anchor'
		,	data: {
				touchpoint: 'home'
			,	hashtag: '#search'
			}
		},
		{
			text: _('Shoes').translate()
		,	href: '/search'
		,	'class': 'header-menu-level3-anchor'
		,	data: {
				touchpoint: 'home'
			,	hashtag: '#search'
			}
		}
	];
/*	,   categories:[
				{
					categories: alldepts
				}
			]
*/
	
	/*
	,   categories:[
				{
					categories: alldepts
				}
	*/
	
	var ak47=[
				{
                    text: _('Complete Guns').translate()
                ,   href: '/MCategories+AK-47-Complete-Guns'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AK-47-Complete-Guns'
					}
                }
            ,	{
                    text: _('Grips').translate()
                ,   href: '/MCategories+AK-47-Grips'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AK-47-Grips'
					}
                }
            ,	{
                    text: _('Magazines').translate()
                ,   href: '/MCategories+AK-47-Magazines'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AK-47-Magazines'
					}
                }
            ,	{
                    text: _('Optic Mounts').translate()
                ,   href: '/MCategories+AK-47-Optic-Mounts'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AK-47-Optic-Mounts'
					}
                }
            ,  {
                    text: _('Parts').translate()
                ,   href: '/MCategories+AK-47-Parts'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AK-47-Parts'
					}
                }
            ,	{
                    text: _('Rails & Handguards').translate()
                ,   href: '/MCategories+AK-47-Rails--AND--Handguards'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AK-47-Rails--AND--Handguards'
					}
                }
            ,	{
                    text: _('Stock Components').translate()
                ,   href: '/MCategories+AK-47-Stock-Component'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AK-47-Stock-Component'
					}
                } 
			,	{
                    text: _('Stocks').translate()
                ,   href: '/MCategories+AK-47-Stocks'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AK-47-Stocks'
					}
                }
            ,	{
                    text: _('Triggers').translate()
                ,   href: '/MCategories+AK-47-Triggers'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AK-47-Triggers'
					}
                }
	];
	var reddots=[
	 {
                    text: _('Magnifier Mounts').translate()
                ,   href: '/MCategories+Magnifier-Mounts'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+Magnifier-Mounts'
			}
                }
             ,  {
                    text: _('Red Dot Accessories').translate()
                ,   href: '/MCategories+Red-Dot-Accessories'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+Red-Dot-Accessories'
			}
                }
            ,   {
                    text: _('Red Dot Magnifiers').translate()
                ,   href: '/MCategories+Red-Dot-Magnifiers'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+Red-Dot-Magnifiers'
			}
                }
             ,   {
                    text: _('Red Dot Mounts').translate()
                ,   href: '/MCategories+Red-Dot-Mounts'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+Red-Dot-Mounts'
			}
                }
             ,   {
                    text: _('Red Dot Sights').translate()
                ,   href: '/MCategories+Red-Dot-Sights'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+Red-Dot-Sights'
			}
                }
	];
	var scopes=[
	 {
                    text: _('Scope Accessories').translate()
                ,   href: '/MCategories+Scope-Accessories'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Scope-Accessories'
					}
                }
             ,  {
                    text: _('Scope Mounts').translate()
                ,   href: '/MCategories+Scope-Mounts'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
						{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Scope-Mounts'
						}
                }
            ,   {
                    text: _('Scope Rings').translate()
                ,   href: '/MCategories+Scope-Rings'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Scope-Rings'
					}
                }
             ,   {
                    text: _('Rifle Scopes').translate()
                ,   href: '/MCategories+Rifle-Scopes'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Rifle-Scopes'
					}
                }
			,   {
                    text: _('ACSS Optics').translate()
                ,   href: '/MCategories+ACSS-Optics'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+ACSS-Optics'
					}
                }
	];
	//Currently this is not in use, it was used as a flyout menu for Ammunition under Department
	var ammo=[
	 {
                    text: _('.22 LR').translate()
                ,   href: '/MCategories+22-LR'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+22-LR'
					}
                }
             ,  {
                    text: _('.22-.250').translate()
                ,   href: '/MCategories+22-250'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+22-250'
					}
                }
            ,   {
                    text: _('.223/5.56 NATO').translate()
                ,   href: '/MCategories+223-SLASH-556-NATO'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+223-SLASH-556-NATO'
			}
                }
             ,   {
                    text: _('.243 Winchester').translate()
                ,   href: '/MCategories+243-Winchester'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+243-Winchester'
			}
                }
             ,   {
                    text: _('.25-06').translate()
                ,   href: '/MCategories+25-06'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+25-06'
			}
                }
             ,   {
                    text: _('.270 Winchester').translate()
                ,   href: '/MCategories+270-Winchester'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+270-Winchester'
			}
                }
             ,   {
                    text: _('.270 WSM').translate()
                ,   href: '/MCategories+270-WSM'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+270-WSM'
			}
                }
             ,   {
                    text: _('.280 Rem').translate()
                ,   href: '/MCategories+280-Rem'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+280-Rem'
			}
                }
             ,   {
                    text: _('.300 Win Mag').translate()
                ,   href: '/MCategories+300-Win-Mag'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+300-Win-Mag'
			}
                }
             ,   {
                    text: _('.300 WSM').translate()
                ,   href: '/MCategories+300-WSM'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+300-WSM'
			}
                }
             ,   {
                    text: _('.30-06').translate()
                ,   href: '/MCategories+30-06'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+30-06'
			}
                }
             ,   {
                    text: _('.300 Blackout (7.62X35MM)').translate()
                ,   href: '/MCategories+300-Blackout-762x35mm'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+300-Blackout-762x35mm'
			}
                }
             ,   {
                    text: _('.30-30').translate()
                ,   href: '/MCategories+30-30'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+30-30'
			}
                }
             ,   {
                    text: _('.308 Winchester').translate()
                ,   href: '/MCategories+308-Winchester'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+308-Winchester'
			}
                }
             ,   {
                    text: _('.338 Lapua Magnum').translate()
                ,   href: '/MCategories+338-Lapua-Magnum'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+338-Lapua-Magnum'
			}
                }
             ,   {
                    text: _('.357 Magnum/.38 Special').translate()
                ,   href: '/MCategories+357-Magnum-SLASH-38-Special'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+357-Magnum-SLASH-38-Special'
			}
                }
             ,   {
                    text: _('.380 ACP').translate()
                ,   href: '/MCategories+380-ACP'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+380-ACP'
			}
                }
             ,   {
                    text: _('.40 S&W').translate()
                ,   href: '/MCategories+40-S-AND-W'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+40-S-AND-W'
			}
                }
			,   {
                    text: _('See More').translate()
                ,   href: '/MCategories+Ammunition'
                ,   'class': 'header-menu-level2-menu'
		        ,   data: {
					touchpoint: 'home'
				,   hashtag: '#/MCategories+Ammunition'
		    }
				}
	];
	var ar308=[
	{
                    text: _('Barrels').translate()
                ,   href: '/MCategories+AR-308-Barrels'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Barrels'
			}
                }
             ,  {
                    text: _('Bolt Carriers').translate()
                ,   href: '/MCategories+AR-308-Bolt-Carriers'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Bolt-Carriers'
			}
                }
			,  {
                    text: _('Complete Rifles').translate()
                ,   href: '/MCategories+AR-308-Complete-Guns'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Complete-Guns'
			}
                }
             ,   {
                    text: _('Gas Blocks').translate()
                ,   href: '/MCategories+AR-308-Gas-Blocks'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Gas-Blocks'
			}
                }
             ,   {
                    text: _('Grips').translate()
                ,   href: '/MCategories+AR-308-Grips'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Grips'
			}
                }
             ,   {
                    text: _('Lower Parts').translate()
                ,   href: '/MCategories+AR-308-Lower-Parts'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Lower-Parts'
			}
                }

             ,   {
                    text: _('Lower Receivers').translate()
                ,   href: '/MCategories+AR-308-Lower-Receivers'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Lower-Receivers'
			}
                }
             ,   {
                    text: _('Magazines').translate()
                ,   href: '/MCategories+AR-308-Magazines'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Magazines'
			}
                }
             ,   {
                    text: _('Rails & Handguards').translate()
                ,   href: '/MCategories+AR-308-Rails--AND--Handguards'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Rails--AND--Handguards'
			}
                }
             ,   {
                    text: _('Stocks').translate()
                ,   href: '/MCategories+AR-308-Stocks'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Stocks'
			}
                }

             ,   {
                    text: _('Stock Components').translate()
                ,   href: '/MCategories+AR-308-Stock-Component'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Stock-Component'
			}
                }
             ,   {
                    text: _('Triggers').translate()
                ,   href: '/MCategories+AR-308-Triggers'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Triggers'
			}
                }
             ,   {
                    text: _('Upper Parts').translate()
                ,   href: '/MCategories+AR-308-Upper-Parts'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Upper-Parts'
			}
                }
 			,   {
                    text: _('Upper Receivers').translate()
                ,   href: '/MCategories+AR-308-Upper-Receivers'
                ,   'class': 'header-menu-level3-menu'
				,	data: 
					{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AR-308-Upper-Receivers'
					}
                }
	];
	var arpistols=[
	 {
                    text: _('Barrels').translate()
                ,   href: '/MCategories+AR-15-Barrels'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Barrels'
			}
                }
             ,  {
                    text: _('Bolt Carriers').translate()
                ,   href: '/MCategories+AR-15-Bolt-Carriers'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Bolt-Carriers'
			}
                }
             ,   {
                    text: _('Complete Pistols').translate()
                ,   href: '/MCategories+AR-15-Pistols'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Pistols'
			}
                }
             ,   {
                    text: _('Gas Blocks').translate()
                ,   href: '/MCategories+AR-15-Gas-Blocks'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Gas-Blocks'
			}
                }
             ,   {
                    text: _('Grips').translate()
                ,   href: '/MCategories+AR-15-Grips'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Grips'
			}
                }
             ,   {
                    text: _('Lower Parts').translate()
                ,   href: '/MCategories+AR-15-Lower-Parts'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Lower-Parts'
			}
                }

             ,   {
                    text: _('Lower Receivers').translate()
                ,   href: '/MCategories+AR-15-Lower-Receivers'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Lower-Receivers'
			}
                }

             ,   {
                    text: _('Magazines').translate()
                ,   href: '/MCategories+AR-15-Magazines'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Magazines'
			}
                }
             ,   {
                    text: _('Stock Components').translate()
                ,   href: '/MCategories+AR-15-Stock-Component'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Stock-Component'
			}
                }
             ,   {
                    text: _('Triggers').translate()
                ,   href: '/MCategories+AR-15-Triggers'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Triggers'
			}
                }
             ,   {
                    text: _('Upper Parts').translate()
                ,   href: '/MCategories+AR-15-Upper-Parts'
                ,   'class': 'header-menu-level3-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Upper-Parts'
			}
                }
	];

	var alldepts=[
			{
				text:_('Platform').translate()
			,	'class':'header-menu-level2-anchor'
			,	group: true
			,	widthClass: 'col-xs-2'
			,	categories: [
					{
	                    text: _('AR-15').translate()
	                ,   href: '/MCategories+AR-15'
	                ,   'class': 'header-menu-level3-menu'
					,	data: 
						{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AR-15'
						}
					,	categories: ar15
	                }
	            ,   {   
	                    text: _('AR-308').translate()
	                ,   href: '/MCategories+AR-308'
	                ,   'class': 'header-menu-level3-menu'
					,	data:
						{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AR-308'
						}
					,	categories: ar308
	                }
	            ,   {   
	                    text: _('AK-47').translate()
	                ,   href: '/MCategories+AK-47'
	                ,   'class': 'header-menu-level3-menu'
					,	data: 
						{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+AK-47'
						}
					,	categories: ak47
	                }/*
	                ,   {   
	                    text: _('Other Platforms').translate()
	                ,   href: '/MCategories+Other-Platforms'
	                ,   'class': 'header-menu-level3-menu'
					,	data: 
						{
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Other-Platforms'
						}
	                }*/
				]}
			,	{
					text: _('Product Type').translate()
				,	'class':'header-menu-level2-anchor'
				,	group: true
				,	widthClass: 'col-xs-8'
				,	categories: [
						{   
		                    text: _('Clearance').translate()
		                ,   href: '/MCategories+Clearance'
		                ,   'class': 'header-menu-level3-menu'
						,	data: 
							{
							touchpoint: 'home'
						,	hashtag: '#/MCategories+Clearance'
							}
		                }
		             ,	{   
		                    text: _('Ammunition').translate()
		                ,   href: '/MCategories+Ammunition'
		                ,   'class': 'header-menu-level3-menu'
						,	data: 
							{
							touchpoint: 'home'
						,	hashtag: '#/MCategories+Ammunition'
							}
						}
		             ,  {   
		                    text: _('Apparel').translate()
		                ,   href: '/MCategories+Apparel'
		                ,   'class': 'header-menu-level3-menu'
						,	data: {
							touchpoint: 'home'
						,	hashtag: '#/MCategories+Apparel'
						}
		
		                }
		            ,   {   
		                    text: _('Batteries').translate()
		                ,   href: '/MCategories+Batteries'
		                ,   'class': 'header-menu-level3-menu'
					,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Batteries'
					}
		
		                }
		            ,   {   
		                    text: _('Bipods').translate()
		                ,   href: '/MCategories+Bipods'
		                ,   'class': 'header-menu-level3-menu'
					,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Bipods'
					}
		
		                }
					]}
				,	{
					text: _('').translate()
				,	'class':''
				,	group: true
				,	categories: [
		               {   
		                    text: _('Firearms').translate()
		                ,   href: '/MCategories+Firearms'
		                ,   'class': 'header-menu-level3-menu'
						,	data: {
							touchpoint: 'home'
						,	hashtag: '#/MCategories+Firearms'
						}
		
		                }
		            ,   {   
		                    text: _('First Aid').translate()
		                ,   href: '/MCategories+First-Aid'
		                ,   'class': 'header-menu-level3-menu'
					,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+First-Aid'
					}
		
		                }
		            ,   {   
		                    text: _('Grips').translate()
		                ,   href: '/MCategories+Grips'
		                ,   'class': 'header-menu-level3-menu'
					,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Grips'
					}
		
		                }
		            ,   {   
		                    text: _('Gun Parts').translate()
		                ,   href: '/MCategories+Gun-Parts'
		                ,   'class': 'header-menu-level3-menu'
					,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Gun-Parts'
					}
		
		                }
		            ,   {   
		                    text: _('Handguns').translate()
		                ,   href: '/MCategories+Handguns'
		                ,   'class': 'header-menu-level3-menu'
					,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Handguns'
					}
		
		                }
					]}
				,	{
					text: _('').translate()
				,	'class':''
				,	group: true
				,	categories: [
		               {   
		                    text: _('Lights and Lasers').translate()
		                ,   href: '/MCategories+Lights-AND-Lasers'
		                ,   'class': 'header-menu-level3-menu'
					,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Lights-AND-Lasers'
					}
		
		                }
					,   {   
		                    text: _('Magazines').translate()
		                ,   href: '/MCategories+Magazines'
		                ,   'class': 'header-menu-level3-menu'
						,	data: 
							{
								touchpoint: 'home'
							,	hashtag: '#/MCategories+Magazines'
							}
		
		                }
		            ,
		                {   
		                    text: _('Mounts').translate()
		                ,   href: '/MCategories+Mounts'
		                ,   'class': 'header-menu-level3-menu'
					,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Mounts'
					}
		
		                }
		            ,   {   
		                    text: _('Optics').translate()
		                ,   href: '/MCategories+Optics'
		                ,   'class': 'header-menu-level3-menu'
					,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Optics'
					}
		
		                }
		            ,  {   
		                    text: _('Red Dots').translate()
		                ,   href: '/MCategories+Red-Dots'
		                ,   'class': 'header-menu-level3-menu'
						,	data: 
							{
							touchpoint: 'home'
						,	hashtag: '#/MCategories+Red-Dots'
							}
						,	categories:reddots
		                }
		]}
				,	{
					text: _('').translate()
				,	'class':''
				,	group: true
				,	categories: [
		            ,  {   
		                    text: _('Scopes').translate()
		                ,   href: '/MCategories+Scopes'
		                ,   'class': 'header-menu-level3-menu'
						,	data: 
							{
							touchpoint: 'home'
						,	hashtag: '#/MCategories+Scopes'
							}
						,	categories:scopes
		                }
		            ,  {   
		                    text: _('Sights').translate()
		                ,   href: '/MCategories+Sights'
		                ,   'class': 'header-menu-level3-menu'
					,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Sights'
					}
		
		                }
		            ,  {   
		                    text: _('Shooting Gear').translate()
		                ,   href: '/MCategories+Shooting-Gear'
		                ,   'class': 'header-menu-level3-menu'
					,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Shooting-Gear'
					}
		
		                }
		            ,   {   
		                    text: _('Survival Gear').translate()
		                ,   href: '/MCategories+Survival-Gear'
		                ,   'class': 'header-menu-level3-menu'
					,	data: {
						touchpoint: 'home'
					,	hashtag: '#/MCategories+Survival-Gear'
					}
		
		                }
		            ,   {   
		                    text: _('Tools and Cleaning').translate()
		                ,   href: '/MCategories+Tools-AND-Cleaning-Supplies'
		                ,   'class': 'header-menu-level3-menu'
					,	data: {
						touchpoint: 'home'
						,	hashtag: '#/MCategories+Tools-AND-Cleaning-Supplies'
						}
					}
				]
				}
		];


	var Configuration = {


		// @property {Object} searchPrefs Search preferences
		searchPrefs:
		{
			// @property {Number} searchPrefs.maxLength Search preferences. keyword maximum string length - user won't be able to write more than 'maxLength' chars in the search box
			maxLength: 40

			// @property {Function} searchPrefs.keywordsFormatter Search preferences. Keyword formatter function will format the text entered by the user in the search box. This default implementation will remove invalid keyword characters like *()+-="
		,	keywordsFormatter: function (keywords)
			{
				if (keywords === '||')
				{
					return '';
				}

				var anyLocationRegex = /[\(\)\[\]\{\~\}\!\"\:\/]{1}/g // characters that cannot appear at any location
				,	beginingRegex = /^[\*\-\+]{1}/g // characters that cannot appear at the begining
				,	replaceWith = ''; // replacement for invalid chars

				return keywords.replace(anyLocationRegex, replaceWith).replace(beginingRegex, replaceWith);
			}
		}

		// @property {String} imageNotAvailable url for the not available image
	,	imageNotAvailable: _.getAbsoluteUrl('img/no_image_available.jpeg')

	,	templates: {
			itemOptions: {
				// each apply to specific item option types
				selectorByType:	{
					select: item_views_option_tile_tpl
				,	'default': item_views_option_text_tpl
				}
				// for rendering selected options in the shopping cart
			,	selectedByType: {
					'default': item_views_selected_option_tpl
				}
			}
		}

		// @class SCA.Shopping.Configuration
		// @property {Array<Object>} footerNavigation links that goes in the footer
	,	footerNavigation: [
			{text: 'Link a', href:'#'}
		,	{text: 'Link b', href:'#'}
		,	{text: 'Link c', href:'#'}
		]

		// @property {closable:Boolean,saveInCookie:Boolean,anchorText:String,message:String} cookieWarningBanner
		// settings for the cookie warning message (mandatory for UK stores)
	,	cookieWarningBanner: {
			closable: true
		,	saveInCookie: true
		,	anchorText: _('Learn More').translate()
		,	message: _('To provide a better shopping experience, our website uses cookies. Continuing use of the site implies consent.').translate()
		}

		// @class SCA.Shopping.Configuration
		// @property {betweenFacetNameAndValue:String,betweenDifferentFacets:String,betweenDifferentFacetsValues:String,betweenRangeFacetsValues:String,betweenFacetsAndOptions:String,betweenOptionNameAndValue:String,betweenDifferentOptions:String}
	,	facetDelimiters: {
			betweenFacetNameAndValue: '+'
		,	betweenDifferentFacets: '/'
		,	betweenDifferentFacetsValues: ','
		,	betweenRangeFacetsValues: 'to'
		,	betweenFacetsAndOptions: '?'
		,	betweenOptionNameAndValue: '='
		,	betweenDifferentOptions: '&'
		}
		// Output example: /brand/GT/style/Race,Street?display=table

		// eg: a different set of delimiters
		/*
		,	facetDelimiters: {
				betweenFacetNameAndValue: '-'
			,	betweenDifferentFacets: '/'
			,	betweenDifferentFacetsValues: '|'
			,	betweenRangeFacetsValues: '>'
			,	betweenFacetsAndOptions: '~'
			,	betweenOptionNameAndValue: '/'
			,	betweenDifferentOptions: '/'
		}
		*/
		// Output example: brand-GT/style-Race|Street~display/table

		// @param {Object} searchApiMasterOptions options to be passed when querying the Search API
	,	searchApiMasterOptions: {


			Facets: {
				include: 'facets'
			,	fieldset: 'search'
			}

		,	itemDetails: {
				include: 'facets'
			,	fieldset: 'details'
			}
		,	itemOptions: {
				fieldset: 'details'
			}

		,	relatedItems: {
				fieldset: 'relateditems_details'
			}

		,	correlatedItems: {
				fieldset: 'correlateditems_details'
			}

			// don't remove, get extended
		,	merchandisingZone: {}

		,	typeAhead: {
				fieldset: 'typeahead'
			,	sort: 'custitem_ranking_score:desc'
			}

		,	itemsSearcher: {
				fieldset: 'itemssearcher'
			,	sort: 'custitem_ranking_score:desc'
			}
		}


		// @property {String} logoUrl header will show an image with the url you set here
	,	logoUrl: _.getAbsoluteUrl('img/SCA_Logo.png')

		// @property {String} defaultSearchUrl
	,	defaultSearchUrl: 'search'

		// @property {Boolean} isSearchGlobal setting it to false will search in the current results
		// if on facet list page
	,	isSearchGlobal: true

		// @property {#obj(minLength: Number, maxResults: Number, macro: String, sort: String)} typeahead Typeahead Settings
	,	typeahead: {
			minLength: 3
		,	maxResults: 4
		,	macro: 'typeahead'
		,	sort: 'custitem_ranking_score:desc'
		}

		// @property {Array<NavigationData>} NavigationData array of links used to construct navigation. (maxi menu and sidebar)
		// @class NavigationData
	,	navigationData:  [
            {
                text: _('Department').translate()
			,	id: 'header-menu-home-anchor-department'
            ,   'class': 'header-menu-home-anchor'
			,	group: true
			,   categories: alldepts
            }
		,   {
                text: _('Brands').translate()
            ,   href: '/allbrands'
            ,   'class': 'header-menu-home-anchor'
			,	data: {
				touchpoint: 'home'
			,	hashtag: '#/allbrands'
                }
            }
		,   {
                text: _('AR-15').translate()
            ,   href: '/MCategories+AR-15'
            ,   'class': 'header-menu-home-anchor'
			,	hideMobile: true
			,	data: {
				touchpoint: 'home'
			,	hashtag: '#/MCategories+AR-15'
                }
			,	categories: ar15
            }
		,   {
                text: _('AR-308').translate()
            ,   href: '/MCategories+AR-308'
            ,   'class': 'header-menu-home-anchor'
			,	hideMobile: true
			,	data: {
				touchpoint: 'home'
			,	hashtag: '#/MCategories+AR-308'
                }
			,	categories: ar308
            }
		,   {
                text: _('AK-47').translate()
            ,   href: '/MCategories+AK-47'
            ,   'class': 'header-menu-home-anchor'
			,	hideMobile: true
			,	data: {
				touchpoint: 'home'
			,	hashtag: '#/MCategories+AK-47'
                }
			,	categories: ak47
            }
		,   {
                text: _('Red Dots').translate()
            ,   href: '/MCategories+Red-Dots'
            ,   'class': 'header-menu-home-anchor'
			,	hideMobile: true
			,	data: {
				touchpoint: 'home'
			,	hashtag: '#/MCategories+Red-Dots'
                }
			,	categories: reddots
            }
		,   {
                text: _('Scopes').translate()
            ,   href: '/MCategories+Scopes'
            ,   'class': 'header-menu-home-anchor'
			,	hideMobile: true
			,	data: {
				touchpoint: 'home'
			,	hashtag: '#/MCategories+Scopes'
                }
			,	categories: scopes
            }
]

        /*
,
            {
                text: _('AR-15').translate()
            ,   href: '/search'
            ,   'class': 'header-menu-home-anchor'
            ,	categories: ar15
            }
         ,  {
                text: _('AR-15 Pistols').translate()
            ,   href: '/search'
            ,   'class': 'header-menu-home-anchor'
            ,   categories: [
                {
                    text: _('Barrels').translate()
                ,   href: '/MCategories+AR-15-Barrels'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Barrels'
			}
                }
             ,  {
                    text: _('Bolt Carriers').translate()
                ,   href: '/MCategories+AR-15-Bolt-Carriers'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Bolt-Carriers'
			}
                }
             ,   {
                    text: _('Complete Pistols').translate()
                ,   href: '/MCategories+AR-15-Pistols'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Pistols'
			}
                }
             ,   {
                    text: _('Gas Blocks').translate()
                ,   href: '/MCategories+AR-15-Gas-Blocks'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Gas-Blocks'
			}
                }
             ,   {
                    text: _('Grips').translate()
                ,   href: '/MCategories+AR-15-Grips'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Grips'
			}
                }
             ,   {
                    text: _('Lower Parts').translate()
                ,   href: '/MCategories+AR-15-Lower-Parts'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Lower-Parts'
			}
                }
             ,   {
                    text: _('Magazines').translate()
                ,   href: '/MCategories+AR-15-Magazines'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Magazines'
			}
                }
             ,   {
                    text: _('Triggers').translate()
                ,   href: '/MCategories+AR-15-Triggers'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Triggers'
			}
                }
             ,   {
                    text: _('Lower Receivers').translate()
                ,   href: '/MCategories+AR-15-Lower-Receivers'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Lower-Receivers'
			}
                }
             ,   {
                    text: _('Stock Components').translate()
                ,   href: '/MCategories+AR-15-Stock-Component'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Stock-Component'
			}
                }
             ,   {
                    text: _('Upper Parts').translate()
                ,   href: '/MCategories+AR-15-Upper-Parts'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-15-Upper-Parts'
			}
                }
            ]
            }   
         ,  {
                text: _('AR-308').translate()
            ,   href: '/search'
            ,   'class': 'header-menu-home-anchor'
            ,   categories: [
                {
                    text: _('Barrels').translate()
                ,   href: '/MCategories+AR-308-Barrels'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Barrels'
			}
                }
             ,  {
                    text: _('Bolt Carriers').translate()
                ,   href: '/MCategories+AR-308-Bolt-Carriers'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Bolt-Carriers'
			}
                }
             ,   {
                    text: _('Gas Blocks').translate()
                ,   href: '/MCategories+AR-308-Gas-Blocks'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Gas-Blocks'
			}
                }
             ,   {
                    text: _('Grips').translate()
                ,   href: '/MCategories+AR-308-Grips'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Grips'
			}
                }
             ,   {
                    text: _('Lower Parts').translate()
                ,   href: '/MCategories+AR-308-Lower-Parts'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Lower-Parts'
			}
                }
             ,   {
                    text: _('Magazines').translate()
                ,   href: '/MCategories+AR-308-Magazines'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Magazines'
			}
                }
             ,   {
                    text: _('Rails & Handguards').translate()
                ,   href: '/MCategories+AR-308-Rails--AND--Handguards'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Rails--AND--Handguards'
			}
                }
             ,   {
                    text: _('Stocks').translate()
                ,   href: '/MCategories+AR-308-Stocks'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Stocks'
			}
                }
             ,   {
                    text: _('Triggers').translate()
                ,   href: '/MCategories+AR-308-Triggers'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Triggers'
			}
                }
             ,   {
                    text: _('Lower Receivers').translate()
                ,   href: '/MCategories+AR-308-Lower-Receivers'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Lower-Receivers'
			}
                }
             ,   {
                    text: _('Stock Components').translate()
                ,   href: '/MCategories+AR-308-Stock-Component'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Stock-Component'
			}
                }
             ,   {
                    text: _('Upper Parts').translate()
                ,   href: '/MCategories+AR-308-Upper-Parts'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AR-308-Upper-Parts'
			}
                }
            ]
            } 
         ,  {
                text: _('AK-47').translate()
            ,   href: '/search'
            ,   'class': 'header-menu-home-anchor'
            ,   categories: [
                {
                    text: _('Grips').translate()
                ,   href: '/MCategories+AK-47-Grips'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AK-47-Grips'
			}
                }
             ,   {
                    text: _('Parts').translate()
                ,   href: '/MCategories+AK-47-Parts'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AK-47-Parts'
			}
                }
             ,   {
                    text: _('Magazines').translate()
                ,   href: '/MCategories+AK-47-Magazines'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AK-47-Magazines'
			}
                }
             ,   {
                    text: _('Stocks').translate()
                ,   href: '/MCategories+AK-47-Stocks'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AK-47-Stocks'
			}
                }
             ,   {
                    text: _('Triggers').translate()
                ,   href: '/MCategories+AK-47-Triggers'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AK-47-Triggers'
			}
                }
             ,   {
                    text: _('Optic Mounts').translate()
                ,   href: '/MCategories+AK-47-Optic-Mounts'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AK-47-Optic-Mounts'
			}
                }
             ,   {
                    text: _('Rails & Handguards').translate()
                ,   href: '/MCategories+AK-47-Rails--AND--Handguards'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AK-47-Rails--AND--Handguards'
			}
                }
             ,   {
                    text: _('Stock Components').translate()
                ,   href: '/MCategories+AK-47-Stock-Component'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+AK-47-Stock-Component'
			}
                }
             
            ]
            }  
         ,  {
                text: _('Ammunition').translate()
            ,   href: '/search'
            ,   'class': 'header-menu-home-anchor'
            ,   categories: [
                {
                    text: _('.22 LR').translate()
                ,   href: '/MCategories+.22-LR'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.22-LR'
			}
                }
             ,  {
                    text: _('.22-.250').translate()
                ,   href: '/MCategories+.22-.250'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.22-.250'
			}
                }
            ,   {
                    text: _('.223/5.56 NATO').translate()
                ,   href: '/MCategories+.223-SLASH-5.56-NATO'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.223-SLASH-5.56-NATO'
			}
                }
             ,   {
                    text: _('.243 Winchester').translate()
                ,   href: '/MCategories+.243-Winchester'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.243-Winchester'
			}
                }
             ,   {
                    text: _('.25-06').translate()
                ,   href: '/MCategories+.25-06'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.25-06'
			}
                }
             ,   {
                    text: _('.270 Winchester').translate()
                ,   href: '/MCategories+.270-Winchester'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.270-Winchester'
			}
                }
             ,   {
                    text: _('.270 WSM').translate()
                ,   href: '/MCategories+.270-WSM'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.270-WSM'
			}
                }
             ,   {
                    text: _('.280 Rem').translate()
                ,   href: '/MCategories+.280-Rem'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.280-Rem'
			}
                }
             ,   {
                    text: _('.300 Win Mag').translate()
                ,   href: '/MCategories+.300-Win-Mag'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.300-Win-Mag'
			}
                }
             ,   {
                    text: _('.300 WSM').translate()
                ,   href: '/MCategories+.300-WSM'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.300-WSM'
			}
                }
             ,   {
                    text: _('.30-06').translate()
                ,   href: '/MCategories+.30-06'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.30-06'
			}
                }
             ,   {
                    text: _('.300 Blackout (7.62X35MM)').translate()
                ,   href: '/MCategories+.300-Blackout-%287.62x35mm%29'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.300-Blackout-%287.62x35mm%29'
			}
                }
             ,   {
                    text: _('.30-30').translate()
                ,   href: '/MCategories+.30-.30'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.30-.30'
			}
                }
             ,   {
                    text: _('.308 Winchester').translate()
                ,   href: '/MCategories+.308-Winchester'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.308-Winchester'
			}
                }
             ,   {
                    text: _('.308 Lapua Magnum').translate()
                ,   href: '/MCategories+.338-Lapua-Magnum'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.338-Lapua-Magnum'
			}
                }
             ,   {
                    text: _('.357 Magnum/.38 Special').translate()
                ,   href: '/MCategories+.357-Magnum-SLASH-.38-Special'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.357-Magnum-SLASH-.38-Special'
			}
                }
             ,   {
                    text: _('.380 ACP').translate()
                ,   href: '/MCategories+.380-ACP'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.380-ACP'
			}
                }
             ,   {
                    text: _('.40 S&W').translate()
                ,   href: '/MCategories+.40-S-AND-W'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+.40-S-AND-W'
			}
                }
             
            ]
            } 
         ,  {
                text: _('Red Dots').translate()
            ,   href: '/search'
            ,   'class': 'header-menu-home-anchor'
            ,   categories: [
                {
                    text: _('Magnifier Mounts').translate()
                ,   href: '/MCategories+Red-Dot-Mounts'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategoriesRed-Dot-Mounts'
			}
                }
             ,  {
                    text: _('Red Dot Accessories').translate()
                ,   href: '/MCategories+Red-Dot-Accessories'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+Red-Dot-Accessories'
			}
                }
            ,   {
                    text: _('Red Dot Magnifiers').translate()
                ,   href: '/MCategories+Red-Dot-Magnifiers'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+Red-Dot-Magnifiers'
			}
                }
             ,   {
                    text: _('Red Dot Mounts').translate()
                ,   href: '/MCategories+Red-Dot-Mounts'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+Red-Dot-Mounts'
			}
                }
             ,   {
                    text: _('Red Dot Sights').translate()
                ,   href: '/MCategories+Red-Dot-Sights'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+Red-Dot-Sights'
			}
                }
            ]
            }
         ,  {
                text: _('Scopes').translate()
            ,   href: '/search'
            ,   'class': 'header-menu-home-anchor'
            ,   categories: [
                {
                    text: _('Scope Accessories').translate()
                ,   href: '/MCategories+Scope-Accessories'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+Scope-Accessories'
			}
                }
             ,  {
                    text: _('Scope Mounts').translate()
                ,   href: '/MCategories+Scope-Mounts'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+Scope-Mounts'
			}
                }
            ,   {
                    text: _('Scope Rings').translate()
                ,   href: '/MCategories+Scope-Rings'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+Scope-Rings'
			}
                }
             ,   {
                    text: _('Scopes').translate()
                ,   href: '/MCategories+Scopes'
                ,   'class': 'header-menu-level2-menu'
				,	data: {
					touchpoint: 'home'
				,	hashtag: '#/MCategories+Scopes'
			}
                }
            ]
            }  
       
        ]
		*/
		// @property {Object} imageSizeMapping map of image custom image sizes
		// usefull to be customized for smaller screens
	,	imageSizeMapping: {
			thumbnail: 'thumbnail' // 175 * 175
		,	main: 'main' // 600 * 600
		,	tinythumb: 'tinythumb' // 50 * 50
		,	zoom: 'zoom' // 1200 * 1200
		,	fullscreen: 'fullscreen' // 1600 * 1600
		,	homeslider: 'homeslider' // 200 * 220
		,	homecell: 'homecell' // 125 * 125
		}

		// @property {Array} paymentmethods map of payment methods, please update the keys using your account setup information.

	,	paymentmethods: [
			{
				key: '5,5,1555641112' //'VISA'
			,	regex: /^4[0-9]{12}(?:[0-9]{3})?$/
			}
		,	{
				key: '4,5,1555641112' //'Master Card'
			,	regex: /^5[1-5][0-9]{14}$/
			}
		,	{
				key: '6,5,1555641112' //'American Express'
			,	regex: /^3[47][0-9]{13}$/
			}
		,	{
				key: '3,5,1555641112' // 'Discover'
			,	regex: /^6(?:011|5[0-9]{2})[0-9]{12}$/
			}
		,	{
				key: '16,5,1555641112' // 'Maestro'
			,	regex: /^(?:5[0678]\d\d|6304|6390|67\d\d)\d{8,15}$/
			}
		,	{
				key: '17,3,1555641112' // External
			,	description: 'This company allows both private individuals and businesses to accept payments over the Internet'
			}
		]

	,	bxSliderDefaults: {
			minSlides: 2
		,	slideWidth: 228
		,	maxSlides: 5
		,	forceStart: true
		,	pager: false
		,	touchEnabled:true
		,	nextText: '<a class="item-details-carousel-next"><span class="control-text">' + _('next').translate() + '</span> <i class="carousel-next-arrow"></i></a>'
		,	prevText: '<a class="item-details-carousel-prev"><i class="carousel-prev-arrow"></i> <span class="control-text">' + _('prev').translate() + '</span></a>'
		,	controls: true
		,	preloadImages: 'all'
		}

	,	siteSettings: SC && SC.ENVIRONMENT && SC.ENVIRONMENT.siteSettings || {}

	,	tracking: {
			googleTagManager: {
				id: 'GTM-TD28S8'
			,	dataLayerName: 'dataLayer'
			}
		}

	,	get: function (path, defaultValue)
		{
			return _.getPathFromObject(this, path, defaultValue);
		}

	,	getRegistrationType: function ()
		{
    		//registrationmandatory is 'T' when customer registration is disabled
			if (Configuration.get('siteSettings.registration.registrationmandatory') === 'T')
			{
				// no login, no register, checkout as guest only
				return 'disabled';
			}
			else
			{
				if (Configuration.get('siteSettings.registration.registrationoptional') === 'T')
				{
					// login, register, guest
					return 'optional';
				}
				else
				{
					if (Configuration.get('siteSettings.registration.registrationallowed') === 'T')
					{
						// login, register, no guest
						return 'required';
					}
					else
					{
						// login, no register, no guest
						return 'existing';
					}
				}
			}
		}
	};

	// Append Product Lists configuration
	_.extend(Configuration, {
		product_lists: SC && SC.ENVIRONMENT && SC.ENVIRONMENT.PRODUCTLISTS_CONFIG
	});

	// Append Cases configuration
	_.extend(Configuration, {
		cases: {
			config: SC && SC.ENVIRONMENT && SC.ENVIRONMENT.CASES_CONFIG
		,	enabled: SC && SC.ENVIRONMENT && SC.ENVIRONMENT.casesManagementEnabled
		}
	});

	_.extend(Configuration, {
		useCMS: SC && SC.ENVIRONMENT && SC.ENVIRONMENT.useCMS
	});

	// Append Bronto Integration configuration
	_.extend(Configuration, {
		bronto: {
			accountId: 'c727759672aa67b574686e5b9393eaca661edfb53de8fa32a0ee3abf74c25cd1'
		}
	});

	return Configuration;
});