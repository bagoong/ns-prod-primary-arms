{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="cms-landing-page-row">
	<div class="cms-landing-page-row-full-col" data-cms-area="cms-landing-page-placeholder-page-type" data-cms-area-filters="page_type"></div>
	<div class="col-xs-12" data-cms-area="cms-landing-page-placeholder-path" data-cms-area-filters="path"></div>
    <div class="col-xs-12" id="form-stack-form"></div>
    <div class="col-xs-12" data-cms-area="cms-landing-page-placeholder-path-bottom" data-cms-area-filters="path"></div>
    <div class="col-sm-2" data-cms-area="cms-landing-page-one" data-cms-area-filters="path"></div>
    <div class="col-sm-10" data-cms-area="cms-landing-page-two" data-cms-area-filters="path"></div>
    <div class="col-sm-3" data-cms-area="cms-landing-page-three" data-cms-area-filters="path"></div>
    <div class="col-sm-9" data-cms-area="cms-landing-page-four" data-cms-area-filters="path"></div>
    <div class="col-sm-9" data-cms-area="cms-landing-page-five" data-cms-area-filters="path"></div>
    <div class="col-sm-3" data-cms-area="cms-landing-page-six" data-cms-area-filters="path"></div>
    <div class="col-sm-10" data-cms-area="cms-landing-page-seven" data-cms-area-filters="path"></div>
    <div class="col-sm-2" data-cms-area="cms-landing-page-eight" data-cms-area-filters="path"></div>
    <div class="col-sm-2" data-cms-area="cms-landing-page-nine" data-cms-area-filters="path"></div>
    <div class="col-sm-8" data-cms-area="cms-landing-page-ten" data-cms-area-filters="path"></div>
    <div class="col-sm-2" data-cms-area="cms-landing-page-eleven" data-cms-area-filters="path"></div>
    <div class="col-xs-12" data-cms-area="cms-landing-page-twelve" data-cms-area-filters="path"></div>
</div>
