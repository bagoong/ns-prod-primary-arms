define(
  'CustomBackendConfigurations'
, [
    'Configuration'
  ]
  , function (
      Config
  )
{
	'use strict';
	Config.loginToSeePrices = true;
});