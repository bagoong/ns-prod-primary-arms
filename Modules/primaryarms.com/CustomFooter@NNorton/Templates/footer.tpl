{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}
<div data-view="Global.BackToTop"></div>
<div class="footer-content">
  <div id="banner-footer" class="content-banner banner-footer footer-row" data-cms-area="global_banner_footer" data-cms-area-filters="global"></div>
  <div id="email-footer" class="footer-row">
  	<div id="banner-footer-left" class="col-sm-6 content-banner banner-footer" data-cms-area="global_email_footer_left" data-cms-area-filters="global"></div>
    <div id="banner-footer-center" class="col-xs-4 col-xs-offset-4 col-sm-1 col-sm-offset-1 social-footer" data-cms-area="global_email_footer_center" data-cms-area-filters="global"></div>
    <div id="banner-footer-right" class="col-xs-12 col-sm-3 col-sm-offset-1 social-footer">
      <div id="block-footer-one" class="col-xs-2 social-footer-icon" data-cms-area="global_block_footer_one" data-cms-area-filters="global"></div>
      <div id="block-footer-two" class="col-xs-2 social-footer-icon" data-cms-area="global_block_footer_two" data-cms-area-filters="global"></div>
      <div id="block-footer-three" class="col-xs-2 social-footer-icon" data-cms-area="global_block_footer_three" data-cms-area-filters="global"></div>
      <div id="block-footer-four" class="col-xs-2 social-footer-icon" data-cms-area="global_block_footer_four" data-cms-area-filters="global"></div>
      <div id="block-footer-five" class="col-xs-2 social-footer-icon" data-cms-area="global_block_footer_five" data-cms-area-filters="global"></div>
      <div id="block-footer-six" class="col-xs-2 social-footer-icon" data-cms-area="global_block_footer_six" data-cms-area-filters="global"></div>
    </div>
  </div>
  <div id="email-footer-2" class="footer-row">
    <div id="banner-footer-left-2" class="col-sm-3 social-footer">
      <div id="block-footer-one" class="col-xs-2 social-footer-icon" data-cms-area="global_block_footer_one_2" data-cms-area-filters="global"></div>
      <div id="block-footer-two" class="col-xs-2 social-footer-icon" data-cms-area="global_block_footer_two_2" data-cms-area-filters="global"></div>
      <div id="block-footer-three" class="col-xs-2 social-footer-icon" data-cms-area="global_block_footer_three_2" data-cms-area-filters="global"></div>
      <div id="block-footer-four" class="col-xs-2 social-footer-icon" data-cms-area="global_block_footer_four_2" data-cms-area-filters="global"></div>
      <div id="block-footer-five" class="col-xs-2 social-footer-icon" data-cms-area="global_block_footer_five_2" data-cms-area-filters="global"></div>
      <div id="block-footer-six" class="col-xs-2 social-footer-icon" data-cms-area="global_block_footer_six_2" data-cms-area-filters="global"></div>
    </div>
    <div id="banner-footer-right-2" class="col-sm-9 content-banner banner-footer" data-cms-area="global_email_footer_right" data-cms-area-filters="global"></div>
  </div>

  <div class="footer-row">               
    <div class="RR_Reviews_Widget_Wrapper_Container">
    	<div id="RR_Reviews_Widget_Wrapper_Content" class="RR_Reviews_Widget_Wrapper_Content">
     		<div id="RR_Reviews_Widget_Wrapper" class="RR_Reviews_Widget_Wrapper" >
        	</div>
    	</div>
    </div>
      
    <div id="navigation-footer" class="col-sm-7" data-cms-area="global_navigation_footer" data-cms-area-filters="global"></div>
    <div id="logo-footer" class="col-sm-2" data-cms-area="global_logo_footer" data-cms-area-filters="global"></div>
  </div>
  <div id="copyright-footer" class="content-banner banner-footer footer-row" data-cms-area="global_copyright_footer" data-cms-area-filters="global"></div>  
  <div class="viewport-toggle"><a class="mobile-desktop-view">Desktop</a><span> | </span><a class="mobile-mobile-view">Mobile</a></div><br><br>
</div>