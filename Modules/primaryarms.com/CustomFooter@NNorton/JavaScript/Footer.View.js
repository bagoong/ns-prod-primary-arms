/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Footer
define(
	'Footer.View'
,	[
		'SC.Configuration'
	,	'GlobalViews.BackToTop.View'
	,	'Profile.Model'

	,	'footer.tpl'

	,	'Backbone'
	,	'Backbone.CompositeView'
	,	'jQuery'
	,	'underscore'
	]
,	function (
		Configuration
	,	GlobalViewsBackToTopView
	,	ProfileModel

	,	footer_tpl

	,	Backbone
	,	BackboneCompositeView
	,	jQuery
	,	_
	)
{
	'use strict';

	// @class Footer.View @extends Backbone.View
	return Backbone.View.extend({

		template: footer_tpl

	,	initialize: function (options)
		{
			/*'#main-container'*/
			this.application = options.application;

			BackboneCompositeView.add(this);

			//after appended to DOM, we add the footer height as the content bottom padding, so the footer doesn't go on top of the content
			//wrap it in a setTimeout because if not, calling height() can take >150 ms in slow devices - forces the browser to re-compute the layout.
			this.application.getLayout().on('afterAppendToDom', function ()
			{
				var headerMargin = 25;

				setTimeout(function ()
				{
					// TODO REMOVE this HARDCODED Ids!, this parameters should be pass in by each specific layout, for this the header and footer SHOULD BE removed from the
					// ApplicationSkeleton.Layout as this is generic and should not have any concrete view
					var contentHeight = jQuery(window).innerHeight() - jQuery('#site-header')[0].offsetHeight - headerMargin - jQuery('#site-footer')[0].offsetHeight;
  					jQuery('#main-container').css('min-height', contentHeight);
				},10);
			});
						
			// Set variable for Desktop/Mobile
			var self = this;
			var isset_mobdesk = _.getCookie('pascadesktopmobile');
			var isTouchDevice = _.isTouchDevice();
			var isDesktop = window.screenX !== 0 && !_.isTouchDevice();
			
			// On page load: show toggle links if not on Desktop,
			// if on mobile, check for cookie value to control the viewport
			$(document).ajaxStop(function()
			{
				if (isDesktop === false) {
					$(".viewport-toggle").show();
				}
				if (isset_mobdesk === "yes" && isDesktop === false) {
					$("meta[name='viewport']").prop('content', 'width=1220, initial-scale=0.5, user-scalable=yes');
					$(".mobile-desktop-view").css("opacity", "1.0");
				} else if (isset_mobdesk === "no" && isDesktop === false){
					$("meta[name='viewport']").prop('content', 'width=device-width, initial-scale=1, user-scalable=yes');
					$(".mobile-mobile-view").css("opacity", "1.0");
				}
				
				self.$('.mobile-desktop-view').click(function(){
					_.setCookie("pascadesktopmobile", "yes");
					location.reload();
				});
				
				self.$('.mobile-mobile-view').click(function(){
					_.setCookie("pascadesktopmobile", "no");
					location.reload();
				});
				
			});
			
			//Get profile and check for global subscription status
			var profile = ProfileModel.getInstance()
			,	isSubscribed = profile.get('emailsubscribe');

			if(isSubscribed === 'T'){
				_.setCookie('paclosedsubbtn', 'T', 90, null, 'primaryarms.com');
			}
		}

	,	childViews: {
			'Global.BackToTop': function ()
			{
				return new GlobalViewsBackToTopView();
			}
		}

		// @method getContext @return {Footer.View.Context}
	,	getContext: function ()
		{
			// @class Footer.View.Context
			return {
				logoNRA: _.getAbsoluteUrl('img/logo-nra.png')
				// @property {Boolean} showLanguages
			,	showFooterNavigationLinks: Configuration.footerNavigation
				// @property {Array<Object>} footerNavigationLinks - the object contains the properties name:String, href:String
			,	footerNavigationLinks: Configuration.footerNavigation || []
			};
			// @class Footer.View
		}
	});
});