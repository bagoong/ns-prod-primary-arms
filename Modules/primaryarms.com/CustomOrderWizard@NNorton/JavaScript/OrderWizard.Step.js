/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module OrderWizard
define(
	'OrderWizard.Step'
,	[
		'Wizard.Step'
	,	'order_wizard_step.tpl'
	,	'underscore'
	,	'Profile.Model'
	,	'Header.Simplified.View'
	,	'Footer.Simplified.View'
	,	'GlobalViews.Message.View'
	,	'jQuery'
	,	'Tracker'
	]
,	function (
		WizardStep
	,	order_wizard_step_tpl
	,	_
	,	ProfileModel
	,	HeaderSimplifiedView
	,	FooterSimplifiedView
	,	GlobalViewsMessageView
	,	jQuery
	,	Tracker
	)
{
	'use strict';

	//@class OrderWizard.Step Step View, Renders all the components of the Step @extends Wizard.Step
	return WizardStep.extend({
		//@property {Function} headerView
		headerView: HeaderSimplifiedView
		//@property {Function} footerView
	,	footerView: FooterSimplifiedView
		//@property {Function} template
	,	template: order_wizard_step_tpl
		//@method stepAdvance
	,	stepAdvance: function ()
		{
			if (this.areAllModulesReady())
			{
				return this.isStepReady() || this.wizard.isPaypalComplete();
			}
			return false;
		}
		//@method initialize
	,	initialize: function ()
		{
			WizardStep.prototype.initialize.apply(this, arguments);

			this.restrictions = {};
			this.restrictions.is_restricted = false;
			this.restrictions.is_ffl = false;
			this.restrictions.is_nfa = false;
			this.restrictions.list = [];
			this.restriction_messages = {};
			
			this.on('afterViewRender', this.trackCheckout, this);
			
			//Contains the most accurate logged in status, not SC.ENVIRONMENT.siteSettings.is_logged_in
			var profile = ProfileModel.getInstance();
			
			//Set cookie for login message if the user is logged in. Users will not see messages to 
			if(profile.get('isLoggedIn') === 'T')
			{
				_.setCookie('rstpwpalogt','true');
			}
			
			//Set the application otherwise showErrorInModal doesn't work
			this.application = this.options.wizard.application;
		}
		
		//Tracks the checkout process in GA Enhanced Ecommerce
	,	trackCheckout: function() {
			if (this.options.stepGroup.url === '/opc'){
				var	lines = this.wizard.model ? this.wizard.model.get('lines') : '';
				
				Tracker.getInstance().trackCheckout(lines);
			}
			
			this.off('afterViewRender', this.trackCheckout, this);
		}
		
		//Check each item in cart to see if any items exceed the order limit for that item
	,	checkOrderLimits: function() {
			var lines = this.wizard.model.get('lines').models
			,	items = _.map(lines, function(line){return line.get('item');})
			,	profile = ProfileModel.getInstance()
			,	prevent_checkout = false
			,	prevent_checkout_message = ''
			,	price_level = profile.get('priceLevel');

			//Prevent checkout if some items are out of stock
			if (_.some(items, function(item){return item.get('_isInStock') === false && !(item.get('isbackorderable'));}))
			{
				prevent_checkout = true;
				prevent_checkout_message += "<p><i class='fa fa-exclamation-triangle'></i> Unfortunately some of the items in your cart are no longer in stock. Please remove the items in order to checkout.</p>";
			}
			
			//Function called to check limits and quantity available preventing checkout if limit exceeded or quantity available is too low
			function checkLimits(price_level_limit){
				_.each(items, function(item){
						if (item.get(price_level_limit) || item.get(price_level_limit) === 0 ? (item.get('quantity') > item.get(price_level_limit)) : false)
						{
							prevent_checkout = true;
							prevent_checkout_message += "<p>" + item.get('_sku') + " has an order limit of " + item.get(price_level_limit) + ". Please reduce the Qty within this limit.</p>";
						}

						//If item is not a gift certificate and is not backordable then check quantity
						if (item.get('itemtype') !== 'GiftCert' && !(item.get('isbackorderable'))){
							if (item.get('quantity') > item.get('quantityavailable'))
							{
								prevent_checkout = true;
								prevent_checkout_message += "<p>" + item.get('_sku') + " does not have enough available inventory for your purchase.<br>Please reduce your order Qty.</p>";
							}
						}
					});
			}
			
			//Check the price level and pass in the current limit to use
			switch(price_level) {
				case '5':
					checkLimits('custitem_active_customer_limit');
					break;
				case '2':
					checkLimits('custitem_active_dealer_1_limit');
					break;
				case '6':
					checkLimits('custitem_active_dealer_2_limit');
					break;
				case '7':
					checkLimits('custitem_active_dealer_3_limit');
					break;
				case '10':
					checkLimits('custitem_active_mil_limit');
					break;
				default:
					checkLimits('custitem_active_customer_limit');
			}
			
			//Display the modal with limit and availability message
			if(prevent_checkout) {
				this.hideContinueButton = true;
				this.showErrorInModal(prevent_checkout_message + '<a id="order-wizard-limit-exceeded-link" href="#" data-action="edit-module" data-touchpoint="viewcart">Edit Cart</a>',_("Please edit these items").translate());
				$(document).ready(function(){$('.order-wizard-submitbutton-container').remove();});
			}
		}

		//@method render
	,	render: function ()
		{
			var layout = this.wizard.application.getLayout();

			this.profileModel = ProfileModel.getInstance();
			
			this.checkOrderLimits();

			WizardStep.prototype.render.apply(this, arguments);

			if (this.wizard.isCurrentStepFirst() && // only in the first step
				this.profileModel.get('isLoggedIn') === 'F' && // only if the user doesn't already have a session
				this.wizard.application.getConfig('checkout_skip_login'))
			{
				var message = _('Checking out as a Guest. If you have an account, please <a href="login" data-toggle="show-in-modal" data-id="skip-login-modal">login</a> and enjoy a faster checkout experience.').translate()
				,	 global_view_message = new GlobalViewsMessageView({
						message: message
					,	type: 'info'
					,	closable: true
				});

				this.$('[data-action="skip-login-message"]').empty().append(global_view_message.render().$el.html());
			}

			// Also trigger the afterRender event so the site search module can load the typeahead.
			layout.trigger('afterRender');
		}

		//@method getContext @returns OrderWizard.Step.Context
	,	getContext: function ()
		{
			var	self = this
			,	lines = this.wizard.model ? this.wizard.model.get('lines') : ''
			,	models = lines !== '' ? lines.models : ''
			,	items = models !== '' ? _.map(models, function(model){return model.get('item');}) : ''
			,	restrictions = items !== '' ? _.uniq(_.map(items, function(item){return item.get('custitemship_restrictions');})) : '';
			
			//Generates the array of total warning messages from the items to display on the unique values on the front end
			this.restriction_messages = items !== '' ? _.uniq(_.map(items, function(item){return item.get('custitem_warning_message');})) : '';
			
			//This is used by OrderWizard.Module.Shipmethod.js to determine available shipping methods
			this.restrictions.list = restrictions;
			
			//Checks each line item for restrictions and then sets the appropriate restriction values to controle restriction messaging
			_.each(items, function(item) {
				switch(item.get('custitemship_restrictions')) {
					case 'Unrestricted':
						break;
					case '&lt;10':
						break;
					case 'FFL':
						self.restrictions.is_restricted = true;
						self.restrictions.is_ffl = true;
						break;
					case 'FFL-Handgun':
						self.restrictions.is_restricted = true;
						self.restrictions.is_ffl = true;
						self.restrictions.is_ffl_handgun = true;
						break;
					case 'FFL - CA':
						self.restrictions.is_restricted = true;
						self.restrictions.is_ffl = true;
						break;
					case 'NFA - Firearm':
						self.restrictions.is_restricted = true;
						self.restrictions.is_nfa = true;
						break;
					case 'NFA - Suppressor':
						self.restrictions.is_restricted = true;
						self.restrictions.is_nfa = true;
						break;
					case 'Ammo':
						self.restrictions.is_restricted = true;
						self.restrictions.is_ammo = true;
						break;
					case 10:
						self.restrictions.is_restricted = true;
						break;
					case 11:
						self.restrictions.is_restricted = true;
						break;
					case 12:
						self.restrictions.is_restricted = true;
						break;
					case 13:
						self.restrictions.is_restricted = true;
						break;
					case 14:
						self.restrictions.is_restricted = true;
						break;
					case 15:
						self.restrictions.is_restricted = true;
						break;
					case 16:
						self.restrictions.is_restricted = true;
						break;
					case 17:
						self.restrictions.is_restricted = true;
						break;
					case 18:
						self.restrictions.is_restricted = true;
						break;
					case 19:
						self.restrictions.is_restricted = true;
						break;
					case 20:
						self.restrictions.is_restricted = true;
						break;
					case 21:
						self.restrictions.is_restricted = true;
						break;
					case 22:
						self.restrictions.is_restricted = true;
						break;
					case 23:
						self.restrictions.is_restricted = true;
						break;
					case 24:
						self.restrictions.is_restricted = true;
						break;
					case 25:
						self.restrictions.is_restricted = true;
						break;
					case 26:
						self.restrictions.is_restricted = true;
						break;
					case 27:
						self.restrictions.is_restricted = true;
						break;
					case 28:
						self.restrictions.is_restricted = true;
						break;
					case 29:
						self.restrictions.is_restricted = true;
						break;
					case 30:
						self.restrictions.is_restricted = true;
						break;
					case '&gt;35':
						self.restrictions.is_restricted = true;
						break;
					default:
						break;
				}
			});
			
			//@class OrderWizard.Step.Context
			return {
					//@property {Boolean} showTitle
					showTitle: !!this.getName()
					//@property {String} title
				,	title: this.getName()
					//@property {Boolean} showContinueButton
				,	showContinueButton: !this.hideContinueButton
					// @property {String} continueButtonLabel
				,	continueButtonLabel: this.getContinueButtonLabel() || ''
					//@property {Boolean} showSecondContinueButtonOnPhone
				,	showSecondContinueButtonOnPhone: !!this.hideSecondContinueButtonOnPhone
					//@property {Boolean} showBackButton
				,	showBackButton: !(this.hideBackButton || this.wizard.isCurrentStepFirst())
					//@property {Boolean} showBottomMessage
				,	showBottomMessage: !!this.bottomMessage
					//@property {String} bottomMessage
				,	bottomMessage: _.isFunction(this.bottomMessage) ? this.bottomMessage() : this.bottomMessage || ''
					//@property {String} bottomMessageClass
				,	bottomMessageClass: _.isFunction(this.bottomMessageClass) ? this.bottomMessageClass() : this.bottomMessageClass || ''
				
				,	isRestricted: this.restrictions.is_restricted
				
				,	isffl: this.restrictions.is_ffl
				
				,	isnfa: this.restrictions.is_nfa
				
				,	restrictionMessages: this.restriction_messages
				
				/*,	fflMessage: this.restriction_messages.ffl_message ? this.restriction_messages.ffl_message : ''
				
				,	nfaMessage: this.restriction_messages.nfa_message ? this.restriction_messages.nfa_message : ''*/
			};
		}
	});
});
