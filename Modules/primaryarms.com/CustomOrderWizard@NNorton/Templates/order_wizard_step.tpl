{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div data-action="skip-login-message" class="order-wizard-step-guest-message"></div>

{{#if showTitle}}
<header class="order-wizard-step-header">
	<h2 data-type="wizard-step-name-container" class="order-wizard-step-title">{{title}}</h2>
</header>
{{/if}}

<div data-type="alert-placeholder-step"></div>

<div class="order-wizard-step-review-wrapper">
	
	<section class="order-wizard-step-review-main">
		<div id="wizard-step-review-left"></div>
	</section>

	<section id="wizard-step-review-right" class="order-wizard-step-review-secondary">
	</section>

</div>

<div class="order-wizard-step-content-wrapper">
	
	<section id="wizard-step-content" class="order-wizard-step-content-main">
	</section>
    {{#unless showTitle}}<h2 class="order-wizard-title"><br></h2>{{/unless}}
	<section id="wizard-step-content-right" class="order-wizard-step-content-secondary">
	</section>
    
    {{#if isRestricted}}
    <div class="order-wizard-restriction-message-wrapper">
        <div class="order-wizard-restriction-message-container">
            <h4>Shipping Restrictions</h4>
            <span id="restriction-message-for-sales-order">
            <h5>Your order contains an item or items that are regulated by the ATF {{#if nfaMessage}}, the NFA{{/if}} and/or local jurisdictions.</h5>
            {{#each restrictionMessages}}
            	{{{this}}}
                <br>
                <br>
            {{/each}}
            <!--{{#if isffl}}
            	{{#if isnfa}}
                	{{nfaMessage}}
                {{else}}
                	{{fflMessage}}
                {{/if}}
            {{else}}
            	{{#if isnfa}}
                	{{nfaMessage}}
                {{/if}}
            {{/if}}-->
            <p>Primary arms reviews all controlled item purchases prior to shipment, but the ultimate responsibility in knowing the law, and the requirements to take delivery lies in the hands of the purchaser.</p>
            </span>
            <p>If you have any questions please submit your question here (<a href="/contact-us" data-touchpoint="home" data-hashtag="#/contact-us" target="_blank">Contact Us</a>), or call us at 713-344-9600 Monday through Friday 8am - 5pm CST</p>
        </div>
    </div>
    {{/if}}

	<div class="order-wizard-step-actions">

		{{#if showBottomMessage}}
		<small class="order-wizard-step-message {{bottomMessageClass}}">
			{{bottomMessage}}
		</small>
		{{/if}}

		<div class="order-wizard-step-button-container">

			{{#if showContinueButton}}
			<a class="order-wizard-step-button-continue" data-action="submit-step">
				{{continueButtonLabel}}
			</a>
			{{/if}}
			<a class="order-wizard-step-button-back" {{#unless showBackButton}}style="display:none;"{{/unless}} data-action="previous-step">
				{{translate 'Back'}}
			</a>
		</div>
	</div>
</div>