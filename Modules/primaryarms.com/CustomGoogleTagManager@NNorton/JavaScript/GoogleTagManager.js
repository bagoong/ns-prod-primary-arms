/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module GoogleTagManager

//@class GoogleTagManager @extends ApplicationModule Loads Google Tag Manager scripts
define('GoogleTagManager'
,	[	
		'Profile.Model'
	,	'Tracker'
	,	'GoogleTagManager.NavigationHelper.Plugins.Standard'
	,	'underscore'
	,	'jQuery'
	,	'Backbone'
	,	'SC.Configuration'
	,	'LiveOrder.Model'
]
,	function (
		ProfileModel
	,	Tracker
	,	GoogleTagManagerNavigationHelper
	,	_
	,	jQuery
	,	Backbone
	,	Configuration
	,	LiveOrderModel
	)
{
	'use strict';

	var win = window
	,	profile = ProfileModel.getInstance()
	,	GoogleTagManager = {

		//@property {Boolean} doCallback Indicates if this module do a callback after some particular events
		//ie: when you do a log-in, we need to track that event before we navigate to the new page, otherwise the track of the event could be aborted
		doCallback: true

		//@method trackPageview
		//@param {String} url
		//@return {GoogleTagManager}
	,	trackPageview: function (url)
		{
			var current_view = SC._applications.Shopping ? SC._applications.Shopping._layoutInstance.currentView.el.id : '';
			
			if (_.isString(url))
			{
				
			
				var is_logged_in = profile.get('isLoggedIn') === 'T' && profile.get('isGuest') === 'F'
				,	eventName = 'pageView'
				,	eventData = {
						'event': eventName
            		,	'data': {
							'page': url
						,	'loginstatus': is_logged_in
						,	'pageType': current_view
						}
					};

				//Triggers a Backbone.Event so others can subscribe to this event and add/replace data before is send it to Google Tag Manager
				Tracker.trigger(eventName, eventData, url);
				this.pushData(eventData);
			}

			return this;
		}

		//@method trackEvent Track this actions: guest-checkout, sign-in, create-account, Reorder, add-to-cart, place-order
		//@param {TrackEvent} event
		//@return {GoogleTagManager}
	,	trackEvent: function (event)
		{
			if (event && event.category && event.action)
			{
				var eventName = 'action'
				,	eventData = {
						'event': eventName
					,	'data': {
							'category': event.category
						,	'action': event.action
						,	'label': event.label
						,	'value': parseFloat(event.value) || 0
						,	'page': event.page || '/' + Backbone.history.fragment
						}
					,	'eventCallback': event.callback
					};

				//Triggers a Backbone.Event so others can subscribe to this event and add/replace data before is send it to Google Tag Manager
				Tracker.trigger(eventName, eventData, event);
				this.pushData(eventData);
			}

			return this;
		}

		//@method trackGenericEvent
		//@return {GoogleTagManager}
	,	trackGenericEvent: function ()
		{
			var eventName = 'genericEvent'
			,	eventData = {
				'event': eventName
			,	'data': {}
			};

			//Triggers a Backbone.Event so others can subscribe to this event and add/replace data before is send it to Google Tag Manager
			Tracker.trigger(eventName, eventData, arguments);
			this.pushData(eventData);

			return this;
		}

		//@method trackAddToCart
		//@param {Object} item
		//@return {GoogleTagManager}
	,	trackAddToCart: function (item)
		{
			if (item)
			{
				//Set the position, category and list on the model
				/*item.set('position', this.productViewItem ? this.productViewItem.get('position') : '');
				item.set('category', this.productViewItem ? this.productViewItem.get('category') : '');
				item.set('list', this.productViewItem ? this.productViewItem.get('list') : '');*/
				
				var eventName = 'addToCart'
				,	eventData = {
					'event': eventName
				,	'data': {
							'id': item.get('_sku')
						,	'name': item.get('pagetitle')
						,	'brand': item.get('manufacturer')
						,	'price': item.get('onlinecustomerprice').toFixed(2)
						,	'page': item.get('urlcomponent')
						,	'quantity': item.get('quantity')
						,	'category': this.productViewItem ? this.productViewItem.get('category') : ''
						,	'list': this.productViewItem ? this.productViewItem.get('list') : ''
					}
				};

				//Triggers a Backbone.Event so others can subscribe to this event and add/replace data before is send it to Google Tag Manager
				Tracker.trigger(eventName, eventData, item);
				this.pushData(eventData);
			}
			
			//Remove stored data
			this.productViewItem = null;

			return this;
		}
		
		//Custom function to add support for Checkout tracking
	,	trackCheckout: function (lines)
		{
			if (lines)
			{
				var eventName = 'checkout'
				,	eventData = {
					'event': eventName
				,	'data': {
						'products': []
					}
				};
				
				_.each(lines.models, function (product)
				{
					eventData.data.products.push({
						'sku': product.get('item').get('itemid')
					,	'name': product.get('item').get('pagetitle')
					,	'category': product.category || ''
					,	'price': product.get('rate').toFixed(2)
					,	'quantity': product.get('quantity')
					,	'variant': product.options || ''
					,	'brand': product.get('item').get('manufacturer')
					});
				});
				
				Tracker.trigger(eventName, eventData, lines);
				this.pushData(eventData);
			}
			
			return this;
		}

		//@method trackTransaction
		// https://support.google.com/tagmanager/answer/6106097?hl=en
		//@param {Tracker.Transaction.Model} @extends Backbone.Model transaction
		//@class Tracker.Transaction.Model
			//@property {String} confirmationNumber
			//@property {Number} subTotal
			//@property {Number} total
			//@property {Number} taxTotal
			//@property {Number} shippingCost
			//@property {Number} handlingCost
			//@property {Array<{Tracker.Transaction.Items.Model}>} products
			//@class Tracker.Transaction.Line.Model
				//@property {String} sku
				//@property {String} name
				//@property {String} category
				//@property {Number} rate
				//@property {Number} quantity
				//@property {String} variant
			//@class Tracker.Transaction.Model
		//@class GoogleTagManager
		//@return {GoogleTagManager}
	,	trackTransaction: function (transaction)
		{
			var eventName = 'transaction'
			,	eventData = {
					'event': eventName
				,	'data': {
						'transactionId': transaction.get('confirmationNumber')
					,	'transactionEmail': profile.get('email')
					,	'transactionAffiliation': Configuration.get('siteSettings.displayname')
					,	'transactionSubTotal': transaction.get('subTotal')
					,	'transactionTotal': transaction.get('total')
					,	'transactionTax': transaction.get('taxTotal')
					,	'transactionShipping': transaction.get('shippingCost') + transaction.get('handlingCost')
					,	'transactionCurrency': SC.ENVIRONMENT.currentCurrency && SC.ENVIRONMENT.currentCurrency.code || 'USD'
					,	'transactionProducts': []
					}
				}
			,	products = SC.ENVIRONMENT.CART.lines;
			
			//The live order model is empty at this point
			//console.log(LiveOrderModel.getInstance());
			
			//Pull the data from SC.ENVIRONMENT.CART.lines due to 'manufacturer' and other attributes being undefined in the passed in transaction data
			_.each(products, function (product)
			{
				eventData.data.transactionProducts.push({
					'sku': product.item.itemid
				,	'name': product.item.pagetitle
				,	'category': product.category || ''
				,	'price': product.item.onlinecustomerprice.toFixed(2)
				,	'quantity': product.quantity
				,	'variant': product.options || ''
				,	'brand': product.item.manufacturer
				});
			});

			/*transaction.get('products').each(function (product)
			{
				eventData.data.transactionProducts.push({
					'sku': product.get('sku')
				,	'name': product.get('name')
				,	'category': product.get('category') || ''
				,	'price': product.get('rate')
				,	'quantity': product.get('quantity')
				,	'variant': product.get('options')
				,	'brand': product.get('manufacturer')
				});
			});*/

			//Triggers a Backbone.Event so others can subscribe to this event and add/replace data before is send it to Google Tag Manager
			Tracker.trigger(eventName, eventData, transaction);
			this.pushData(eventData);

			return this;
		}

		//@method trackProductList
		//@param {Backbone.Collection} items
		//@param {String} listName
		//@return {GoogleTagManager}
	,	trackProductList: function (items, listName)
		{
			var self = this
			,	eventName = 'productList'
			,	eventData = {
					'event': eventName
				,	'data': {
						'currencyCode': SC.ENVIRONMENT.currentCurrency && SC.ENVIRONMENT.currentCurrency.code || 'USD'
					,	'items': []

					,	'page': this.getCategory()
					,	'list': listName
					}
				};

			_.each(items.models, function (item, index)
			{
				//We set this properties in the item so we can print them on the html, to later be read them by the trackProductClick event
				item.set('track_productlist_position', index + 1);
				item.set('track_productlist_category', self.getCategory());
				item.set('track_productlist_list', listName);

				eventData.data.items.push({
					'name': item.get('_name')
				,	'sku': item.get('_sku', true)
				,	'price': (item.get('_price') && item.get('_price').toFixed(2)) || 0.00
				,	'list': item.get('track_productlist_list')
				,	'position': item.get('track_productlist_position')
				,	'category': item.get('track_productlist_category')
				,	'brand': item.get('manufacturer')
				});
			});

			//Triggers a Backbone.Event so others can subscribe to this event and add/replace data before is send it to Google Tag Manager
			Tracker.trigger(eventName, eventData, items);
			this.pushData(eventData);

			return this;
		}

		//@method trackProductClick
		//@param {Object} item
		//@return {GoogleTagManager}
	,	trackProductClick: function (item)
		{
			var eventName = 'productClick'
			,	eventData = {
				'event': eventName
			,	'data': {
					'category': item.get('category')
				,	'position': item.get('position')
				,	'list': item.get('list')
				,	'sku': item.get('sku', true)
				,	'name': item.get('name')

				,	'page': this.getCategory()
				,	'label': item.get('name')
				,	'brand': item.get('manufacturer')
				}
			};

			//We set this item in this Tracker to later be read it by the trackProductView event
			this.item = item;
			//Triggers a Backbone.Event so others can subscribe to this event and add/replace data before is send it to Google Tag Manager
			Tracker.trigger(eventName, eventData, item);
			this.pushData(eventData);

			return this;
		}

		//@method trackProductClick
		//@param {Object} item
		//@return {GoogleTagManager}
	,	trackProductView: function (item)
		{
			if (this.item && this.item.get('itemId') === item.get('_id'))
			{
				item.set('category', this.item.get('category'));
				item.set('list', this.item.get('list'));
			}

			var eventName = 'productView'
			,	price = item.getPrice()
			,	eventData = {
				'event': eventName
			,	'data': {
					'sku': item.get('_sku', true)
				,	'name': item.get('_name')
				,	'variant': _.map(item.itemOptions, function(option) { return option.label; }).sort().join(', ')
				,	'price': ((price.price) ? price.price : 0).toFixed(2)
				,	'category': item.get('category') || ''
				,	'list': item.get('list') || ''
				,	'page': this.getCategory()
				,	'brand': item.get('manufacturer')
				}
			};

			//Adds a new reference to the currently viewed item for access by trackAddToCart
			this.productViewItem = this.item || item;
			
			this.item = null;

			//Triggers a Backbone.Event so others can subscribe to this event and add/replace data before is send it to Google Tag Manager
			Tracker.trigger(eventName, eventData, item);
			this.pushData(eventData);

			return this;
		}

		//@method pushData Clean all the values of the dataLayer before we send the new ones
		//@param {Object} data
	,	pushData: function(data)
		{
			// We need to push this dummy empty object to 'clean' the dataLayer, because all the data pushed is appended in another object that is sent to gtm
			// http://www.simoahava.com/gtm-tips/remember-to-flush-unused-data-layer-variables/
			win[this.configuration.dataLayerName].push({
				'event': undefined
			,	'data': undefined
			,	'eventCallback': undefined
			});

			win[this.configuration.dataLayerName].push(data);
		}

		//@method getCategory
		//@return {String}
	,	getCategory: function()
		{
			var options = _.parseUrlOptions(Backbone.history.fragment)
			,	page = options.page || '';

			return '/' + Backbone.history.fragment.split('?')[0] + (page ? '?page=' + page : '');
		}

		//@method loadScript
		//@return {jQuery.Promise|Void}
	,	loadScript: function ()
		{
			return !SC.isPageGenerator() && jQuery.getScript('//www.googletagmanager.com/gtm.js?id=' + this.configuration.id + '&l=' + this.configuration.dataLayerName);
		}

		//@method mountToApp
		//@param {ApplicationSkeleton} application
		//@return {Void}
	,	mountToApp: function (application)
		{
			this.configuration = Configuration.get('tracking.googleTagManager');

			if (this.configuration && this.configuration.id)
			{
				var layout = application.getLayout();

				//Install Standard Navigation Plugins
				layout.mouseDown.install({
					name: 'googleTagManagerStandardNavigation'
				,	priority: 20
				,	execute: function (e)
					{
						return GoogleTagManagerNavigationHelper.mouseDownNavigation(layout, e);
					}
				});

				this.configuration.dataLayerName = this.configuration.dataLayerName || 'dataLayer';

				// (Tracking Start)[https://developers.google.com/tag-manager/quickstart]
				win[this.configuration.dataLayerName] = win[this.configuration.dataLayerName] || [];
				win[this.configuration.dataLayerName].push({
					'gtm.start': new Date().getTime()
				,	'event': 'gtm.js'
				});

				Tracker.getInstance().trackers.push(GoogleTagManager);

				// the analytics script is only loaded if we are on a browser
				application.getLayout().once('afterAppendView', jQuery.proxy(GoogleTagManager, 'loadScript'));
			}
		}
	};

	return GoogleTagManager;
});
