define('SEOExclude', [
  'underscore'
], function SEOExclude(
  _
){
  return {
    mountToApp: function(application){
      //do not include these in exclude list : custitem_marketing_categories, custitem_ad_name, custitem_sale_name
      if(SC.isPageGenerator()){
        _.extend(application.Configuration.searchApiMasterOptions.Facets, {
          "facet.exclude": "custitem_1,custitem_2,custitem_accessory_attachment,custitem_action,custitem_adjustable,custitem_ambidextrous,custitem_armor_type,custitem_assembly,custitem_barrel_length,custitem_brand,custitem_bullet_weight,custitem_caliber_gauge,custitem_caliber_marking,custitem_carrier_style,custitem_color,custitem_decibel_reduction,pricelevel2,pricelevel6,pricelevel7,custitem_fitment,custitem_fixed_folding,custitem_focal_plane,custitem_forward_cant,custitem_frame_size,custitem_gas_system_length,custitem_illuminated,custitem_in_stock,custitem_latch_size,custitem_leg_style,pricelevel10,custitem_magnification,custitem_material,custitem_night_vision_compatible,pricelevel5,custitem_optic_type,custitem_pa_featured_products,custitem_pa_staff_picks,custitem_pa_top_selling,custitem_padded,custitem_platform,custitem_position,pricelevel,custitem_quick_release,custitem_ring_height,custitem_size,custitem_storage,custitem_style,custitem_thread_pattern,custitem_throw,custitem_tube_diameter,custitem_type,itemtype,custitem_user_serviceable,onlinecustomerprice"
        });
      }
    }
  };
});
