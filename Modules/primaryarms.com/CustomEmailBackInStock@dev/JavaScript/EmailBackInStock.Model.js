define('EmailBackInStock.Model', [
    'Backbone'
  , 'underscore'
  ,	'Utils'
], function(
    Backbone
  , _
) {
  'use strict';

  return Backbone.Model.extend({
    urlRoot: _.getAbsoluteUrl('services/EmailBackInStock.Service.ss')
  });

});