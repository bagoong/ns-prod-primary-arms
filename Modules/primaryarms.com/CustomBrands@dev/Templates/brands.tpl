<div class="container">
	<div class="cms-landing-page-row">
		<div class="col-xs-12" data-cms-area="brands-landing-page-12-col-a" data-cms-area-filters="path"></div>
        <div class="col-xs-2" data-cms-area="brands-landing-page-2-col-1-a" data-cms-area-filters="path"></div>
        <div class="col-xs-2" data-cms-area="brands-landing-page-2-col-2-a" data-cms-area-filters="path"></div>
        <div class="col-xs-2" data-cms-area="brands-landing-page-2-col-3-a" data-cms-area-filters="path"></div>
        <div class="col-xs-2" data-cms-area="brands-landing-page-2-col-4-a" data-cms-area-filters="path"></div>
        <div class="col-xs-2" data-cms-area="brands-landing-page-2-col-5-a" data-cms-area-filters="path"></div>
        <div class="col-xs-2" data-cms-area="brands-landing-page-2-col-6-a" data-cms-area-filters="path"></div>
        <div class="col-xs-12" data-cms-area="brands-landing-page-12-col-a-2" data-cms-area-filters="path"></div>
	</div>
	<div class="row" style="margin:20px auto">
		{{#each firstLetters}}
			<h4 id="{{this}}" class="letters" style="cursor:pointer; float: left; margin-right: 10px;"> {{this}} </h4>
		{{/each}}
	</div>
	<div class="row">
		{{#each brands}}
			<div class="col-sm-4 col-xs-12">
				{{#each group}}
					<div style="margin-bottom: 10px;">
						<h2 id="go-to-{{firstLetter}}">{{firstLetter}}</h2>
						{{#each facets}}
							<li><a href="/Brand+{{url}}">{{label}}</a></li>
						{{/each}}
					</div>
				{{/each}}
			</div>
		{{/each}}
	</div>
    <div class="row" style="margin-bottom: 20px;">
		<div class="col-xs-12" data-cms-area="brands-landing-page-12-col-b" data-cms-area-filters="path"></div>
        <div class="col-xs-2" data-cms-area="brands-landing-page-2-col-1-b" data-cms-area-filters="path"></div>
        <div class="col-xs-2" data-cms-area="brands-landing-page-2-col-2-b" data-cms-area-filters="path"></div>
        <div class="col-xs-2" data-cms-area="brands-landing-page-2-col-3-b" data-cms-area-filters="path"></div>
        <div class="col-xs-2" data-cms-area="brands-landing-page-2-col-4-b" data-cms-area-filters="path"></div>
        <div class="col-xs-2" data-cms-area="brands-landing-page-2-col-5-b" data-cms-area-filters="path"></div>
        <div class="col-xs-2" data-cms-area="brands-landing-page-2-col-6-b" data-cms-area-filters="path"></div>
        <div class="col-xs-12" data-cms-area="brands-landing-page-12-col-b-2" data-cms-area-filters="path"></div>
	</div>
</div>