define(
	'Brands'
,	[ 'Brands.Router' ]
,	function (BrandsRouter)
{
	'use strict';

	return {

	mountToApp: function (application)
		{
			return new BrandsRouter(application);
		}
	};
});