define(
    'Brands.View'
,   [
        'Backbone'
    ,   'jQuery'
    ,   'underscore'
    ,   'brands.tpl'
    ,   'Facets.Model'
]
, function(
        Backbone
    ,   jQuery
    ,   _
    ,   brandsTpl
    ,   FacetsModel
)
{
    'use strict';

    return Backbone.View.extend({

        template: brandsTpl

    ,   title: _('Brands').translate()

    ,   events:
        {
            "click .letters" : "scrollTo"
        }

    ,   initialize: function (options)
        {
			//Push ajaxStop event to the dataLayer for Google Tag Manager
			//Set variable to check before pushing to prevent some cases of duplication
			var ajaxfirebrand = false;
			
			$(document).ajaxStop(function(){
				if(!ajaxfirebrand){
					//Set to true to prevent some duplication
					ajaxfirebrand = true;
					//Wait one second and then push ajaxStop
					setTimeout(function(){
						dataLayer.push({'event': 'ajaxStop'});
					}, 1000);
				}
			});
        }
		//Function, scrolls the user to brands associated with the clicked letter
    ,   scrollTo: function(e)
        {
            var letter = e.currentTarget.id;
            $('html, body').animate({
                scrollTop: $("#go-to-"+letter).offset().top
            }, 2000);
        }

    ,   getContext: function () 
        {
			var self = this
			,	brandArray = this.options.data ? _.find(this.options.data.get('facets'), function(facet){return facet.id == 'custitem_brand';}) : ''
			,	brands = brandArray.values
			,	firstLetters = []
			,	groupedObj = [];
			
			//function capitalizeEachWord(str) {
//				return str.replace(/(?!\d\S*|\.\S*)\w\S*/g, function(txt) {
//					return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
//				});
//			}
//			
//			brands = _.each(brands, function(brand){
//						brand.label = capitalizeEachWord(brand.label);
//					});
			
			//Builds the array of first letters
			for (var i = 0; i < brands.length; i++) {
				var label = brands[i].label ? brands[i].label : '';
				firstLetters.push(label.charAt(0).toUpperCase());
			}
			//Only use the unique first letters
			firstLetters = _.uniq(firstLetters);
			
			
			//Builds the array of Brands for each letter
			for (var i = 0; i < firstLetters.length; i++) {
				var array = [];
				for (var j = 0; j < brands.length; j++) {
					var label = brands[j].label ? brands[j].label : '';
					if (label.charAt(0).toUpperCase() == firstLetters[i]) {
						array.push(brands[j]);
					}
				}
				groupedObj.push({
					firstLetter: firstLetters[i],
					facets: array
				});
			}
			
			//Splits list of Brands into 3 groups
			var len = groupedObj.length
			, brandColumns = [];
			
			len = Math.ceil(len / 3);
			
			while (groupedObj.length) {
				brandColumns.push({
					group: (groupedObj.splice(0, len))
				});
			}
			
			return {
				firstLetters: firstLetters,
				brands: brandColumns
			}
        }

    });
});